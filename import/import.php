<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

//$content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/import/marshrutki.csv");

$handle = fopen($_SERVER["DOCUMENT_ROOT"] . "/import/siversky.csv", "r");

$arrContent = [];

while(($data = fgetcsv($handle, 0, ";")) != false)
    $arrContent[] = $data;

$el = new CIBlockElement;

foreach($arrContent as $c) {
    /*$PROP = [];
    $PROP["ROUTE_NAME"] = $c[1];
    $PROP["ROUTE"] = $c[2];*/

    $arLoadItemArray = Array(
        "IBLOCK_SECTION_ID" => 168,
        "IBLOCK_ID"      => 7,
        //"PROPERTY_VALUES"=> $PROP,
        "NAME"           => $c[0],
        "ACTIVE"         => "Y"
    );

    $el->Add($arLoadItemArray);
}

fclose($handle);

/*
$elements = CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID" => 10, "SECTION_ID" => 165), false, false, Array("ID", "NAME"));

while($e = $elements->Fetch()) {
    foreach($arrContent as $c) {
        if($c[0] == $e["NAME"]) {
            CIBlockElement::SetPropertyValuesEx($e["ID"], false, Array("ROUTE" => $c[2], "ROUTE_NAME" => $c[1]));
        }
    }
}*/