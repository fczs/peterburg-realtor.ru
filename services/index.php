<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <div class="gp_hide page-header">
        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services.jpg">

        <h1>������</h1>

        <div class="header-text-wrapper">
            <div class="quote">
                <div class="quote-text">�� ��������� ����� ������� ���������� �� ������ � ���, ����� ���������
                    ������������� �����������, ������� � ���, ���� �� �� ���������
                </div>
                <div class="quote-author">������� ���������</div>
            </div>
        </div>
    </div>
<div class="content-left">
    <h2 style="line-height: 1.4"><span style="color: #EE2A7A">������</span> ��������� ������������<br><span
            style="color: #EE2A7A">�������������� �������</span></h2>

    <p>�� ���������� � ������������ ������ �����?<br>
        �� ������ ������ �������� � ����� ���������� ����?<br>
        ��� ����� ������� ��� ����� �������� ��� ���������� ���?<br>
        �� ���������� ������ � ��� ����� ������������ ������������?<br>
        �� ���������� � ������� ������������ � ����� ��� �� �������?<br>
        �� �� ������ ������� ������� � ������� ������������ ��-�� ���������� �������� �������?</p>

    <p><span class="we">����������� ��������� "������������� �������" ������� ���!</span></p>

    <div class="txt-block">
        <i></i>��� ������ �������� �� ����� ��������� ���� �������� ������������ �����-���������� � ������������� ������� �
        �������, ��������, ����, ������������ ��������� � �������.<br>
        <i></i>����� �� ���������� ������ �� ������� ��������� �� ����� �������� �������������, ������ ���������� ���������!<br>
        <i></i>�������� �������� ������� �������� ��������� ������ ���� ��� ��������, � ������� ���� �������� � ��������� �������.<br>
        <i></i>����� �� ����� ������������ �������� �������� ��� ����� ����� ������������ �� ������ �������� ��� �������� ���� �������� ��� ���������.
    </div>

    <p>�� ���������������� � ����� ������� �� �������� <a href="tel:+78123266904">(812) 326-69-04</a> ��� ���������
        ������/������ �� ����������� ����� <a href="mailto:spbrealt@gmail.com">spbrealt@gmail.com</a>.</p>

    <p>�������� ����� ������ ������ ����� �����.</p>

    <div class="index-about">
        <ul class="grid effect-8" id="grid">
            <li>
                <div class="teaser">
                    <a href="/services/lease/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/sales/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/new/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�����������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/rent/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�����</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/redemption/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�����</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/credit/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>������������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/management/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center" style="top: 34%;"><span
                                style="line-height: 1.5;">�������������</span><br><span style="line-height: 1.5;">����������</span>
                        </div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/services/additional/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center" style="top: 34%;"><span
                                style="line-height: 1.5;">��������������</span><br><span style="line-height: 1.5;">������</span>
                        </div>
                    </a>
                </div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/masonry.pkgd.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/imagesloaded.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/classie.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/AnimOnScroll.js"></script>
    <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.5,
            maxDuration: 1,
            viewportFactor: 0.2
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>