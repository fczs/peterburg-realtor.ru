<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

    <h1>������������� ����������</h1>

    <p>��� �� ������ ����� ������ � �������� ����� �� ����� � ������ ����� ������������... �� �� ��������, ��� �������
        ���������, ��������� �������� � ����������� ��������. ����� ��������� ������ � ������ � ��������� �� �����
        �������������, �� ������� ����� ������� � ���. </p>

    <p><span class="we">�� �� ������ ���������� � ��������������!</span></p>

    <p>�� �������������� ������� � 2008 �. ��������� ������ �� �������������� ���������� ����� � ������������
        �������������.</p>

    <h2>������������� ���������� ����� �������������</h2>

    <p>� ����� ������ �������� ����� ����� ������, ��� ��������� ������������� � ����������� �� ������������ ���������
        ������������ ���������, �, �������� �� ����������� �������, ��� ������� ����� ���� ��������� ��� ������������
        ������� ��������. ��� ���� � ������� ���������������� ����������� ���������� �� �����. � ���. </p>

    <p>��� ����� ����� � ������ � ������������ ��������� ������� � ����������������� ������ ������, ��������� ������
        ������������ ��������, ������ ��������� ������ � �������, ��������� ���������������� �������.</p>

    <p><span class="we">��� � ������ ������ ������� ������� ������ ���������� �������� �� ������������� ���������� � �����
        ����������.</span></p>

    <p>��� ���������� �������� �� ������������� ���������� ���� ����������� ����� ������������ �� ����������� ���������
        ������ ������ �� ������ ����� ������ ������������, �� ��������� �����������, ������ ������, �������� � ������
        ��������������. </p>

    <p>����� ���������� �������� �� ������������� ���������� �� �������� ���������������� ���������� � ������ ���������
        ��������.</p>

    <div class="txt-block">
        <h3>���� ������ ������� (�� ����� 10% �� ����� ����������� �������� ��������), �� ��� ��������:</h3>
        <i></i>����� ���������� � ���������� � ���������� �������� ������;<br>
        <i></i>�������� �� ������������� ��������� �������� ��������, ������������ ��������, ������ ��
        ����������� ���������������;<br>
        <i></i>���������� ����������� � ������������� � ����������������� ��������, ��������� ������ ��
        ������;<br>
        <i></i>����������� ������� ��������, ����������� � ������������ � ���������� � ���� �������� ��������
        ������;<br>
        <i></i>����������� �������� ��������� ���������� ���������;<br>
        <i></i>�������������� ����������� ������� ������������ �����;<br>
        <i></i>���������� �������������� ����� ������������� � �����������;<br>
        <i></i>��������� ���������������� �������;<br>
        <i></i>���������� ��������� � ������ ������ ����������.
    </div>

    <h2>������������� ���������� ����� �������������</h2>

    <p>������������ ������������ ������ ���� � �������� �������� ������������ �����������, ��� �������� � �����. ��
        ������ � ���, ��� ����� ������ ������ ����������� ������ �� ���������.</p>

    <p>����� ������������ ������� ���������� � ���������� ���������������� �������, ���������� ������������ ������������
        � ������� ��������� � ��������� ������� �� ������������ �����.</p>

    <p>��� �������� �������� ����� ��� ��������� ������ ������������ ��������� ����������� ����� ���������������.
        ���������� ������ ������ � ���������� �������, ��� ��� ���� �������� �������� ������� � ������ ������������ ��
        ����������� ������� ������� �� ������� ��������� �������������������� ��������.</p>

    <p>��� ������ � �������������, ������ ��� ���������, ���������, � �.�. ��������� �������� ���������, ���������
        ���������, ���������� ���� ����������� � ������������ � ����������������� �� � ����� ��������� �����������.
        ������������ ���������� ��������� ������� � ������������ ������������ ����� �������, � ��� ������������,
        ��������, ������ �������� ������������ ��� ���������� ���� ���� ����� �������� �������. � �������� ����������
        ���� � ������ ������� �� ������������ ���������.</p>

    <p><span class="we">�� �������������� �������, �������� ������������ ������������� � ������ �������� � ������������� ����������,
        ������ ��� ����������������, ����������� ������, ������� ������������ ������������ � ������������.</span></p>

    <p>�� ���������� ������� �� �������� ������ �� ������� ������������ (��������, ��������� � ����������� ��������
        ����� ����, �����, ���� ��� �������������� �������� ������� ������� ����� ��� � ��� ����, � ���������� ��������
        �� ����� ������ � ������� �� ������ ����).</p>

    <p>����� ��� ������������� �� ������� ����������� � ��������� �������������� � ����� ���������, �������� �����������
        �/��� ������� ������. </p>

    <div class="txt-block">
        <h3>���� ������ �� �������������� ���������� �������� � ����:</h3>
        <i></i>������ ������� ������������ � ������������ ����������� �� �������� �������� ��������;<br>
        <i></i>���������� ��������� ��������, ������������ �� ����� ����������;<br>
        <i></i>���������� ����������, ���������� � ����������� �������� ������;<br>
        <i></i>���������� ����� �� ������������ �������;<br>
        <i></i>�������� �� ���������� ���������;<br>
        <i></i>���������� � �������� �� ���������� � ����������-�������������, ��������� ������ �� ������
        ������������ ��������, ������ �� ���������������� �����;<br>
        <i></i>������ ��� ������������ ��������������;<br>
        <i></i>������ � ����������� � ���������� ������������ � �������� �������;<br>
        <i></i>����������� ��������������;<br>
        <i></i>������ � ������� �������� ����� ������������� � ����������� � ���������� �������;<br>
        <i></i>������ � ������� �������� ���;<br>
        <i></i>������������ ����������.
    </div>

    <p><span class="we">�������������� ������ � ������� �������!</span></p>

    <p><b>�������� ������ ����� �� �������� <a href="tel:+78123266904">(812) 326-69-04</a> ��� ��������������� ������
            �������� �����</b></p>

<? require($_SERVER["DOCUMENT_ROOT"] . "/include/service-order.php"); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>