'use strict';

var gulp    = require('gulp'),
    image   = require('gulp-image'),
    concat = require('gulp-concat'),
    uglifycss = require('gulp-uglifycss');

gulp.task('image', function () {
    gulp.src('src/**/*')
        .pipe(image({
            zopflipng: false
        }))
        .pipe(gulp.dest('upload'));
});

gulp.task('default', ['image']);

gulp.task('css', function () {
    return gulp.src([
        // 'bitrix/templates/gp_real_estate_lite/css/animations.css',
        // 'bitrix/templates/gp_real_estate_lite/css/component.css',
        // 'bitrix/templates/gp_real_estate_lite/css/timeline.css',
        // 'bitrix/templates/gp_real_estate_lite/css/animate.css',
        'bitrix/templates/gp_real_estate_lite/css/styles.css'])
        .pipe(concat('styles.min.css'))
        .pipe(uglifycss({
            "maxLineLen": 500,
            "uglyComments": true
        }))
        .pipe(gulp.dest('bitrix/templates/gp_real_estate_lite'))
});
