<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <h1>�������</h1>
    <p><span class="we">"������������� �������"</span> ���� ���, ��� ����� ���� ������ � ������ ��� ����� ������!</p>
    <p>���� �� ������� ��� ���� ��������� �������� (������ �� ������������) � ��������� ������������ �� ������� �
        ������!</p>
    <p>���� ������� �������� ����� �������� �������� ����� ���������!</p>
    <p>���� �� ������, ����������� � �������������, ���������������� � ���������������!</p>
    <p>���� �� ������ ����� ���������� � �������� �����������, �������� � ��������� ��� ������ � ����������� �
        �����������!</p>
    <p>�� ���� ��� �� ������������� �� ������: ������������ ��., �. 17, ���� 402.</p>
    <p>������� ��� �����: <b>+7&nbsp;(812)&nbsp;326-69-04</b></p>
<?
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => "8"), false, Array(), Array());
while ($arFields = $res->GetNext()) :?>
    <div class="vacant-position">
        <i></i><a href="#" class="show-txt" name="<?= $arFields["ID"] ?>"><?= $arFields["NAME"] ?></a>
        <div class="more-txt declaration-<?= $i ?>" data-name="<?= $arFields["ID"] ?>">
            <?= $arFields["DETAIL_TEXT"] ?>
        </div>
    </div>
<? endwhile; ?>
    <h2>��������� ������ ����������</h2>
<? $APPLICATION->IncludeComponent(
    "gp:iblock.element.add.form",
    "gp_add_vacancy",
    Array(
        "SEF_MODE" => "Y",
        "AJAX_MODE" => "Y",
        "IBLOCK_TYPE" => "gp_realestate_vacancy",
        "IBLOCK_ID" => "12",
        "PROPERTY_CODES" => Array(
            "NAME",
            "82",
            "83",
            "84",
            "85",
            "86"
        ),
        "PROPERTY_CODES_REQUIRED" => Array(
            "NAME",
            "82",
            "83"
        ),
        "GROUPS" => Array("1", "2", "3", "4", "5", "6", "7"),
        "STATUS" => Array(),
        "STATUS_NEW" => "2",
        "ALLOW_EDIT" => "N",
        "ALLOW_DELETE" => "N",
        "ELEMENT_ASSOC" => "CREATED_BY",
        "NAV_ON_PAGE" => "10",
        "MAX_USER_ENTRIES" => "100000",
        "MAX_LEVELS" => "100000",
        "LEVEL_LAST" => "Y",
        "USE_CAPTCHA" => "N",
        "USER_MESSAGE_ADD" => "���� ������ �������",
        "USER_MESSAGE_EDIT" => "",
        "DEFAULT_INPUT_SIZE" => "60",
        "RESIZE_IMAGES" => "N",
        "MAX_FILE_SIZE" => "0",
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_TAGS" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
        "CUSTOM_TITLE_DETAIL_TEXT" => "",
        "CUSTOM_TITLE_DETAIL_PICTURE" => "",
        "SEF_FOLDER" => "/",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "VARIABLE_ALIASES" => Array()
    )
); ?>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>