<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <div class="gp_hide page-header">
        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/cooperating.jpg">
        <h1>��������������</h1>
        <div class="header-text-wrapper">
            <div class="quote">
                <div class="quote-text">���������� ������ � ��� ������, ���������� ������ � ��� ��������, �������� ������ � ��� ������</div>
                <div class="quote-author">����� ����</div>
            </div>
        </div>
    </div>
<div class="content-left">
    <h2>�������������� � ���������� ������������ �������������� �������</h2>
    <p>����������� �� ������������ �������� "������������� �������" ����� ������� ���� ���������� ������ �����
        ���������, ��������� ������ ������ �����: �� ������ �������� �� ������� ���������� ������, �������
        ���������������� �� ����������� � ���������� ��������, ����������� � ������������ ���������, �������������
        ����������� ������ �����-�������, ������, ��������� � ��. �� ����� ������������ �����-����������, �������,
        �������� ������ � ���������.</p>

    <p>��� ������ ����� �������� ��� ��������� ������ ���������? ����� ������� ��������� ����� �������� �����.</p>
    <div class="add-review-link pink-btn"><a class="btn callme" href="#">�������� ������</a></div>

    <p>�� ���������� ����������� ������ � � ��������� ����� �������� � ���� ��� ���������� ������������.</p>

    <p>����� �� ������ ��������� ���: <span class="we">(812) 326-69-04</span></p>
    <div class="index-about">
        <ul class="grid effect-8" id="grid">
            <li>
                <div class="teaser">
                    <a href="/cooperating/applications/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>������ ���������</span></div>
                    </a>
                </div>
            </li>
            <? if ($USER->isAdmin()): ?>
            <li>
                <div class="teaser">
                    <a href="/cooperating/needs/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>���������</span></div>
                    </a>
                </div>
            </li>
            <? endif ?>
            <li>
                <div class="teaser">
                    <a href="/cooperating/vacancy/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/vacancy.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>��������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/cooperating/franchise/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�����������</span></div>
                    </a>
                </div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/masonry.pkgd.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/imagesloaded.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/classie.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/AnimOnScroll.js"></script>
    <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.5,
            maxDuration: 1,
            viewportFactor: 0.2
        });
    </script>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>