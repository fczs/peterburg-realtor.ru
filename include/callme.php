<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<script type="text/javascript" src="/bitrix/js/main/ajax.js"></script>
<?$APPLICATION->IncludeComponent(
    "gp:callme",
    "real_estate",
    array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "USE_CAPTCHA" => "N",
        "OK_TEXT" => "�������, ���� ������ �������",
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
            1 => "PHONE",
            2 => "MESSAGE",
        ),
        "EMAIL_TO" => "info@peterburg-realtor.ru",
        "IBLOCK_TYPE" => "gp_realestate_callme",
        "IBLOCK_ID" => "2",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "EVENT_MESSAGE_ID" => array(
            0 => "8",
        )
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>