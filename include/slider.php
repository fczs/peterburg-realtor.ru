<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<!--/ Slider /-->
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/slides.min.jquery.js"></script>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/slides.js"></script>

<h2>������ �����������</h2>
<div class="gp_slider">
    <? $APPLICATION->IncludeComponent(
        "bitrix:news",
        "gp_slider",
        Array(
            "DISPLAY_DATE" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "USE_SHARE" => "N",
            "SEF_MODE" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "gp_realestate_catalog",
            "IBLOCK_ID" => "5",
            "NEWS_COUNT" => "",
            "USE_SEARCH" => "N",
            "USE_RSS" => "N",
            "USE_RATING" => "N",
            "USE_CATEGORIES" => "N",
            "USE_FILTER" => "N",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "CHECK_DATES" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "LIST_FIELD_CODE" => array(),
            "LIST_PROPERTY_CODE" => array("SLIDER_URL"),
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "DISPLAY_NAME" => "Y",
            "META_KEYWORDS" => "-",
            "META_DESCRIPTION" => "-",
            "BROWSER_TITLE" => "-",
            "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
            "DETAIL_FIELD_CODE" => array(),
            "DETAIL_PROPERTY_CODE" => array(),
            "DETAIL_DISPLAY_TOP_PAGER" => "N",
            "DETAIL_DISPLAY_BOTTOM_PAGER" => "N",
            "DETAIL_PAGER_TITLE" => "",
            "DETAIL_PAGER_TEMPLATE" => "",
            "DETAIL_PAGER_SHOW_ALL" => "Y",
            "SET_TITLE" => "Y",
            "SET_STATUS_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "USE_PERMISSIONS" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "0",//"36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "Y",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "SEF_FOLDER" => "#SITE_DIR#catalog/",
            "SEF_URL_TEMPLATES" => Array(
                "detail" => "#SECTION_CODE#/#SECTION_ID#/#ELEMENT_ID#/"
            ),
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "VARIABLE_ALIASES" => Array(
                "detail" => Array(),
            )
        )
    ); ?>
</div>
<!--/ End Slider /-->
