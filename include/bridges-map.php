<script src="http://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>
<script>
    YMaps.jQuery(function () {
        var map = new YMaps.Map(YMaps.jQuery("#map")[0]);
        map.setCenter(new YMaps.GeoPoint(30.349386, 59.951247), 12);
        var options = {
            hasBalloon: true,
            style: "default#blueSmallPoint"
        };
        bridges = { a23: new YMaps.Placemark(new YMaps.GeoPoint(30.396238, 59.925315), options), a24: new YMaps.Placemark(new YMaps.GeoPoint(30.303541, 59.946592), options), a25: new YMaps.Placemark(new YMaps.GeoPoint(30.289121, 59.93462), options), a14: new YMaps.Placemark(new YMaps.GeoPoint(30.401571, 59.942291), options), a26: new YMaps.Placemark(new YMaps.GeoPoint(30.452972, 59.877581), options), a27: new YMaps.Placemark(new YMaps.GeoPoint(30.334709, 59.967861), options), a21: new YMaps.Placemark(new YMaps.GeoPoint(30.308219, 59.941075), options), a13: new YMaps.Placemark(new YMaps.GeoPoint(30.32192, 59.9781), options), a28: new YMaps.Placemark(new YMaps.GeoPoint(30.349815, 59.952745), options), a29: new YMaps.Placemark(new YMaps.GeoPoint(30.33737, 59.95779), options), a30: new YMaps.Placemark(new YMaps.GeoPoint(30.329645, 59.946157), options), a31: new YMaps.Placemark(new YMaps.GeoPoint(30.285688, 59.949176), options), a32: new YMaps.Placemark(new YMaps.GeoPoint(30.409284, 59.915146), options)};
        assoc = { a24: 1, a14: 2, a21: 3, a33: 4, a30: 5};
        descr = { a23: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��. ��������</span><br />������ ����������:<br />� 2:20 �� 5:10<br />', a24: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������</span><br />������ ����������:<br />� 2:00 �� 4:55<br />', a25: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������������</span><br />������ ����������:<br />� 1:25 �� 2:45<br />� 3:10 �� 5:00<br />', a14: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">���������������</span><br />������ ����������:<br />� 2:00 �� 5:00<br />', a26: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">�����������</span><br />������ ����������:<br />� 2:00 �� 3:45<br />� 4:15 �� 5:45<br />', a27: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">������������</span>', a21: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">���������</span><br />������ ����������:<br />� 1:25 �� 2:50<br />� 3:10 �� 4:55<br />', a13: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������������</span>', a28: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������</span><br />������ ����������:<br />� 1:40 �� 4:45<br />', a29: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������������</span>', a30: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">��������</span><br />������ ����������:<br />� 1:35 �� 4:50<br />', a31: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">������</span><br />������ ����������:<br />� 2:00 �� 2:55<br />� 3:35 �� 4:55<br />', a32: '<span style="padding:10px 20px 10px 0px;font-weight:bold;">�����������</span><br />������ ����������:<br />� 2:20 �� 5:30<br />'};
        for (var key in bridges) {
            map.addOverlay(bridges[key]);
            bridges[key].desar = key;
            bridges[key].description = descr[key];
        }
    })
</script>
<div id="map" style="height:450px!important;"></div>