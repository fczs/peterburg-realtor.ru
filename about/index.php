<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
    <div class="gp_hide page-header">
        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/teamwork.jpg">
        <h1>� ��������</h1>
        <div class="header-text-wrapper">
            <div class="quote">
                <div class="quote-text">������� ���������� ����, � ������� � �����������</div>
                <div class="quote-author">����� �������</div>
            </div>
        </div>
    </div>
<div class="content-left">
    <h2>�������� <span style="color: #EE2A7A">�������������� �������</span></h2>
    <p><span class="we">�������</span> �������������� � ������� ����� �� ����� ������������.</p>
    <p><span class="we">��������</span> �� ����� ������������ � 2008 ����.</p>
    <p><span class="we">�������</span> � �������������� � ���������� � �������� ������ � ������ � �� �������.</p>
    <h2>�������� <span style="color: #EE2A7A">���� ������������</span>:</h2>
    <div class="txt-block">
        <i></i>�������� � ����� � ������������ �������������;<br>
        <i></i>������, ������� � ������� ������������ � ������ � �� �������;<br>
        <i></i>������������� ���������� ��������� ������������;<br>
        <i></i>�������� �� ����� ���������� ������������;<br>
        <i></i>������� ���������� ������������;<br>
        <i></i>���������� ��������� �����;<br>
        <i></i>������ �� ������ (����);<br>
        <i></i>������� ����� �������� ������������, ������� ��� ����� ������������;<br>
        <i></i>������������ � ����������� ��������� �� �������� ������������.

    </div>
    <div class="index-about">
        <ul class="grid effect-8" id="grid">
            <li>
                <div class="teaser">
                    <a href="/about/us/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/aboutus.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>� ���</span></div>
                    </a>
                </div>
            </li>
            <!--<li>
                <div class="teaser">
                    <a href="/about/agents/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/agents.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�� � �����</span></div>
                    </a>
                </div>
            </li>-->
            <li>
                <div class="teaser">
                    <a href="/about/reviews/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/reviews.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>������</span></div>
                    </a>
                </div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/masonry.pkgd.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/imagesloaded.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/classie.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/AnimOnScroll.js"></script>
    <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.5,
            maxDuration: 1,
            viewportFactor: 0.2
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>