<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
include("directory.php");
CModule::IncludeModule("iblock");
set_time_limit(0);

$strTmp = "";

$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/export/yandex_sber.xml", "wb");

$strTmp .= '<?xml version="1.0" encoding="utf-8"?>';
$strTmp .= '<realty-feed xmlns="http://webmaster.yandex.ru/schemas/feed/realty/2010-06">';
$strTmp .= '<generation-date>' . Date("c") . '</generation-date>';
$updateTime = date("H:i:s d.m.Y");

$arFilter = Array(
    "IBLOCK_ID" => CATALOG_IBLOCK_ID,
    "SECTION_ID" => array(6, /*9*/),
    "ACTIVE" => "Y"
);

$arSelectFields = Array(
    "ID",
    "IBLOCK_ID",
    "NAME",
    "IBLOCK_SECTION_ID",
    "DATE_CREATE",
    "TIMESTAMP_X",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_*"
);

$elements = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$user = array();
while ($element = $elements->GetNextElement()) {
    $r = $element->GetFields();
    $r["PROPERTY"] = $element->GetProperties();

    $rsUser = CUser::GetByID($r["PROPERTY"]["realtor"]["VALUE"]);
    $user = $rsUser->Fetch();

    $sectionCode = "";
    $address = "";
    $allArea = "";
    $areaUnit = "";

    if ($r["IBLOCK_SECTION_ID"] == CAT_RESALE) {
        if (empty($r["PROPERTY"]["type"]["VALUE_ENUM_ID"])) {
            $category = w2u($arCategory[$r["PROPERTY"]["names_list"]["VALUE_ENUM_ID"]]);
        } else {
            if ($r["PROPERTY"]["type"]["VALUE_ENUM_ID"] == 173) continue;
            $category = w2u($arCategory[$r["PROPERTY"]["type"]["VALUE_ENUM_ID"]]);
        }
    } else {
        $category = "квартира";
    }

    $res = CIBlockSection::GetByID($r["IBLOCK_SECTION_ID"]);
    if ($ar_res = $res->GetNext())
        $sectionCode = $ar_res["CODE"];

    if ($r["PROPERTY"]["area"]["VALUE_ENUM_ID"] == PROP_AREA_LEN_OBL)
        $localityName = w2u($r["PROPERTY"]["district"]["VALUE"]);
    elseif ($r["PROPERTY"]["area"]["VALUE_ENUM_ID"] == PROP_AREA_SPB)
        $localityName = w2u($arRegion[$r["PROPERTY"]["area"]["VALUE_ENUM_ID"]]);

    if (strlen($r["PROPERTY"]["address"]["VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY"]["address"]["VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $rooms = $r["PROPERTY"]["rooms"]["VALUE"] == PROP_ROOMS_STUDIO ? 0 : str_replace("+", "", $r["PROPERTY"]["rooms"]["VALUE"]);

    $houseNumber = (strlen($r["PROPERTY"]["number"]["VALUE"])) ? ", д. " . w2u($r["PROPERTY"]["number"]["VALUE"]) : "";

    if (strlen($r["PROPERTY"]["housing"]["VALUE"])) {
        $houseNumber .= ", к. " . $r["PROPERTY"]["housing"]["VALUE"];
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty($r["IBLOCK_ID"], $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty($r["IBLOCK_ID"], $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $strTmp .= '<offer internal-id="' . $r["ID"] . '">';
    $strTmp .= '<type>' . w2u($type) . '</type>';
    $strTmp .= '<property-type>' . w2u($propertyType) . '</property-type>';
    $strTmp .= '<category>' . $category . '</category>';

    $strTmp .= '<url>' . $urlCat . $sectionCode . '/' . $r["IBLOCK_SECTION_ID"] . '/' . $r["ID"] . '/</url>';

    $created = time() - strtotime($r["DATE_CREATE"]) > 5184000 ? time() - 5184000 : strtotime($r["DATE_CREATE"]);
    $edited = time() - strtotime($r["TIMESTAMP_X"]) > 259200 ? time() - 259200 : strtotime($r["TIMESTAMP_X"]);

    $strTmp .= '<creation-date>' . date_format(date_create('@' . $created), 'c') . '</creation-date>';
    $strTmp .= '<last-update-date>' . date_format(date_create('@' . $edited), 'c') . '</last-update-date>';

    if ($r["IBLOCK_SECTION_ID"] == CAT_NEW) {
        $propHouseDB = CIBlockElement::GetProperty($r["IBLOCK_ID"], $r["ID"], array("sort" => "asc"), array("CODE" => "house_name"));
        if ($propHouse = $propHouseDB->Fetch()) {
            if ($propHouse["VALUE_XML_ID"] != "X") {
                $strTmp .= '<yandex-building-id>' . $propHouse["VALUE_XML_ID"] . '</yandex-building-id>';
            }
        }
    }

    $strTmp .= '<location>';
    $strTmp .= '<country>' . w2u($country) . '</country>';
    if ($r["PROPERTY"]["area"]["VALUE_ENUM_ID"] != 490) {
        $strTmp .= '<region>' . w2u($arRegion[$r["PROPERTY"]["area"]["VALUE_ENUM_ID"]]) . '</region>';
    }
    if ($r["PROPERTY"]["area"]["VALUE_ENUM_ID"] == PROP_AREA_LEN_OBL)
        $strTmp .= '<district>' . w2u($r["PROPERTY"]["city"]["VALUE"]) . '</district>';
    $strTmp .= '<locality-name>' . $localityName . '</locality-name>';
    if ($r["PROPERTY"]["area"]["VALUE_ENUM_ID"] == PROP_AREA_SPB)
        $strTmp .= '<sub-locality-name>' . w2u($r["PROPERTY"]["district"]["VALUE"]) . w2u($district) . '</sub-locality-name>';
    if (strlen($r["PROPERTY"]["address"]["VALUE"]))
        $strTmp .= '<address>' . $address . $houseNumber . '</address>';
    if (strlen($r["PROPERTY"]["metro"]["VALUE"])) {
        $strTmp .= '<metro>';
        $strTmp .= '<name>' . w2u($r["PROPERTY"]["metro"]["VALUE"]) . '</name>';
        $strTmp .= '</metro>';
    }
    if (strlen($r["PROPERTY"]["FLAT_NUMBER"]["VALUE"])) {
        $strTmp .= '<apartment>' . $r["PROPERTY"]["FLAT_NUMBER"]["VALUE"] . '</apartment>';
    }
    $strTmp .= '</location>';

    $strTmp .= '<sales-agent>';
    $strTmp .= '<name>' . w2u($user["NAME"]) . " " . w2u($user["LAST_NAME"]) . '</name>';
    $strTmp .= '<phone>' . $user["PERSONAL_PHONE"] . '</phone>';
    $strTmp .= '<category>' . w2u($cat) . '</category>';
    $strTmp .= '<organization>' . w2u($name) . '</organization>';
    $strTmp .= '<url>' . $url . '</url>';
    $strTmp .= '<email>' . $user["EMAIL"] . '</email>';
    $strTmp .= '<photo>' . $urlLogo . '</photo>';
    $strTmp .= '</sales-agent>';

    $strTmp .= '<price>';
    $strTmp .= '<value>' . trim($r["PROPERTY"]["price"]["VALUE"]) . '</value>';
    $strTmp .= '<currency>' . w2u($arCurrency[$r["PROPERTY"]["price_currency"]["VALUE_ENUM_ID"]]) . '</currency>';
    $strTmp .= '</price>';

    if (strlen($r["PROPERTY"]["all_area"]["VALUE"])) {
        $strTmp .= '<area>';
        $strTmp .= '<value>' . trim($r["PROPERTY"]["all_area"]["VALUE"]) . '</value>';
        $strTmp .= '<unit>' . w2u($areaUnit) . '</unit>';
        $strTmp .= '</area>';
    }

    if (strlen($r["PROPERTY"]["live_area"]["VALUE"])) {
        $strTmp .= '<living-space>';
        $strTmp .= '<value>' . trim($r["PROPERTY"]["live_area"]["VALUE"]) . '</value>';
        $strTmp .= '<unit>' . w2u($areaUnit) . '</unit>';
        $strTmp .= '</living-space>';
    } elseif (strlen($r["PROPERTY"]["rooms_of_transaction"]["VALUE"])) {
        $strTmp .= '<living-space>';
        $strTmp .= '<value>' . trim($r["PROPERTY"]["room_area"]["VALUE"]) . '</value>';
        $strTmp .= '<unit>' . w2u($areaUnit) . '</unit>';
        $strTmp .= '</living-space>';
    }

    if (strlen($r["PROPERTY"]["kitchen_area"]["VALUE"])) {
        $strTmp .= '<kitchen-space>';
        $strTmp .= '<value>' . trim($r["PROPERTY"]["kitchen_area"]["VALUE"]) . '</value>';
        $strTmp .= '<unit>' . w2u($areaUnit) . '</unit>';
        $strTmp .= '</kitchen-space>';
    }

    foreach ($schema as $s) {
        $s == '' or $strTmp .= '<image>' . $url . $s . '</image>';
    }
    empty(CFile::GetPath($r["DETAIL_PICTURE"])) or $strTmp .= '<image>' . $url . CFile::GetPath($r["DETAIL_PICTURE"]) . '</image>';
    foreach ($photo as $p) {
        $strTmp .= '<image>' . $url . $p . '</image>';
    }

    $strTmp .= '<description>' . w2u(str_replace("&nbsp;", " ", $r["PREVIEW_TEXT"])) . '</description>';

    if ($r["IBLOCK_SECTION_ID"] == CAT_NEW) {
        $strTmp .= '<new-flat>true</new-flat>';
        $strTmp .= '<deal-status>reassignment</deal-status>';
    }
    else {
        $strTmp .= '<deal-status>primary sale of secondary</deal-status>';
    }

    if ($rooms === 0) {
        $strTmp .= '<studio>true</studio>';
        $strTmp .= '<rooms>1</rooms>';
    } else {
        $strTmp .= '<rooms>' . $rooms . '</rooms>';
    }

    if (strlen($r["PROPERTY"]["rooms_of_transaction"]["VALUE"]))
        $strTmp .= '<rooms-offered>' . w2u($r["PROPERTY"]["rooms_of_transaction"]["VALUE"]) . '</rooms-offered>';

    if (strlen($r["PROPERTY"]["floor"]["VALUE"]))
        $strTmp .= '<floor>' . $r["PROPERTY"]["floor"]["VALUE"] . '</floor>';

    if (strlen($r["PROPERTY"]["floors"]["VALUE"]))
        $strTmp .= '<floors-total>' . $r["PROPERTY"]["floors"]["VALUE"] . '</floors-total>';

    if (strlen($r["PROPERTY"]["house_name"]["VALUE"]))
        $strTmp .= '<building-name>' . w2u($r["PROPERTY"]["house_name"]["VALUE"]) . '</building-name>';

    if (strlen($r["PROPERTY"]["term"]["VALUE"])) {
        $strTmp .= strtotime($r["PROPERTY"]["term"]["VALUE_XML_ID"]) > time()
            ? '<building-state>unfinished</building-state>'
            : '<building-state>hand-over</building-state>';
        $strTmp .= '<built-year>' . date("Y", strtotime($r["PROPERTY"]["term"]["VALUE_XML_ID"])) . '</built-year>';
        $strTmp .= '<ready-quarter>' .substr($r["PROPERTY"]["term"]["VALUE"], 0, 1) . '</ready-quarter>';
    }

    $strTmp .= '</offer>';
}

@fwrite($fp, $strTmp);

@fwrite($fp, '</realty-feed>');

@fclose($fp);

copy($_SERVER["DOCUMENT_ROOT"] . "/export/yandex_sber.xml", $_SERVER["DOCUMENT_ROOT"] . "/export/yandex.xml");

echo $updateTime;