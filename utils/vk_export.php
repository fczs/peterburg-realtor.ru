<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/autoload.php");
include("directory.php");
CModule::IncludeModule("iblock");
set_time_limit(0);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);

$VkImages = new \VkApi\UploadImages();
$VkMarket = new \VkApi\ManageItems();

$arFilter = Array(
    "IBLOCK_ID" => CATALOG_IBLOCK_ID,
    "ACTIVE" => "Y",
    "PROPERTY_EXPORT_VK" => "Y"
);

$arSelectFields = Array(
    "ID",
    "IBLOCK_ID",
    "NAME",
    "IBLOCK_SECTION_ID",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_TYPE",
    "PROPERTY_TYPE_OF_DEAL",
    "PROPERTY_PRICE",
    "PROPERTY_COMMERCIAL",
    "PROPERTY_SITE_AREA"
);

$elementsDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$elements = [];
while ($elem = $elementsDB->Fetch()) {
    $elements[] = $elem;
}

$arItemParams = [];
$image = [];

foreach ($elements as $r) {
    $sectionCode = CIBlockSection::GetByID($r["IBLOCK_SECTION_ID"])->GetNext()["CODE"];

    $catID = $CAT_FLAT;
    if ($r["PROPERTY_TYPE_ENUM_ID"] == PROP_ROOM_SALE) {
        $catID = $CAT_ROOM;
    } elseif ($r["IBLOCK_SECTION_ID"] == CAT_COMMERCIAL) {
        $catID = $CAT_COMM;
    } elseif ($r["PROPERTY_SITE_AREA_VALUE"]) {
        $catID = $CAT_SITE;
    } elseif (w2u($arCategory[$r["PROPERTY_TYPE_ENUM_ID"]]) == "дом") {
        $catID = $CAT_HOUSE;
    }

    $image = $VkImages->uploadPhoto("http://peterburg-realtor.ru" . CFile::GetPath($r["DETAIL_PICTURE"]));

    $photoIds = "";
    $i = 0;
    $photoDB = CIBlockElement::GetProperty($r["IBLOCK_ID"], $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    if(!empty($photoDB)) {
        while ($p = $photoDB->GetNext()) {
            $photo = $VkImages->uploadPhoto("http://peterburg-realtor.ru" . CFile::GetPath($p["VALUE"]));
            $photoIds .= $photo["pid"] . ",";
            if ($i++ == 3) break;
        }
    }

    $arItemParams = array(
        "name" => w2u($r["NAME"]),
        "description" => w2u(substr(strip_tags($r["PREVIEW_TEXT"]), 0, 680)) . "\n\nСсылка на объект: " . $urlCat . $sectionCode . '/' . $r["IBLOCK_SECTION_ID"] . '/' . $r["ID"] . "/",
        "category_id" => $catID,
        "price" => $r["PROPERTY_PRICE_VALUE"],
        "deleted" => "0",
        "main_photo_id" => $image["pid"],
        "photo_ids" => $photoIds
    );

    _r($image);
    _r($VkMarket->add($arItemParams));

    CIBlockElement::SetPropertyValuesEx($r["ID"], $r["IBLOCK_ID"], array("EXPORT_VK" => "N"));
}