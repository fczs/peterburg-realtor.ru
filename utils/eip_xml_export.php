<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);

$strTmp = "";
$aArray = Array();
$bArray = Array();
include("directory.php");

$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/export/eip.xml", "wb");

@fwrite($fp, '<?xml version="1.0" encoding="windows-1251"?>');
@fwrite($fp, '<eip>');

$arFilter = Array(
    "IBLOCK_ID" => "5",
    "ACTIVE" => "Y"
);

$arSelectFields = Array(
    "ID",
    "NAME",
    "IBLOCK_SECTION_ID",
    "PREVIEW_TEXT",
    "PROPERTY_AREA",
    "PROPERTY_CITY",
    "PROPERTY_DISTRICT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_ROOMS",
    "PROPERTY_METRO",
    "PROPERTY_TYPE",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_TYPE_OF_DEAL",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_SITE_AREA",
    "PROPERTY_LIVE_AREA",
    "PROPERTY_KITCHEN_AREA",
    "PROPERTY_BUILDING",
    "PROPERTY_FLOOR"
);

$rDB = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelectFields);

while($r = $rDB->Fetch()){
    $aArray[] = $r["ID"];
    $bArray[] = $r;
}

//echo print_r($bArray);

foreach($bArray as $r){
    $streetStr = ""; $what = "";
    $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
    if($streetEl = $streetElement->GetNext())
        $streetStr = $streetEl["NAME"];

    if($r["PROPERTY_AREA_VALUE"] == "������������� ���." || $r["PROPERTY_CITY_VALUE"] != "�����-��������� �.") {
        $region = $r["PROPERTY_CITY_VALUE"];
        if(strlen($streetStr))
            $streetStr = ', ' . $streetStr;
        $street = $r["PROPERTY_DISTRICT_VALUE"] . $streetStr;
    }
    else {
        $region = $r["PROPERTY_DISTRICT_VALUE"] . " �-�";
        $street = $streetStr;
    }

    if($r["PROPERTY_AREA_VALUE"] == "������������� ���.") {
        $city = "������������� ���.";
    }
    else {
        $city = "�-���������";
    }

    if(strlen($r["PROPERTY_METRO_VALUE"]))
        $metro = $arMetro[$r["PROPERTY_METRO_VALUE"]];
    else
        $metro = 57;

    if($r["IBLOCK_SECTION_ID"] == 9)
        $what = "���";
    elseif(strlen($r["PROPERTY_TYPE_VALUE"]))
        $what = $arWhat[$r["PROPERTY_TYPE_VALUE"]];
    else
        $what = $arWhat[$r["PROPERTY_NAMES_LIST_VALUE"]];

    $oper = 2;
    if($r["IBLOCK_SECTION_ID"] == 7)
        $oper = 5;
    if($r["IBLOCK_SECTION_ID"] == 5)
        $oper = $arOper[$r["PROPERTY_TYPE_OF_DEAL_VALUE"]];



    $strTmp .= '<rec>';
    $strTmp .= '<num_rec>' . $r["ID"] . '</num_rec>';
    $strTmp .= '<region>' . $region . '</region>';
    $strTmp .= '<address home="' . $r["PROPERTY_NUMBER_VALUE"] . '" corp="' . $r["PROPERTY_HOUSING_VALUE"] . '">' . $street . '</address>';
    $strTmp .= '<district>' . $city . '</district>';
    $strTmp .= '<n_room>' . $r["PROPERTY_ROOMS_VALUE"] . '</n_room>';
    $strTmp .= '<metro>' . $metro . '</metro>';
    $strTmp .= '<what>' . $what . '</what>';
    $strTmp .= '<oper>' . $oper . '</oper>';
    $strTmp .= '<price valuta="�." condition="">' . $r["PROPERTY_PRICE_VALUE"] . '</price>';
    $strTmp .= '<area>' . $r["PROPERTY_ALL_AREA_VALUE"] . '</area>';
    if(strlen($r["PROPERTY_SITE_AREA_VALUE"]))
        $strTmp .= '<landarea>' . $r["PROPERTY_SITE_AREA_VALUE"] . '</landarea>';
    $strTmp .= '<live>' . $r["PROPERTY_LIVE_AREA_VALUE"] . '</live>';
    $strTmp .= '<kitch>' . $r["PROPERTY_KITCHEN_AREA_VALUE"] . '</kitch>';
    $strTmp .= '<tip>' . $arBuilding[$r["PROPERTY_BUILDING_VALUE"]] . '</tip>';
    if(is_numeric($r["PROPERTY_FLOOR_VALUE"]))
        $strTmp .= '<floor>' . $r["PROPERTY_FLOOR_VALUE"] . '</floor>';
    $strTmp .= '<phone country_code="7" city_code="812">326-69-04</phone>';
    $strTmp .= '<name>������������� �������</name>';
    if(strlen($r["PREVIEW_TEXT"]) && strlen($r["PREVIEW_TEXT"]) < 251)
        $strTmp .= '<note>' . $r["PREVIEW_TEXT"] . '</note>';

    $strTmp .= '</rec>';
}

@fwrite($fp, $strTmp);

@fwrite($fp, '</eip>');

@fclose($fp);

echo date("H:i:s d.m.Y");

