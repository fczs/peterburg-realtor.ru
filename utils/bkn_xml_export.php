<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
set_time_limit(0);

$strTmp = "";
include("directory.php");

function makeArray($_)
{
    $aArray = [];
    while ($r = $_->Fetch()) {
        $aArray[] = $r;
    }
    return $aArray;
}

$fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/export/bkn.xml", "wb");

$strTmp .= '<?xml version="1.0" encoding="utf-8"?>';
$strTmp .= '<import>';

/**
 * Персонал
 */
$strTmp .= '<staffs>';
$order = array('sort' => 'asc');
$tmp = 'sort';
$filter = array("ACTIVE" => "Y");
$rsUsers = CUser::GetList($order, $tmp, $filter);
$dbArray = makeArray($rsUsers);

foreach ($dbArray as $r) {
    if ($r["ID"] == "1") continue;
    elseif ($r["ID"] == "11") {
        $role = "4";
        $position = "1";
        $structure = "1";
    } elseif ($r["ID"] == "3" || $r["ID"] == "4") {
        $role = "4";
        $position = "3";
        $structure = "1";
    } else {
        $role = "10";
        $position = "2";
        $structure = "1";
    }

    $date = date('Y.m.d', strtotime($r["DATE_REGISTER"]));

    if (w2u($r["PERSONAL_GENDER"] == "M"))
        $gender = 1;
    else
        $gender = 0;

    if (empty($r["SECOND_NAME"]))
        $secondName = "-";
    else
        $secondName = w2u($r["SECOND_NAME"]);

    $strTmp .= '<staff>';
    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<last_name>' . w2u($r["LAST_NAME"]) . '</last_name>';
    $strTmp .= '<first_name>' . w2u($r["NAME"]) . '</first_name>';
    $strTmp .= '<father_name>' . $secondName . '</father_name>';
    $strTmp .= '<sex>' . $gender . '</sex>';
    $strTmp .= '<role>' . $role . '</role>';
    $strTmp .= '<login>' . $r["LOGIN"] . '</login>';
    $strTmp .= '<password>' . $r["LOGIN"] . '123' . '</password>';

    $strTmp .= '<works>';
    $strTmp .= '<work>';
    $strTmp .= '<position>' . $position . '</position>';
    $strTmp .= '<structure>1</structure>';
    $strTmp .= '<date_begin>' . $date . '</date_begin>';
    $strTmp .= '</work>';
    $strTmp .= '</works>';

    $strTmp .= '<contacts>';
    $strTmp .= '<contact>';
    $strTmp .= '<type>3</type>';
    $strTmp .= '<value>' . $r["PERSONAL_PHONE"] . '</value>';
    $strTmp .= '<isMedia>1</isMedia>';
    $strTmp .= '</contact>';
    $strTmp .= '</contacts>';

    $strTmp .= '</staff>';
}
$strTmp .= '</staffs>';

/**
 * Вторичка
 */
$strTmp .= '<second>';
$arFilter = Array(
    "IBLOCK_ID" => "5",
    "SECTION_ID" => "6",
    "ACTIVE" => "Y"
);
$arSelectFields = Array(
    "ID",
    "NAME",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_AREA",
    "PROPERTY_CITY",
    "PROPERTY_DISTRICT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_ROOMS",
    "PROPERTY_ROOMS_OF_TRANSACTION",
    "PROPERTY_ROOMS_IN_FLAT",
    "PROPERTY_METRO",
    "PROPERTY_TYPE",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_LIVE_AREA",
    "PROPERTY_KITCHEN_AREA",
    "PROPERTY_BUILDING",
    "PROPERTY_FLOOR",
    "PROPERTY_FLOORS",
    "PROPERTY_YEARS",
    "PROPERTY_REALTOR",
    "PROPERTY_ROOMS_AREA"
);
$rDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$dbArray = makeArray($rDB);

foreach ($dbArray as $r) {
    $saleRooms = "";
    $address = "";
    if (w2u($r["PROPERTY_TYPE_VALUE"]) == "Комната") {
        $type = 2;
        $countRooms = str_replace("+", "", $r["PROPERTY_ROOMS_IN_FLAT_VALUE"]);
        $saleRooms = $r["PROPERTY_ROOMS_OF_TRANSACTION_VALUE"];
    } else {
        if (w2u($r["PROPERTY_ROOMS_VALUE"]) == "Студия") {
            $type = 3;
            $countRooms = 1;
        } else {
            $type = 1;
            $countRooms = str_replace("+", "", $r["PROPERTY_ROOMS_VALUE"]);
        }
    }

    if ($r["PROPERTY_AREA_ENUM_ID"] == 489) {
        $area = 5001; // ЛенОбл
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } elseif ($r["PROPERTY_CITY_ENUM_ID"] == 475 || $r["PROPERTY_CITY_ENUM_ID"] == 476 || $r["PROPERTY_CITY_ENUM_ID"] == 477 || $r["PROPERTY_CITY_ENUM_ID"] == 481 || $r["PROPERTY_CITY_ENUM_ID"] == 484) {
        $area = 1; // СПб
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } else {
        $area = 1; // СПб
        $city = "Санкт-Петербург";
    }

    if ($arDistrict[$r["PROPERTY_CITY_ENUM_ID"]])
        $district = $arDistrict[$r["PROPERTY_CITY_ENUM_ID"]];
    else
        $district = $arDistrict[$r["PROPERTY_DISTRICT_ENUM_ID"]];

    if (strlen($r["PROPERTY_ADDRESS_VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    $strTmp .= '<object>';
    $strTmp .= '<exportType>3</exportType>';
    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<objType>' . $type . '</objType>';
    $strTmp .= '<countRooms>' . $countRooms . '</countRooms>';
    if ($saleRooms)
        $strTmp .= '<saleRooms>' . $saleRooms . '</saleRooms>';
    $strTmp .= '<note>' . w2u(substr($r["PREVIEW_TEXT"], 0, 2000)) . '</note>';
    $strTmp .= '<staffId>' . $r["PROPERTY_REALTOR_VALUE"] . '</staffId>';

    $strTmp .= '<location>';
    $strTmp .= '<region>' . $area . '</region>';
    $strTmp .= '<district>' . $district . '</district>';
    $strTmp .= '<city>' . $city . '</city>';
    $strTmp .= '<street>' . $address . '</street>';
    $strTmp .= '<house>' . w2u($r["PROPERTY_NUMBER_VALUE"]) . '</house>';
    $strTmp .= '</location>';

    $strTmp .= '<transport>';
    $strTmp .= '<subway>' . $arSubway[$r["PROPERTY_METRO_ENUM_ID"]] . '</subway>';
    $strTmp .= '</transport>';

    $strTmp .= '<agreement>';
    $strTmp .= '<type>2</type>'; //Есть договор
    $strTmp .= '</agreement>';

    $strTmp .= '<price>';
    $strTmp .= '<amount>' . floor($r["PROPERTY_PRICE_VALUE"] / 1000) . '</amount>';
    $strTmp .= '</price>';

    // По нечетным числам пишем, что комиссия 0%. Сделано для того, чтобы в базе БКН информация по объекту менялась и
    // он не уходил в архив.
    if (date('j') % 2 != 0) {
        $strTmp .= '<comission>';
        $strTmp .= '<comType>1</comType>';
        $strTmp .= '<comValue>0</comValue>';
        $strTmp .= '</comission>';
    }

    $strTmp .= '<square>';
    $strTmp .= '<all>' . str_replace(",", ".", $r["PROPERTY_ALL_AREA_VALUE"]) . '</all>';
    $strTmp .= '<living>' . str_replace(",", ".", $r["PROPERTY_LIVE_AREA_VALUE"]) . '</living>';
    $strTmp .= '<kitchen>' . str_replace(",", ".", $r["PROPERTY_KITCHEN_AREA_VALUE"]) . '</kitchen>';
    $strTmp .= '<rooms>' . str_replace(",", ".", $r["PROPERTY_ROOMS_AREA_VALUE"]) . '</rooms>';
    $strTmp .= '</square>';

    $strTmp .= '<floor>' . $r["PROPERTY_FLOOR_VALUE"] . '</floor>';
    $strTmp .= '<floors>' . $r["PROPERTY_FLOORS_VALUE"] . '</floors>';
    $strTmp .= '<houseType>' . $houseType[$r["PROPERTY_BUILDING_ENUM_ID"]] . '</houseType>';
    if (strlen($r["PROPERTY_YEARS_VALUE"]))
        $strTmp .= '<yearBuilding>' . $r["PROPERTY_YEARS_VALUE"] . '</yearBuilding>';

    $strTmp .= '<images>';
    $schema_old = false; // У объекта заполнено старое свойство для изображений планировки
    if ($schema["0"]) {
        foreach ($schema as $s) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $s . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '<isPlan>1</isPlan>';
            $strTmp .= '</image>';
        }
        $schema_old = true;
    }
    if (CFile::GetPath($r["DETAIL_PICTURE"])) {
        $strTmp .= '<image>';
        $strTmp .= '<url>http://peterburg-realtor.ru' . CFile::GetPath($r["DETAIL_PICTURE"]) . '</url>';
        $strTmp .= '<isExport>1</isExport>';
        if (!$schema_old)
            $strTmp .= '<isPlan>1</isPlan>';
        $strTmp .= '</image>';
    }
    foreach ($photo as $p) {
        if ($p) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $p . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '</image>';
        }
    }
    $strTmp .= '</images>';

    $strTmp .= '</object>';
}

$strTmp .= '</second>';

/**
 * Новостройки
 */
$strTmp .= '<primary>';
$arFilter = Array(
    "IBLOCK_ID" => "5",
    "SECTION_ID" => "9",
    "ACTIVE" => "Y",
    "PROPERTY_RESALE" => "Y" // новостройки только по переуступке
);
$arSelectFields = Array(
    "ID",
    "NAME",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_ROOMS",
    "PROPERTY_DEVELOPER_NEW",
    "PROPERTY_HOUSE_NAME",
    "PROPERTY_RESALE",
    "PROPERTY_TYPE",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_LIVE_AREA",
    "PROPERTY_KITCHEN_AREA",
    "PROPERTY_BUILDING",
    "PROPERTY_FLOOR",
    "PROPERTY_FLOORS",
    "PROPERTY_REALTOR",
    "PROPERTY_ROOMS_AREA"
);
$rDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$dbArray = makeArray($rDB);

foreach ($dbArray as $r) {
    $address = "";
    $importBlock = "";

    if ($r["PROPERTY_HOUSING_VALUE"]) {
        $importBlock = w2u($r["PROPERTY_HOUSE_NAME_ENUM_ID"]) . "-" . $r["PROPERTY_HOUSING_VALUE"];
    } else {
        $importBlock = w2u($r["PROPERTY_HOUSE_NAME_ENUM_ID"]);
    }

    if (w2u($r["PROPERTY_ROOMS_VALUE"]) == "Студия") {
        $type = 3;
        $countRooms = 1;
    } else {
        $type = 1;
        $countRooms = str_replace("+", "", $r["PROPERTY_ROOMS_VALUE"]);
    }

    if (strlen($r["PROPERTY_ADDRESS_VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    if ($r["PROPERTY_RESALE_VALUE"] == "Y")
        $assignment = 1;
    else
        $assignment = 0;

    $strTmp .= '<object>';
    $strTmp .= '<exportType>3</exportType>';
    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<objType>' . $type . '</objType>';
    $strTmp .= '<note>' . w2u(substr($r["PREVIEW_TEXT"], 0, 2000)) . '</note>';
    $strTmp .= '<staffId>' . $r["PROPERTY_REALTOR_VALUE"] . '</staffId>';

    $strTmp .= '<agreement>';
    // По четным числам пишем, что нет договора, по нечетным — договор на рекламу.
    // Сделано для того, чтобы в базе БКН информация по объекту менялась и он не уходил в архив.
    // У новостроек проавла комиссия, поэтому с ней не получается.
    $agreementType = date('j') % 2 == 0 ? 1 : 2;
    $strTmp .= '<type>' . $agreementType . '</type>'; //Есть договор
    $strTmp .= '</agreement>';

    $strTmp .= '<importBlock>' . $importBlock . '</importBlock>';
    $strTmp .= '<developer>' . w2u($r["PROPERTY_DEVELOPER_NEW_VALUE"]) . '</developer>';
    $strTmp .= '<blockName>' . w2u($r["PROPERTY_HOUSE_NAME_VALUE"]) . '</blockName>';
    $strTmp .= '<blockAddress>' . $address . ', ' . $r["PROPERTY_NUMBER_VALUE"] . '</blockAddress>';

    $strTmp .= '<price>';
    $strTmp .= '<amount>' . floor($r["PROPERTY_PRICE_VALUE"] / 1000) . '</amount>';
    $strTmp .= '</price>';

    $strTmp .= '<square>';
    $strTmp .= '<all>' . str_replace(",", ".", $r["PROPERTY_ALL_AREA_VALUE"]) . '</all>';
    $strTmp .= '<living>' . str_replace(",", ".", $r["PROPERTY_LIVE_AREA_VALUE"]) . '</living>';
    $strTmp .= '<kitchen>' . str_replace(",", ".", $r["PROPERTY_KITCHEN_AREA_VALUE"]) . '</kitchen>';
    $strTmp .= '<rooms>' . str_replace(",", ".", $r["PROPERTY_ROOMS_AREA_VALUE"]) . '</rooms>';
    $strTmp .= '</square>';

    $strTmp .= '<floor>' . $r["PROPERTY_FLOOR_VALUE"] . '</floor>';
    $strTmp .= '<assignment>' . $assignment . '</assignment>';
    $strTmp .= '<countRooms>' . $countRooms . '</countRooms>';

    $strTmp .= '<images>';
    $schema_old = false; // У объекта заполнено старое свойство для изображений планировки
    if ($schema["0"]) {
        foreach ($schema as $s) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $s . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '<isPlan>1</isPlan>';
            $strTmp .= '</image>';
        }
        $schema_old = true;
    }
    if (CFile::GetPath($r["DETAIL_PICTURE"])) {
        $strTmp .= '<image>';
        $strTmp .= '<url>http://peterburg-realtor.ru' . CFile::GetPath($r["DETAIL_PICTURE"]) . '</url>';
        $strTmp .= '<isExport>1</isExport>';
        if (!$schema_old)
            $strTmp .= '<isPlan>1</isPlan>';
        $strTmp .= '</image>';
    }
    foreach ($photo as $p) {
        if ($p) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $p . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '</image>';
        }
    }
    $strTmp .= '</images>';

    $strTmp .= '</object>';
}

$strTmp .= '</primary>';

/**
 * Загородная
 */
$strTmp .= '<zagorod>';
$arFilter = Array(
    "IBLOCK_ID" => "5",
    "SECTION_ID" => "8",
    "ACTIVE" => "Y"
);
$arSelectFields = Array(
    "ID",
    "NAME",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_AREA",
    "PROPERTY_CITY",
    "PROPERTY_DISTRICT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_ROOMS",
    "PROPERTY_TYPE",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_LIVE_AREA",
    "PROPERTY_KITCHEN_AREA",
    "PROPERTY_BUILDING",
    "PROPERTY_FLOOR",
    "PROPERTY_FLOORS",
    "PROPERTY_REALTOR",
    "PROPERTY_COUNTRY_TYPE",
    "PROPERTY_SITE_AREA"
);
$rDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$dbArray = makeArray($rDB);

foreach ($dbArray as $r) {
    $type = "";
    $address = "";

    $res = CIBlockElement::GetProperty("5", $r["ID"], "sort", "asc", array("ID" => "92"));
    if ($ar_res = $res->GetNext())
        $type = $ar_res["VALUE_XML_ID"];

    if ($r["PROPERTY_AREA_ENUM_ID"] == 489) {
        $area = 5001; // ЛенОбл
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } elseif ($r["PROPERTY_CITY_ENUM_ID"] == 475 || $r["PROPERTY_CITY_ENUM_ID"] == 476 || $r["PROPERTY_CITY_ENUM_ID"] == 477 || $r["PROPERTY_CITY_ENUM_ID"] == 481 || $r["PROPERTY_CITY_ENUM_ID"] == 484) {
        $area = 1; // СПб
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } else {
        $area = 1; // СПб
        $city = "Санкт-Петербург";
    }

    if ($arDistrict[$r["PROPERTY_CITY_ENUM_ID"]])
        $district = $arDistrict[$r["PROPERTY_CITY_ENUM_ID"]];
    else
        $district = $arDistrict[$r["PROPERTY_DISTRICT_ENUM_ID"]];

    if (strlen($r["PROPERTY_ADDRESS_VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    $strTmp .= '<object>';

    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<exportType>3</exportType>';
    $strTmp .= '<objType>' . $type . '</objType>';
    $strTmp .= '<note>' . w2u(substr($r["PREVIEW_TEXT"], 0, 2000)) . '</note>';
    $strTmp .= '<staffId>' . $r["PROPERTY_REALTOR_VALUE"] . '</staffId>';

    $strTmp .= '<location>';
    $strTmp .= '<region>' . $area . '</region>';
    $strTmp .= '<district>' . $district . '</district>';
    $strTmp .= '<city>' . $city . '</city>';
    $strTmp .= '<street>' . $address . '</street>';
    $strTmp .= '<house>' . w2u($r["PROPERTY_NUMBER_VALUE"]) . '</house>';
    $strTmp .= '</location>';

    $strTmp .= '<agreement>';
    $strTmp .= '<type>2</type>'; //Есть договор
    $strTmp .= '</agreement>';

    $strTmp .= '<price>';
    $strTmp .= '<amount>' . floor($r["PROPERTY_PRICE_VALUE"] / 1000) . '</amount>';
    $strTmp .= '</price>';

    // По нечетным числам пишем, что комиссия 0%. Сделано для того, чтобы в базе БКН информация по объекту менялась и
    // он не уходил в архив.
    if (date('j') % 2 != 0) {
        $strTmp .= '<comission>';
        $strTmp .= '<comType>1</comType>';
        $strTmp .= '<comValue>0</comValue>';
        $strTmp .= '</comission>';
    }

    $strTmp .= '<square>';
    if ($r["PROPERTY_SITE_AREA_VALUE"]) {
        $strTmp .= '<land>' . $r["PROPERTY_SITE_AREA_VALUE"] . '</land>';
        $strTmp .= '<landType>3</landType>';
    }
    $strTmp .= '<all>' . str_replace(",", ".", $r["PROPERTY_ALL_AREA_VALUE"]) . '</all>';
    $strTmp .= '<living>' . str_replace(",", ".", $r["PROPERTY_LIVE_AREA_VALUE"]) . '</living>';
    $strTmp .= '<kitchen>' . str_replace(",", ".", $r["PROPERTY_KITCHEN_AREA_VALUE"]) . '</kitchen>';
    $strTmp .= '</square>';

    $strTmp .= '<floors>' . $r["PROPERTY_FLOORS_VALUE"] . '</floors>';

    $strTmp .= '<images>';
    $schema_old = false; // У объекта заполнено старое свойство для изображений планировки
    if ($schema["0"]) {
        foreach ($schema as $s) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $s . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '<isPlan>1</isPlan>';
            $strTmp .= '</image>';
        }
        $schema_old = true;
    }
    if (CFile::GetPath($r["DETAIL_PICTURE"])) {
        $strTmp .= '<image>';
        $strTmp .= '<url>http://peterburg-realtor.ru' . CFile::GetPath($r["DETAIL_PICTURE"]) . '</url>';
        $strTmp .= '<isExport>1</isExport>';
        if (!$schema_old)
            $strTmp .= '<isPlan>1</isPlan>';
        $strTmp .= '</image>';
    }
    foreach ($photo as $p) {
        if ($p) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $p . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '</image>';
        }
    }
    $strTmp .= '</images>';

    $strTmp .= '</object>';
}

$strTmp .= '</zagorod>';

/**
 * Аренда
 */
$strTmp .= '<rent>';
$arFilter = Array(
    "IBLOCK_ID" => "5",
    "SECTION_ID" => "7",
    "ACTIVE" => "Y"
);
$arSelectFields = Array(
    "ID",
    "NAME",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_AREA",
    "PROPERTY_CITY",
    "PROPERTY_DISTRICT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_ROOMS",
    "PROPERTY_ROOMS_OF_TRANSACTION",
    "PROPERTY_ROOMS_IN_FLAT",
    "PROPERTY_METRO",
    "PROPERTY_TYPE",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_LIVE_AREA",
    "PROPERTY_KITCHEN_AREA",
    "PROPERTY_BUILDING",
    "PROPERTY_FLOOR",
    "PROPERTY_FLOORS",
    "PROPERTY_YEARS",
    "PROPERTY_REALTOR",
    "PROPERTY_ROOMS_AREA"
);
$rDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$dbArray = makeArray($rDB);

foreach ($dbArray as $r) {
    $saleRooms = "";
    $address = "";
    if (w2u($r["PROPERTY_TYPE_VALUE"]) == "Комната") {
        $type = 2;
        $countRooms = str_replace("+", "", $r["PROPERTY_ROOMS_IN_FLAT_VALUE"]);
        $saleRooms = $r["PROPERTY_ROOMS_OF_TRANSACTION_VALUE"];
    } else {
        if (w2u($r["PROPERTY_ROOMS_VALUE"]) == "Студия") {
            $type = 7;
            $countRooms = 1;
        } else {
            $type = 1;
            $countRooms = str_replace("+", "", $r["PROPERTY_ROOMS_VALUE"]);
        }
    }

    if ($r["PROPERTY_AREA_ENUM_ID"] == 489) {
        $area = 5001; // ЛенОбл
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } elseif ($r["PROPERTY_CITY_ENUM_ID"] == 475 || $r["PROPERTY_CITY_ENUM_ID"] == 476 || $r["PROPERTY_CITY_ENUM_ID"] == 477 || $r["PROPERTY_CITY_ENUM_ID"] == 481 || $r["PROPERTY_CITY_ENUM_ID"] == 484) {
        $area = 1; // СПб
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } else {
        $area = 1; // СПб
        $city = "Санкт-Петербург";
    }

    if ($arDistrict[$r["PROPERTY_CITY_ENUM_ID"]])
        $district = $arDistrict[$r["PROPERTY_CITY_ENUM_ID"]];
    else
        $district = $arDistrict[$r["PROPERTY_DISTRICT_ENUM_ID"]];

    if (strlen($r["PROPERTY_ADDRESS_VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    $strTmp .= '<object>';
    $strTmp .= '<exportType>3</exportType>';
    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<objType>' . $type . '</objType>';
    $strTmp .= '<countRooms>' . $countRooms . '</countRooms>';
    if ($saleRooms)
        $strTmp .= '<saleRooms>' . $saleRooms . '</saleRooms>';
    $strTmp .= '<leaseType>1</leaseType>';
    $strTmp .= '<lease>3</lease>';
    $strTmp .= '<note>' . w2u(substr($r["PREVIEW_TEXT"], 0, 2000)) . '</note>';
    $strTmp .= '<staffId>' . $r["PROPERTY_REALTOR_VALUE"] . '</staffId>';

    $strTmp .= '<location>';
    $strTmp .= '<region>' . $area . '</region>';
    $strTmp .= '<district>' . $district . '</district>';
    $strTmp .= '<city>' . $city . '</city>';
    $strTmp .= '<street>' . $address . '</street>';
    $strTmp .= '<house>' . w2u($r["PROPERTY_NUMBER_VALUE"]) . '</house>';
    $strTmp .= '</location>';

    $strTmp .= '<transport>';
    $strTmp .= '<subway>' . $arSubway[$r["PROPERTY_METRO_ENUM_ID"]] . '</subway>';
    $strTmp .= '</transport>';

    $strTmp .= '<agreement>';
    $strTmp .= '<type>2</type>'; //Есть договор
    $strTmp .= '</agreement>';

    $strTmp .= '<price>';
    $strTmp .= '<amountMonth>' . $r["PROPERTY_PRICE_VALUE"] . '</amountMonth>';
    $strTmp .= '</price>';

    // По четным нечислам пишем, что комиссия 0%. Сделано для того, чтобы в базе БКН информация по объекту менялась и
    // он не уходил в архив.
    if (date('j') % 2 != 0) {
        $strTmp .= '<comission>';
        $strTmp .= '<comType>1</comType>';
        $strTmp .= '<comValue>0</comValue>';
        $strTmp .= '</comission>';
    }

    $strTmp .= '<square>';
    $strTmp .= '<all>' . str_replace(",", ".", $r["PROPERTY_ALL_AREA_VALUE"]) . '</all>';
    $strTmp .= '<living>' . str_replace(",", ".", $r["PROPERTY_LIVE_AREA_VALUE"]) . '</living>';
    $strTmp .= '<kitchen>' . str_replace(",", ".", $r["PROPERTY_KITCHEN_AREA_VALUE"]) . '</kitchen>';
    $strTmp .= '<rooms>' . str_replace(",", ".", $r["PROPERTY_ROOMS_AREA_VALUE"]) . '</rooms>';
    $strTmp .= '</square>';

    $strTmp .= '<floor>' . $r["PROPERTY_FLOOR_VALUE"] . '</floor>';
    $strTmp .= '<floors>' . $r["PROPERTY_FLOORS_VALUE"] . '</floors>';
    $strTmp .= '<houseType>' . $houseType[$r["PROPERTY_BUILDING_ENUM_ID"]] . '</houseType>';
    if (strlen($r["PROPERTY_YEARS_VALUE"]))
        $strTmp .= '<yearBuilding>' . $r["PROPERTY_YEARS_VALUE"] . '</yearBuilding>';

    $strTmp .= '<images>';
    $schema_old = false; // У объекта заполнено старое свойство для изображений планировки
    if ($schema["0"]) {
        foreach ($schema as $s) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $s . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '<isPlan>1</isPlan>';
            $strTmp .= '</image>';
        }
        $schema_old = true;
    }
    if (CFile::GetPath($r["DETAIL_PICTURE"])) {
        $strTmp .= '<image>';
        $strTmp .= '<url>http://peterburg-realtor.ru' . CFile::GetPath($r["DETAIL_PICTURE"]) . '</url>';
        $strTmp .= '<isExport>1</isExport>';
        if (!$schema_old)
            $strTmp .= '<isPlan>1</isPlan>';
        $strTmp .= '</image>';
    }
    foreach ($photo as $p) {
        if ($p) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $p . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '</image>';
        }
    }
    $strTmp .= '</images>';

    $strTmp .= '</object>';
}

$strTmp .= '</rent>';


/**
 * Коммерческая
 */
$strTmp .= '<commerc>';
$arFilter = Array(
    "IBLOCK_ID" => "5",
    "SECTION_ID" => "5",
    "ACTIVE" => "Y"
);
$arSelectFields = Array(
    "ID",
    "NAME",
    "DETAIL_PICTURE",
    "PREVIEW_TEXT",
    "PROPERTY_AREA",
    "PROPERTY_COMMERCIAL",
    "PROPERTY_TYPE_OF_DEAL",
    "PROPERTY_CITY",
    "PROPERTY_DISTRICT",
    "PROPERTY_NUMBER",
    "PROPERTY_HOUSING",
    "PROPERTY_ADDRESS",
    "PROPERTY_METRO",
    "PROPERTY_NAMES_LIST",
    "PROPERTY_PRICE",
    "PROPERTY_ALL_AREA",
    "PROPERTY_SITE_AREA",
    "PROPERTY_REALTOR"
);
$rDB = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelectFields);
$dbArray = makeArray($rDB);

foreach ($dbArray as $r) {

    $address = "";
    $type = "";

    $res = CIBlockElement::GetProperty("5", $r["ID"], "sort", "asc", array("ID" => "56"));
    if ($ar_res = $res->GetNext())
        $type = $ar_res["VALUE_XML_ID"];

    if ($r["PROPERTY_TYPE_OF_DEAL_ENUM_ID"] == 513)
        $operType = 2;
    else
        $operType = 1;

    if ($r["PROPERTY_AREA_ENUM_ID"] == 489) {
        $area = 5001; // ЛенОбл
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } elseif ($r["PROPERTY_CITY_ENUM_ID"] == 475 || $r["PROPERTY_CITY_ENUM_ID"] == 476 || $r["PROPERTY_CITY_ENUM_ID"] == 477 || $r["PROPERTY_CITY_ENUM_ID"] == 481 || $r["PROPERTY_CITY_ENUM_ID"] == 484) {
        $area = 1; // СПб
        $city = w2u($r["PROPERTY_DISTRICT_VALUE"]);
    } else {
        $area = 1; // СПб
        $city = "Санкт-Петербург";
    }

    if ($arDistrict[$r["PROPERTY_CITY_ENUM_ID"]])
        $district = $arDistrict[$r["PROPERTY_CITY_ENUM_ID"]];
    else
        $district = $arDistrict[$r["PROPERTY_DISTRICT_ENUM_ID"]];

    if (strlen($r["PROPERTY_ADDRESS_VALUE"])) {
        $streetElement = CIBlockElement::GetByID($r["PROPERTY_ADDRESS_VALUE"]);
        if ($street = $streetElement->GetNext())
            $address = w2u($street["NAME"]);
    }

    $photo = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "PHOTO"));
    while ($ob = $dbRes->GetNext()) {
        $photo[] = CFile::GetPath($ob["VALUE"]);
    }

    $schema = array();
    $dbRes = CIBlockElement::GetProperty(5, $r["ID"], "sort", "asc", array("CODE" => "SCHEM"));
    while ($ob = $dbRes->GetNext()) {
        $schema[] = CFile::GetPath($ob["VALUE"]);
    }

    $strTmp .= '<object>';
    $strTmp .= '<exportType>3</exportType>';
    $strTmp .= '<id>' . $r["ID"] . '</id>';
    $strTmp .= '<objType>' . $type . '</objType>';
    $strTmp .= '<operType>' . $operType . '</operType>';

    $strTmp .= '<note>' . w2u(substr($r["PREVIEW_TEXT"], 0, 2000)) . '</note>';
    $strTmp .= '<staffId>' . $r["PROPERTY_REALTOR_VALUE"] . '</staffId>';

    $strTmp .= '<location>';
    $strTmp .= '<region>' . $area . '</region>';
    $strTmp .= '<district>' . $district . '</district>';
    $strTmp .= '<city>' . $city . '</city>';
    $strTmp .= '<street>' . $address . '</street>';
    $strTmp .= '<house>' . w2u($r["PROPERTY_NUMBER_VALUE"]) . '</house>';
    $strTmp .= '</location>';

    $strTmp .= '<transport>';
    $strTmp .= '<subway>' . $arSubway[$r["PROPERTY_METRO_ENUM_ID"]] . '</subway>';
    $strTmp .= '</transport>';

    $strTmp .= '<agreement>';
    $strTmp .= '<type>2</type>'; //Есть договор
    $strTmp .= '</agreement>';

    $strTmp .= '<price>';
    $strTmp .= '<amountType>2</amountType>';
    if ($operType == 1)
        $strTmp .= '<amount>' . $r["PROPERTY_PRICE_VALUE"] . '</amount>';
    else
        $strTmp .= '<rentCost>' . $r["PROPERTY_PRICE_VALUE"] . '</rentCost>';
    $strTmp .= '</price>';

    // По нечетным числам пишем, что комиссия 0%. Сделано для того, чтобы в базе БКН информация по объекту менялась и
    // он не уходил в архив.
    if (date('j') % 2 != 0) {
        $strTmp .= '<comission>';
        $strTmp .= '<comType>1</comType>';
        $strTmp .= '<comValue>0</comValue>';
        $strTmp .= '</comission>';
    }

    $strTmp .= '<square>';
    $strTmp .= '<all>' . str_replace(",", ".", $r["PROPERTY_ALL_AREA_VALUE"]) . '</all>';
    if ($r["PROPERTY_SITE_AREA_VALUE"]) {
        if ($r["PROPERTY_SITE_AREA_VALUE"] > 100) {
            $strTmp .= '<land>' . round($r["PROPERTY_SITE_AREA_VALUE"] / 100, 2) . '</land>';
            $strTmp .= '<landType>1</landType>';
        } else {
            $strTmp .= '<land>' . $r["PROPERTY_SITE_AREA_VALUE"] . '</land>';
            $strTmp .= '<landType>3</landType>';
        }
    }
    $strTmp .= '</square>';

    $strTmp .= '<images>';
    $schema_old = false; // У объекта заполнено старое свойство для изображений планировки
    if ($schema["0"]) {
        foreach ($schema as $s) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $s . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '<isPlan>1</isPlan>';
            $strTmp .= '</image>';
        }
        $schema_old = true;
    }
    if (CFile::GetPath($r["DETAIL_PICTURE"])) {
        $strTmp .= '<image>';
        $strTmp .= '<url>http://peterburg-realtor.ru' . CFile::GetPath($r["DETAIL_PICTURE"]) . '</url>';
        $strTmp .= '<isExport>1</isExport>';
        if (!$schema_old)
            $strTmp .= '<isPlan>1</isPlan>';
        $strTmp .= '</image>';
    }
    foreach ($photo as $p) {
        if ($p) {
            $strTmp .= '<image>';
            $strTmp .= '<url>http://peterburg-realtor.ru' . $p . '</url>';
            $strTmp .= '<isExport>1</isExport>';
            $strTmp .= '</image>';
        }
    }
    $strTmp .= '</images>';

    $strTmp .= '</object>';
}

$strTmp .= '</commerc>';

@fwrite($fp, $strTmp);

@fwrite($fp, '</import>');

@fclose($fp);

echo date("H:i:s d.m.Y");

