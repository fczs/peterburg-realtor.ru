<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die(0); //������ ajax ������

CModule::IncludeModule("iblock");

if(strlen($_POST["from"]) && strlen($_POST["to"]))
{
    $i = 0;
    $dbElements = CIBlockElement::GetList(Array("NAME" => "ASC"), Array('IBLOCK_ID' => 5, 'PROPERTY_REALTOR' => $_POST["from"]), false, false, Array("ID", "NAME", "IBLOCK_SECTION_ID"));
    while ($elem = $dbElements->Fetch())
    {
        $i++;
        CIBlockElement::SetPropertyValuesEx($elem["ID"], false, array("realtor" => $_POST["to"]));
    }
    if($i == 0)
        echo "��� �������� ��� ��������";
    else
        echo "������� ������� ����������";
}
else
{
    die("��� �������� ��� ��������");
}