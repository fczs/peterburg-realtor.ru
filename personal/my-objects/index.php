<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "��� �������");
$APPLICATION->SetTitle("��� �������");

if ($USER->IsAuthorized()) {

    $APPLICATION->IncludeComponent(
        "bitrix:iblock.element.add.list",
        "gp_list_element",
        array(
            "EDIT_URL" => SITE_DIR . "personal/add/",
            "NAV_ON_PAGE" => "10000000",
            "MAX_USER_ENTRIES" => "100000",
            "IBLOCK_TYPE" => "gp_realestate_catalog",
            "IBLOCK_ID" => "5",
            "GROUPS" => array(
                0 => "1",
                1 => "5",
                2 => "7",
            ),
            "STATUS" => "ANY",
            "ELEMENT_ASSOC" => "PROPERTY_ID",
            "ALLOW_EDIT" => "Y",
            "ALLOW_DELETE" => "Y",
            "SEF_MODE" => "N",
            "SEF_FOLDER" => SITE_DIR . "personal/my-objects/",
            "ELEMENT_ASSOC_PROPERTY" => "51",
        ),
        false);
}?>
    <br/>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>