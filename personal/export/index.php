<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������");

$content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/export/yandex_sber.xml");
preg_match('|<generation-date>(.+)T(.+)\+(.+)</generation-date>|isU', $content, $matches);
$sTime =  date("H:i:s d.m.Y", strtotime(($matches[1] . " " . $matches[2])));

$content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/export/yandex_feed.xml");
preg_match('|<generation-date>(.+)T(.+)\+(.+)</generation-date>|isU', $content, $matches);
$yTime =  date("H:i:s d.m.Y", strtotime(($matches[1] . " " . $matches[2])));

$content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/export/glavbaza_feed.xml");
preg_match('|<generation-date>(.+)T(.+)\+(.+)</generation-date>|isU', $content, $matches);
$gTime =  date("H:i:s d.m.Y", strtotime(($matches[1] . " " . $matches[2])));

$bTime = date("H:i:s d.m.Y", filemtime($_SERVER["DOCUMENT_ROOT"] . "/export/bkn.xml"));
$eTime = date("H:i:s d.m.Y", filemtime($_SERVER["DOCUMENT_ROOT"] . "/export/eip.xml"));

if ($USER->isAdmin()): ?>
    <div id="object-table">
        ���� �������� ������.������������ ��������: <span><?= $yTime ?></span>
        <input id="update-y" type="button" value="��������" data-path="/utils/yandex_xml_export.php">
    </div>
    <div id="object-table">
        ���� �������� �������� ��������: <span><?= $sTime ?></span>
        <input id="update-s" type="button" value="��������" data-path="/utils/sber_xml_export.php">
    </div>
    <div id="object-table">
        ���� �������� ��������: <span><?= $gTime ?></span>
        <input id="update-g" type="button" value="��������" data-path="/utils/glavbaza_xml_export.php">
    </div>
    <div id="object-table">
        ���� �������� � ������� ��� ��������: <span><?= $bTime ?></span>
        <input id="update-b" type="button" value="��������" data-path="/utils/bkn_xml_export.php">
    </div>
    <div id="object-table">
        ���� �������� � ������� EIP ��������: <span><?= $eTime ?></span>
        <input id="update-e" type="button" value="��������" data-path="/utils/eip_xml_export.php">
    </div>
    <div id="object-table">
        ��������� � ��: <span style="display: none"></span>
        <input id="update-v" type="button" value="���������" data-path="/utils/vk_export.php">
    </div>


    <script>
        $(document).ready(function() {
            $('[type="button"]').click(function() {
                var button = $(this),
                    time = button.parent().find('span'),
                    path = button.data('path');
                button.addClass('ajax_loader');
                time.text("00:00:00 00.00.0000");

                $.post(path, function(response) {
                    time.text(response);
                    button.removeClass('ajax_loader');
                });
            });
        })
    </script>
<? endif;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");