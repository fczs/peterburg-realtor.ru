<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>
<? $APPLICATION->SetPageProperty("title", "������� ��������"); ?>

<? if ($USER->isAdmin()): ?>
    <div class="list-filter">
        <label for="agent-from">��������� ������� �:</label>
        <select id="agent-from" class="object-select" data-child="4">
            <option value="">..</option>
            <?
            $dbUsers = CUser::GetList($sortBy = "LAST_NAME", $sortOrder = "ASC", array("GROUPS_ID" => "7"));
            while ($arUser = $dbUsers->Fetch()):?>
                <option value="<?= $arUser["ID"] ?>"><?= $arUser["LAST_NAME"] ?></option>
            <? endwhile; ?>
        </select>
        <label for="agent-to">��:</label>
        <select id="agent-to" class="object-select" data-child="4">
            <option value="">..</option>
            <?
            $dbUsers = CUser::GetList($sortBy = "LAST_NAME", $sortOrder = "ASC", array("GROUPS_ID" => "7"));
            while ($arUser = $dbUsers->Fetch()):?>
                <option value="<?= $arUser["ID"] ?>"><?= $arUser["LAST_NAME"] ?></option>
            <? endwhile; ?>
        </select>
        <input id="transfer" value="���������" type="button">
        <div class="response" style="display: inline; margin-left: 15px;"></div>
    </div>

    <script>
        $(document).ready(function() {
            $('#transfer').click(function() {
                var from = $('#agent-from').val(),
                    to = $('#agent-to').val(),
                    button = $(this);
                button.addClass('ajax_loader');

                $.post("/utils/transfer.php", {from: from, to: to}, function(response) {
                    $('.response').text(response);
                    button.removeClass('ajax_loader');
                });
            });
        })
    </script>
<? endif; ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>