<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "�������� ������");
$APPLICATION->SetTitle("�������� ������");

if ($USER->IsAuthorized()) {

    if ($USER->isAdmin() && CUser::GetID() == PROP_ADMIN_ID)
        $elementAssoc = "87";
    else
        $elementAssoc = "51";

    $APPLICATION->IncludeComponent(
        "gp:iblock.element.add.form",
        "gp_add_element_form",
        array(
            "IBLOCK_TYPE" => "gp_realestate_catalog",
            "IBLOCK_ID" => "5",
            "STATUS_NEW" => "N",
            "LIST_URL" => SITE_DIR . "personal/my-objects/",
            "USE_CAPTCHA" => "N",
            "USER_MESSAGE_EDIT" => "������ ������� ��������",
            "USER_MESSAGE_ADD" => "������ ������� ��������",
            "DEFAULT_INPUT_SIZE" => "30",
            "RESIZE_IMAGES" => "Y",
            "PROPERTY_CODES" => array(
                0 => "NAME",
                1 => "IBLOCK_SECTION",
                2 => "29",
                3 => "56",
                4 => "30",
                5 => "32",
                6 => "31",
                7 => "33",
                8 => "35",
                9 => "38",
                10 => "37",
                11 => "39",
                12 => "43",
                13 => "40",
                14 => "57",
                15 => "58",
                16 => "42",
                17 => "41",
                18 => "54",
                19 => "60",
                20 => "47",
                21 => "59",
                22 => "48",
                23 => "49",
                24 => "61",
                25 => "50",
                26 => "45",
                27 => "46",
                28 => "PREVIEW_TEXT",
                29 => "DETAIL_TEXT",
                30 => "DETAIL_PICTURE",
                31 => "52",
                //33 => "36",
                35 => "51",
                36 => "66",
                37 => "68",
                38 => "69",
                39 => "67",
                40 => "89",
                41 => "71",
                42 => "87",
                43 => "88",
                44 => "90",
                45 => "91",
                46 => "92",
                47 => "93",
                48 => "98",
                49 => "99",
                50 => "100"
            ),
            "PROPERTY_CODES_REQUIRED" => array(
                0 => "NAME",
                1 => "IBLOCK_SECTION",
                2 => "33",
                3 => "35",
                //7 => "40",
                //8 => "48",
                //10 => "DETAIL_TEXT",
                //11 => "DETAIL_PICTURE",
                //13 => "PREVIEW_TEXT",
                12 => "51",

            ),
            "GROUPS" => array(
                0 => "1",
                1 => "5",
                2 => "7",
            ),
            "STATUS" => "ANY",
            "ELEMENT_ASSOC" => "PROPERTY_ID",
            "MAX_USER_ENTRIES" => "0",
            "MAX_LEVELS" => "1",
            "LEVEL_LAST" => "Y",
            "MAX_FILE_SIZE" => "0",
            "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
            "DETAIL_TEXT_USE_HTML_EDITOR" => "Y",
            "SEF_MODE" => "N",
            "SEF_FOLDER" => SITE_DIR . "personal/add/",
            "CUSTOM_TITLE_NAME" => "",
            "CUSTOM_TITLE_TAGS" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
            "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
            "CUSTOM_TITLE_IBLOCK_SECTION" => "",
            "CUSTOM_TITLE_PREVIEW_TEXT" => "",
            "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
            "CUSTOM_TITLE_DETAIL_TEXT" => "",
            "CUSTOM_TITLE_DETAIL_PICTURE" => "",
            "ELEMENT_ASSOC_PROPERTY" => $elementAssoc
        ),
        false);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");?>