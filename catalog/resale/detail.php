<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"gp_element", 
	array(
		"IBLOCK_TYPE" => "gp_realestate_catalog",
		"IBLOCK_ID" => "5",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"PROPERTY_CODE" => array(
            0 => "rooms",
            1 => "rooms_of_transaction",
            2 => "rooms_in_flat",
            3 => "price",
            4 => "area",
            5 => "city",
            6 => "district",
            7 => "metro",
            8 => "address",
            9 => "number",
            10 => "housing",
            11 => "building",
            12 => "floor",
            13 => "floors",
            14 => "all_area",
            15 => "room_area",
            16 => "live_area",
            17 => "kitchen_area",
            18 => "ymap",
            19 => "lavatory",
            20 => "have",
            21 => "repair",
            22 => "sight",
            23 => "entrance",
            24 => "realtor",
            25 => "photo",
            26 => "schem",
            27 => "rooms_area"
		),
		"OFFERS_LIMIT" => "0",
		"SECTION_URL" => SITE_DIR."catalog/#CODE#/",
		"DETAIL_URL" => SITE_DIR."catalog/#SECTION_CODE#/#SECTION_ID#/#ELEMENT_ID#/",
		"BASKET_URL" => "",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"LINK_IBLOCK_TYPE" => "gp_realestate",
		"LINK_IBLOCK_ID" => "20",
		"LINK_PROPERTY_SID" => "district",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"HIDE_NOT_AVAILABLE" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"CONVERT_CURRENCY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
