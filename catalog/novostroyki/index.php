<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if ($_REQUEST["sort"]){
	$SORT_FIELD = trim(htmlspecialchars($_REQUEST["sort"]));
	$SORT_ORDER = trim(htmlspecialchars($_REQUEST["order"]));
    $SORT_FIELD_2 = "name";
    $SORT_ORDER_2 = "asc";
} else {
	$SORT_FIELD = "propertysort_rooms";
	$SORT_ORDER = "asc";
    $SORT_FIELD_2 = "property_price";
    $SORT_ORDER_2 = "asc";
}
if ($_REQUEST["agent"]){
	global $gp_filter;
	$gp_filter = Array("PROPERTY_realtor" => trim(htmlspecialchars($_REQUEST["agent"])));
}

$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"gp_section_list",
	array(
		"IBLOCK_TYPE" => "gp_realestate_catalog",
		"IBLOCK_ID" => "5",
		"SECTION_ID" => 9,
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => $SORT_FIELD,
		"ELEMENT_SORT_ORDER" => $SORT_ORDER,
		"ELEMENT_SORT_FIELD2" => $SORT_FIELD_2,
		"ELEMENT_SORT_ORDER2" => $SORT_ORDER_2,
		"FILTER_NAME" => "gp_filter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"PAGE_ELEMENT_COUNT" => "12",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "price",
			1 => "price_currency",
			2 => "district",
            3 => "address",
            4 => "number",
			5 => "metro",
			6 => "all_area",
            7 => "term",
		),
		"OFFERS_LIMIT" => "12",
		"SECTION_URL" => SITE_DIR."catalog/#SECTION_CODE#/",
		"DETAIL_URL" => SITE_DIR."catalog/#SECTION_CODE#/#SECTION_ID#/#ELEMENT_ID#/",
		"BASKET_URL" => "",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "Y",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"PAGER_TEMPLATE" => "orange",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "�������",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"HIDE_NOT_AVAILABLE" => "N",
		"CONVERT_CURRENCY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N"
	),
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>