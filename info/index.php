<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<h1>������� � �������� ����������</h1>

<div class="content-left">
    <div class="index-about">
        <ul class="grid effect-8" id="grid">
            <li>
                <div class="teaser">
                    <a href="/info/news/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>�������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/info/useful/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span style="font-size: 18px;">�������� ����������</span></div>
                    </a>
                </div>
            </li>
            <li>
                <div class="teaser">
                    <a href="/info/important/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                        <div class="fade"></div>
                        <div class="span-center"><span>������ �� �����</span></div>
                    </a>
                </div>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/masonry.pkgd.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/imagesloaded.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/classie.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/AnimOnScroll.js"></script>
    <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.5,
            maxDuration: 1,
            viewportFactor: 0.2
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>