<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/catalog/novostroyki/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/novostroyki/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/novostroyki/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/commercial/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/commercial/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/commercial/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/country/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/country/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/country/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/resale/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/resale/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/resale/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/abroad/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/abroad/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/abroad/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/elite/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/elite/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/elite/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/rent/([0-9]+)/([0-9]+)/\\?{0,10}(.*)\$#",
		"RULE" => "/catalog/rent/detail.php?SECTION_ID=\\1&ELEMENT_ID=\\2",
		"ID" => "",
		"PATH" => "/catalog/rent/detail.php",
	),
	array(
		"CONDITION" => "#^/catalog/novostroyki/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/novostroyki/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/commercial/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/commercial/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/country/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/country/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/resale/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/resale/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/abroad/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/abroad/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/elite/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/elite/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/catalog/rent/\\?{0,15}(.*)\$#",
		"RULE" => "/catalog/rent/index.php?SECTION_CODE=\\1&\\2",
		"ID" => "",
		"PATH" => "",
	),
	array(
		"CONDITION" => "#^/info/important/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/info/important/index.php",
	),
	array(
		"CONDITION" => "#^/about/reviews/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/reviews/index.php",
	),
	array(
		"CONDITION" => "#^/info/useful/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/info/useful/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/include/slider.php",
	),
	array(
		"CONDITION" => "#^/info/news/(.*)#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "bitrix:news",
		"PATH" => "/info/news/index.php",
	)
);
