<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    <div class="mobile-h3">������</div>

<?
$props = getMobileCategory($APPLICATION->GetCurPage(false));
$redirectFolder = array_pop($props);

$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter", 
	"gp_filter_mobile", 
	array(
        "IBLOCK_TYPE" => "gp_realestate_catalog",
        "IBLOCK_ID" => 5,
        "FILTER_NAME" => "gp_filter",
        "FIELD_CODE" => array(),
        "PROPERTY_CODE" => $props,
        "LIST_HEIGHT" => "5",
        "TEXT_WIDTH" => "220",
        "NUMBER_WIDTH" => "80",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "SAVE_IN_SESSION" => "Y",
        "PRICE_CODE" => array(),
        "REDIRECT_FOLDER" => SITE_DIR . $redirectFolder,
        "CHECK_ACTIVE_SECTIONS" => "Y",
        "SECTION_ID" => 9,
        "SECTION_CODE" => "novostroyki",
        "SELECT_WIDTH" => "330",
        "ELEMENT_IN_ROW" => "3",
        "NAME_WIDTH" => "85",
        "FILTER_TITLE" => "",
        "BUTTON_ALIGN" => "right",
        "SELECT_IN_CHECKBOX" => array(
            0 => "rooms",
            1 => "",
        ),
        "SELECT_IN_RADIO" => array(
            0 => "",
        ),
        "CHECKBOX_NEW_STRING" => "N",
        "REPLACE_ALL_LABEL" => "Y",
        "REMOVE_POINTS" => "Y",
        "INCLUDE_JQUERY" => "N",
        "INCLUDE_PLACEHOLDER" => "Y",
        "INCLUDE_CHOSEN_PLUGIN" => "Y",
        "INCLUDE_FORMSTYLER_PLUGIN" => "N",
        "INCLUDE_AUTOCOMPLETE_PLUGIN" => "N",
        "INCLUDE_JQUERY_UI" => "Y",
        "INCLUDE_JQUERY_UI_SLIDER" => "Y",
        "JQUERY_UI_SLIDER_BORDER_RADIUS" => "Y",
        "JQUERY_UI_THEME" => "gp-turquoise",
        "JQUERY_UI_FONT_SIZE" => "12px",
        "RADIO_NEW_STRING" => "N"
    ),
	false
);?>