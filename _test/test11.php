<?php

class Beer
{
    protected static $name = 'Beer!';

    public function getName()
    {
        return static::$name;
    }
}

class Ale extends Beer
{
    protected static $name = 'Ale!';
}

$beerDrink = new Beer;

$aleDrink = new Ale;

echo "Beer is: " . $beerDrink->getName() . "<br>";
echo "Ale is:  " . $aleDrink->getName() . "<br>";
