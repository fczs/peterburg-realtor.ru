<?php

class CDdEventHandler
{
    public static function OnAfterIBlockElementAddHandler($aFields)
    {
        //���� ������� �� �������� ��� ��� �� ������� ��������� "������������", �������
        if (!$aFields["ID"] || ($aFields["IBLOCK_ID"] != CATALOG_IBLOCK_ID)) {
            return;
        }

        $sAddress = "";

        $sCity = CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], array("sort" => "asc"), array("ID" => PROP_CITY_ID));
        if ($ob = $sCity->GetNext()) {
            $sAddress .= $ob["VALUE_ENUM"] . ", ";
        }

        $sDistrict = CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], array("sort" => "asc"), array("ID" => PROP_DISTRICT_ID));
        if ($ob = $sDistrict->GetNext()) {
            $sAddress .= $ob["VALUE_ENUM"] . ", ";
        }

        $sStreet = CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], array("sort" => "asc"), array("ID" => PROP_STREET_ID));
        if ($ob = $sStreet->GetNext()) {
            //�.�. �������� "�����" - ��� "�������� � ��������� � ���������������", �� ��� VALUE - ��� ��� ��������� ��,
            //������� �������� ����� �� �� ����.
            $streetElement = CIBlockElement::GetByID($ob["VALUE"]);
            if ($street = $streetElement->GetNext())
                $sAddress .= $street["NAME"] . ", ";
        }

        $sHouse = CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], array("sort" => "asc"), array("ID" => PROP_HOUSE_ID));
        if ($ob = $sHouse->GetNext()) {
            $sAddress .= $ob["VALUE"];
        }

        $sHousing = CIBlockElement::GetProperty($aFields["IBLOCK_ID"], $aFields["ID"], array("sort" => "asc"), array("ID" => PROP_HOUSING_ID));
        if ($ob = $sHousing->GetNext()) {
            if (!empty($ob["VALUE"])) $sAddress .= ", " . $ob["VALUE"];
        }

        if ($sAddress) {
            $aParams = array(
                'geocode' => $sAddress, //����� �������
                'format' => 'json'
            );

            $oResponse = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($aParams, '', '&')));

            if ($oResponse->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
                $aCoords = explode(' ', $oResponse->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
            }
        }

        //���� ���������� ��������, ��������� �������� ��������
        if ($aCoords) {
            CIBlockElement::SetPropertyValues($aFields["ID"], $aFields["IBLOCK_ID"], $aCoords[1] . "," . $aCoords[0], PROP_YMAP_POSITION_ID);
        }
    }
}

