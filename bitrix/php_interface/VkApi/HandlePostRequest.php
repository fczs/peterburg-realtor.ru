<?php

namespace VkApi;

use VkApi\Contracts\VkApi;
use VkApi\Utils\Response;

/**
 * Forms POST request and returns response result
 */
class HandlePostRequest extends VkApi
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->requestMethod = new RequestMethod\Post();
    }

    /**
     * Adds params to POST request and submits it, returns response result
     *
     * @param string $url
     * @param array $content
     * @param string $contentType
     * @return string
     */
    public function handleRequest($url, $content, $contentType)
    {
        $params = new RequestParams($url, $content, $contentType);
        $rawResult = $this->requestMethod->submit($params);

        return Response::fromJson($rawResult);
    }
}