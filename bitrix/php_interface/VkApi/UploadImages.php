<?php

namespace VkApi;

use VkApi\Contracts\VkApi;
use VkApi\Contracts\Config;
use VkApi\Utils\ManageImages;

/**
 * Uploads images
 */
class UploadImages extends VkApi
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->request = new HandlePostRequest();
        $this->contentType = Config::$CONTENT_MP;
    }
    
    /**
     * Uploads photo to a server
     *
     * @param string $image
     * @param string $isMain
     * @return array
     */
    public function uploadPhoto($image, $isMain = "1")
    {
        $manageImages = new ManageImages();
        $url = $manageImages->getMarketUploadServer($isMain);

        $imageParams = $this->request->handleRequest($url, $image, $this->contentType);

        return $manageImages->saveMarketPhoto($imageParams);
    }
}