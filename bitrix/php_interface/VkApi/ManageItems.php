<?php

namespace VkApi;

use VkApi\Contracts\VkApi;
use VkApi\Contracts\Config;

/**
 * Represents methods for items management
 */
class ManageItems extends VkApi
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->url = Config::$API_URL;
        $this->content = array(
            "owner_id" => "-" . Config::$GROUP_ID,
            "access_token" => Config::$ACCESS_TOKEN
        );
        $this->request = new HandleGetRequest();
    }

    /**
     * Adds item to a group
     *
     * @param $item array with item params contains "name", "description", "category_id",
     * "price", "deleted" (0 by default), "main_photo_id" as required fields,
     * and "photo_ids" as optional field with enumeration of additional images
     * 
     * @return array
     */
    public function add($item)
    {
        $url = $this->url . Config::$ADD_ITEM . "?";
        $content = array_merge($this->content, $item);

        return $this->request->handleRequest($url, $content);
    }
}