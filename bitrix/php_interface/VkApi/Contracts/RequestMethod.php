<?php

namespace VkApi\Contracts;

use \VkApi\RequestParams;

/**
 * Method used to send the request to the API
 */
interface RequestMethod
{
    /**
     * Submit the request with the specified parameters
     *
     * @param RequestParams $params Request parameters
     * @return string Body of the VK API response
     */
    public function submit(RequestParams $params);
}
