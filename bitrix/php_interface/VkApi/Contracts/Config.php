<?php

namespace VkApi\Contracts;

/**
 * VK API connection parameters
 */
abstract class Config
{
    /**
     * API methods URL
     * @var string
     */
    static $API_URL = "https://api.vk.com/method/";

    /**
     * Content type multipart/form-data
     * @var string
     */
    static $CONTENT_MP = "Content-Type: multipart/form-data;";

    /**
     * Authorized VK user ID
     * @var string
     */
    static $USER_ID = "189968672";

    /**
     * VK application ID
     * @var string
     */
    static $APP_ID = "6030763";

    /**
     * VK group ID
     * @var string
     */
    static $GROUP_ID = "20248509";

    /**
     * Offline access token, got with OAuth method
     * @var string
     */
    static $ACCESS_TOKEN = "6b41302d46f1cebc17ffb59bb62a6985022e5d29bd06be9da3a373c22ffc4d40b51b69cd3e7a3eafcda3a";

    /**
     * VK API current version
     * @var string
     */
    static $API_VERSION = "5.64";

    /**
     * Market Add method
     * @var string
     */
    static $ADD_ITEM = "market.add";

    /**
     * Photos Get URL method
     * @var string
     */
    static $IMG_GET_URL = "photos.getMarketUploadServer";

    /**
     * Photos Save file method
     * @var string
     */
    static $IMG_SAVE = "photos.saveMarketPhoto";
}