<?php

namespace VkApi\Contracts;

/**
 * Parameters used to communicate with VK API
 */
abstract class VkApi
{
    /**
     * Request URL
     * @var string
     */
    protected $url;

    /**
     * Request content
     * @var mixed
     */
    protected $content;

    /**
     * Content type
     * @var string
     */
    protected $contentType;

    /**
     * Method used to communicate with VK API
     * @var \VkApi\Contracts\RequestMethod
     */
    protected $requestMethod;

    /**
     * Instance of the class used to handle a particular type of request
     * @var object
     */
    protected $request;
}
