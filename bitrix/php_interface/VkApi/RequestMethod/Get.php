<?php

namespace VkApi\RequestMethod;

use VkApi\Contracts\RequestMethod;
use VkApi\RequestParams;

/**
 * Gets contents at specified URI
 */
class Get implements RequestMethod
{
    public function submit(RequestParams $params)
    {
        return file_get_contents($params->getParam("url") . $params->getQueryParams());
    }
}