<?php

namespace VkApi\RequestMethod;

use VkApi\Contracts\RequestMethod;
use VkApi\Utils\MultipartQuery;
use VkApi\RequestParams;

/**
 * Sends POST requests to VK API
 */
class Post implements RequestMethod
{
    public function submit(RequestParams $params)
    {
        $options = array(
            "http" => array(
                "method" => "POST",
                "header" => MultipartQuery::buildHeader(),
                "content" => MultipartQuery::buildContent($params->getParam("content"), $params->getParam("contentType"))
            ),
        );
        $context = stream_context_create($options);
        return file_get_contents($params->getParam("url"), false, $context);
    }
}
