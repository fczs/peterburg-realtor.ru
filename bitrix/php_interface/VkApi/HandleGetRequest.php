<?php

namespace VkApi;

use VkApi\Contracts\VkApi;
use VkApi\Utils\Response;

/**
 * Forms GetContents request and returns response result
 */
class HandleGetRequest extends VkApi
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->requestMethod = new RequestMethod\Get();
    }

    /**
     * Adds params to GetContents request and submits it, returns response result
     *
     * @param string $methodUri
     * @param array $arParams
     * @return mixed
     */
    public function handleRequest($methodUri, $arParams)
    {
        $params = new RequestParams($methodUri, $arParams);
        $rawResult = $this->requestMethod->submit($params);

        return Response::fromJson($rawResult);
    }
}