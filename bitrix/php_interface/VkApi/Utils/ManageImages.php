<?php

namespace VkApi\Utils;

use VkApi\Contracts\VkApi;
use VkApi\Contracts\Config;
use VkApi\HandleGetRequest;

/**
 * Represents methods for images management
 */
class ManageImages extends VkApi
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->url = Config::$API_URL;
        $this->content = array(
            "group_id" => Config::$GROUP_ID,
            "access_token" => Config::$ACCESS_TOKEN
        );
        $this->request = new HandleGetRequest();
    }

    /**
     * Gets photo upload URL
     *
     * @param string $isMain
     * @return string
     */
    public function getMarketUploadServer($isMain)
    {
        $url = $this->url . Config::$IMG_GET_URL . "?";
        $content = $this->content;
        $content["main_photo"] = $isMain;

        return $this->request->handleRequest($url, $content)["response"]["upload_url"];
    }

    /**
     * Saves uploaded photo on a server
     *
     * @param $imageParams array with image params contains "photo", "server", "hash" as required fields
     * and "crop_data", "crop_hash" as optional fields for the main photo only
     *
     * @return array
     */
    public function saveMarketPhoto($imageParams)
    {
        $url = $this->url . Config::$IMG_SAVE . "?";
        $content = array_merge($this->content, $imageParams);

        return $this->request->handleRequest($url, $content)["response"]["0"];
    }
}