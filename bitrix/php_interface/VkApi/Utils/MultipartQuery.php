<?php

namespace VkApi\Utils;

/**
 * Methods for building a multipart POST query with boundary
 */
class MultipartQuery
{
    /**
     * Boundary
     * @var string
     */
    private static $MULTIPART_BOUNDARY;

    /**
     * File field
     * @var string
     */
    private static $FORM_FIELD = "file";

    /**
     * Constructor
     */
    private function __construct()
    {
        self::$MULTIPART_BOUNDARY = '--------------------------' . microtime(true);
    }

    /**
     * Builds a query header
     *
     * @return string
     */
    public static function buildHeader()
    {
        return "Content-Type: multipart/form-data; boundary=" . self::$MULTIPART_BOUNDARY;
    }

    /**
     * Builds a query content
     *
     * @param string $filename
     * @param string $contentType
     * @return string
     */
    public static function buildContent($filename, $contentType)
    {
        $fileContents = file_get_contents($filename);

        return "--" . self::$MULTIPART_BOUNDARY . "\r\n" .
            "Content-Disposition: form-data; name=\"" . self::$FORM_FIELD . "\"; filename=\"" . basename($filename) . "\"\r\n" .
            $contentType . "\r\n\r\n" .
            $fileContents . "\r\n" .
            "--" . self::$MULTIPART_BOUNDARY . "--\r\n";
    }
}