<?php

namespace VkApi\Utils;

/**
 * Handles the response returned from VK
 */
class Response
{
    /**
     * Builds the response from JSON returned by the VK API
     *
     * @param string $json
     * @return mixed
     */
    public static function fromJson($json)
    {
        $responseData = json_decode($json, true);

        if (!$responseData) {
            return false;
        } else {
            return (array)$responseData;
        }
    }
}
