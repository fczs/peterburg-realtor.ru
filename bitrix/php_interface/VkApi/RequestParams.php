<?php

namespace VkApi;

/**
 * Stores and formats the parameters for the request to VK API
 */
class RequestParams
{
    /**
     * Request URL
     * @var string
     */
    private $url;

    /**
     * Request content
     * @var mixed
     */
    private $content;

    /**
     * Content type
     * @var string
     */
    private $contentType;

    /**
     * Initialise parameters
     *
     * @param string $url
     * @param mixed $content
     * @param string $contentType
     */
    public function __construct($url, $content, $contentType = "")
    {
        $this->url = $url;
        $this->content = $content;
        $this->contentType = $contentType;
    }

    /**
     * Returns request parameter by its name
     *
     * @param string $paramName
     * @return string
     */
    public function getParam($paramName)
    {
        return $this->$paramName;
    }

    /**
     * Returns the JSON string of request parameter by its name
     *
     * @param string $paramName
     * @return string
     */
    public function getParamJson($paramName)
    {
        return json_encode($this->$paramName);
    }

    /**
     * Returns URL query params
     *
     * @return string
     */
    public function getQueryParams()
    {
        return http_build_query($this->content);
    }
}
