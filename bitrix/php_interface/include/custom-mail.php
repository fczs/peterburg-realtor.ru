<?php
/**
 * ��������� ����� ����� SMTP-������ sweb.
 *
 * @see CEvent::HandleEvent()
 * @see bxmail()
 *
 * @param string $to ����� ����������.
 * @param string $subject ����.
 * @param string $message ����� ���������.
 * @param string $additionalHeaders �������������� ��������� ���������� ��������� ����� ������ ("FROM" ��������� �����).
 *
 * @return bool
 */
require_once 'Net/SMTP.php';

function custom_mail($to, $subject, $message, $additionalHeaders = '')
{
    $smtpServerHost         = 'smtp.spaceweb.ru';
    $smtpServerHostPort     = 25;
    $smtpServerUser         = 'info@peterburg-realtor.ru';
    $smtpServerUserPassword = 'oEzF8JIGp3';

    if (!($smtp = new Net_SMTP($smtpServerHost, $smtpServerHostPort))) {
        return false;
    }
    if (PEAR::isError($e = $smtp->connect())) {
        return false;
    }
    if (PEAR::isError($e = $smtp->auth($smtpServerUser, $smtpServerUserPassword))) {
        return false;
    }

    preg_match('/From: (.+)\n/i', $additionalHeaders, $matches);
    list(, $from) = $matches;

    preg_match('/Cc: (.+)\n/i', $additionalHeaders, $matches);
    list(, $cc) = $matches;

    $smtp->mailFrom($from);
    $smtp->rcptTo($to);
    $smtp->rcptTo($cc);

    $eol = CAllEvent::GetMailEOL();

    $additionalHeaders .= $eol . 'Subject: ' . $subject;

    if (PEAR::isError($e = $smtp->data($additionalHeaders . "\r\n\r\n" . $message))) {
        return false;
    }

    $smtp->disconnect();

    return true;
}