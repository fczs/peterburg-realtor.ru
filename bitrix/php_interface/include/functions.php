<?php

function _r($data)
{
    global $USER;
    if ($USER->isAdmin()) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

function getCatPage($page)
{
    switch ($page) :
        case  '/catalog/novostroyki/':
        case  '/catalog/':
        case  '/': return 'new';
        case  '/catalog/resale/': return 'resale';
        case  '/catalog/abroad/': return 'abroad';
        case  '/catalog/commercial/': return 'commercial';
        case  '/catalog/country/': return 'country';
        case  '/catalog/elite/': return 'elite';
        case  '/catalog/rent/': return 'rent';
        default : return 'none';
    endswitch;
}

function getMobileCategory($cat)
{
    if (strpos($cat, "catalog/resale/")) {
        $props = array(0 => "type", 1 => "rooms", 2 => "metro", 3 => "price", 4 => "all_area", "redirect" => "catalog/resale/");
    } elseif (strpos($cat, "catalog/abroad/")) {
        $props = array(0 => "rooms", 1 => "metro", 2 => "price", 3 => "all_area", "redirect" => "catalog/abroad/");
    } elseif (strpos($cat, "catalog/commercial/")) {
        $props = array(0 => "commercial", 2 => "price", 3 => "all_area", 4 => "type_of_deal", "redirect" => "catalog/commercial/");
    } elseif (strpos($cat, "catalog/country/")) {
        $props = array(0 => "price", 1 => "all_area", 2 => "site_area", "redirect" => "catalog/country/");
    } elseif (strpos($cat, "catalog/elite/")) {
        $props = array(0 => "rooms", 1 => "metro", 2 => "price", 3 => "all_area", "redirect" => "catalog/elite/");
    } elseif (strpos($cat, "catalog/rent/")) {
        $props = array(0 => "rooms", 1 => "metro", 2 => "price", 3 => "all_area", "redirect" => "catalog/rent/");
    } else {
        $props = array(0 => "rooms", 1 => "metro", 2 => "price", 3 => "all_area", "redirect" => "catalog/novostroyki/");
    }

    return $props;
}

function w2u($val)
{
    return iconv('windows-1251', 'UTF-8', $val);
}

function u2w($val)
{
    return iconv('UTF-8', 'windows-1251', $val);
}


function my_mb_ucfirst($str)
{
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}