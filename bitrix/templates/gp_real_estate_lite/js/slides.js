$(function () {
    $('#slides').slides({
        preload: true,
        preloadImage: 'images/slider/loading.gif',
        generatePagination: false,
        generateNextPrev: false,
        effect: 'fade',
        play: 5000,
        pause: 3000,
        hoverPause: true,
        fadeSpeed: 300
    });
});
