var newSection = [29, 56, 66, 32, 31, 54, 61, 67, 68, 71, 92];
var resaleSection = [56, 66, 60, 67, 68, 70, 89, 90, 91, 92];
var countrySection = [29, 56, 66, 32, 31, 60, 43, 61, 70, 75, 76, 77, 46, 89, 90, 91, 93];
var commerceSection = [29, 30, 31, 32, 60, 54, 61, 49, 50, 45, 46, 68, 70, 76, 77, 89, 90, 91, 92, 93];
var rentSection = [56, 66, 60, 67, 68, 70, 89, 90, 91, 92];
var eliteSection = [29, 56, 66, 32, 31, 61, 67, 68, 89, 90, 91, 92];
var abroadSection = [29, 56, 66, 32, 31, 61, 67, 68, 60, 70, 89, 90, 91, 92];
var objFlat = [31, 32, 61];
var objRoom = [30, 49];
var dataNames;

function displayTR(dataNames) {
    $('tr').each(function () {
        $('tr').css('display', '');
    });
    $(dataNames).each(function () {
        $('[data-name =' + this + ']').css('display', 'none');
    });
    $('[data-name = "69"]').css('display', 'none');
    $('[data-name = "87"]').css('display', 'none');
    $('[data-name = "88"]').css('display', 'none');
    $('[data-name = "99"]').css('display', 'none');
}

function chooseFilter(filter) {
    switch ($(filter).val()) {
        case '9' :
            dataNames = newSection;
            break;
        case '6' :
            dataNames = $.merge([], resaleSection);
            switch ($('#obj-type').val()) {
                case '172' :
                    $.merge(dataNames, objFlat);
                    break;
                case '173' :
                    $.merge(dataNames, objRoom);
            }
            break;
        case '8' :
            dataNames = countrySection;
            break;
        case '5' :
            dataNames = commerceSection;
            break;
        case '7' :
            dataNames = $.merge([], rentSection);
            switch ($('#obj-type').val()) {
                case '172' :
                    $.merge(dataNames, objFlat);
                    break;
                case '173' :
                    $.merge(dataNames, objRoom);
            }
            break;
        case '10' :
            dataNames = eliteSection;
            break;
        case '11' :
            dataNames = abroadSection;
    }

    displayTR(dataNames);
}

$(document).ready(function () {
    $('font.notetext').delay(3000).fadeOut();
    $('#bx_address_search_control_map_yandex_ymap_42').empty();

    $('#names-list').change(function () {
        var nameListVal = $(this).val();
        $('#obj-name').val(nameListVal);
        $("#names-list-prop").find("option:contains('" + nameListVal + "')").attr("selected", "selected");
    });

    $('#term').change(function () {
        var termVal = $('#term  option:selected').data('term');
        $('#date-term').val(termVal);
    });

    $('#section-name').change(function () {
        chooseFilter('#section-name');
    });

    $('#obj-type').change(function () {
        chooseFilter('#section-name');
    });

    chooseFilter('#section-name');

    $('.nums-only').keypress(function (e) {
        if (!(e.which == 8 || e.which == 46 || (e.which > 47 && e.which < 58))) return false;
    });
});