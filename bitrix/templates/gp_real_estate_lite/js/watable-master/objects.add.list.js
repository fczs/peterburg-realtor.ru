$(document).ready(function () {
    $('.btn.btn-phone.small.callme').css('display', 'none');

    var waTable = $('#object-table').WATable({
        //data: generateSampleData(100), //Initiate with data if you already have it
        debug: false,                //Prints some debug info to console
        dataBind: true,             //Auto-updates table when changing data row values. See example below. (Note. You need a column with the 'unique' property)
        pageSize: 20,                //Initial pagesize
        pageSizePadding: true,      //Pads with empty rows when pagesize is not met
        filter: true,               //Show filter fields
        sorting: true,              //Enable sorting
        sortEmptyLast: true,         //Empty values will be shown last
        columnPicker: true,         //Show the columnPicker button
        pageSizes: [5, 10, 15, 20, 30, 50, 100, 200],  //Set custom pageSizes. Leave empty array to hide button.
        hidePagerOnEmpty: true,     //Removes the pager if data is empty.
        checkboxes: false,           //Make rows checkable. (Note. You need a column with the 'unique' property)
        checkAllToggle: true,        //Show the check-all toggle
        preFill: true,              //Initially fills the table with empty rows (as many as the pagesize).
        types: {                    //If you want, you can supply some properties that will be applied for specific data types.
            string: {
                filterTooltip: "������� ������",    //What to say in tooltip when hoovering filter fields. Set false to remove.
                placeHolder: "������..."    //What to say in placeholder filter fields. Set false for empty.
            },
            number: {
                filterTooltip: '�������� "10..20"<br>��� ��������� "!10..20"<br>������ �������� "=50"',
                placeHolder: "��..��",
                decimals: 2   //Sets decimal precision for float types
            },
            bool: {
                //filterTooltip: false
            },
            date: {
                filterTooltip: '',
                utc: true,            //Show time as universal time, ie without timezones.
                //format: 'yy/dd/MM',   //The format. See all possible formats here http://arshaw.com/xdate/#Formatting.
                datePicker: true      //Requires "Datepicker for Bootstrap" plugin (http://www.eyecon.ro/bootstrap-datepicker).
            }
        },
        tableCreated: function (data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.
            console.log('table created'); //data.table holds the html table element.
            console.log(data);            //'this' keyword also holds the html table element.
        },
        rowClicked: function (data) {      //Fires when a row or anything within is clicked (Note. You need a column with the 'unique' property).
            console.log('row clicked');   //data.event holds the original jQuery event.
            //data.row holds the underlying row you supplied.
            //data.index holds the index of row in rows array (Useful when you want to remove the row)
            //data.column holds the underlying column you supplied.
            //data.checked is true if row is checked. (Set to false/true to have it unchecked/checked)
            //'this' keyword holds the clicked element.

            //Removes the row if user clicked on the column called 'remove'.
            if (data.column.column == "remove") {
                data.event.preventDefault();
                //Remove fast with dataBind option
                waTable.getData().rows.splice(data.index, 1);
                Platform.performMicrotaskCheckpoint();

                //This would have worked fine as well, but is slower
                //var d = waTable.getData();
                //d.rows.splice(data.index, 1);
                //waTable.setData(d);
            }
//We can look at classes on the clicked element as well
            if (this.classList.contains('someClass')) {
                //Do something...
            }
//Example toggle checked state
            if (data.column.column == "check") {
                data.checked = !data.checked;
            }
            console.log(data);
        },
        columnClicked: function (data) {    //Fires when a column is clicked
            console.log('column clicked');  //data.event holds the original jQuery event
            console.log(data);              //data.column holds the underlying column you supplied
            //data.descending is true when sorted descending (duh)
        },
        pageChanged: function (data) {      //Fires when manually changing page
            console.log('page changed');    //data.event holds the original jQuery event
            console.log(data);              //data.page holds the new page index
        },
        pageSizeChanged: function (data) {  //Fires when manually changing pagesize
            console.log('pagesize changed');//data.event holds teh original event
            console.log(data);              //data.pageSize holds the new pagesize
        }
    }).data('WATable');  //This step reaches into the html data property to get the actual WATable object. Important if you want a reference to it as we want here.

//Generate some data
    var data = generateData();
    waTable.setData(data);  //Sets the data.
//waTable.setData(data, true); //Sets the data but prevents any previously set columns from being overwritten
//waTable.setData(data, false, false); //Sets the data and prevents any previously checked rows from being reset

//Set options on the fly
    var pageSize = waTable.option("pageSize"); //Get option
//waTable.option("pageSize", pageSize); //Set option

//Example event handler triggered by the custom export links above.
    $('#object-table').on('click', '.export', function (e) {
        e.preventDefault();
        var elem = $(e.target);
        var data;
        if (elem.hasClass('all')) data = waTable.getData(false);
        else if (elem.hasClass('checked')) data = waTable.getData(true);
        else if (elem.hasClass('filtered')) data = waTable.getData(false, true);
        else if (elem.hasClass('rendered')) data = waTable.getData(false, false, true);
        console.log(data.rows.length + ' rows returned', data);
        alert(data.rows.length + ' rows returned.\nSee data in console.');
    });

    $(".object-select").change(function () {
        var e = jQuery.Event("keyup", { keyCode: 13 });
        var option = $(this).find('option:selected').val();
        var child = $(this).data("child");
        $("table.watable tr.filter th:nth-child(" + child + ") input").val(option).focus().trigger(e);
    });

});



