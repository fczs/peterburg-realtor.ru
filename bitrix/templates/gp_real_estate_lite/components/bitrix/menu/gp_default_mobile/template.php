<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="mobile-nav">
	<span class="mobile-menu">
		<a id="mobile-menu" href="#sidr-left">
			<i class="mobile-menu-img">&nbsp;</i>
		</a>
	</span>
	
	<div class="mobile-title">
        <a href="<?=SITE_DIR?>">
			<div class="gp_mhome"></div></a>
	</div>

	<span class="mobile-search">
		<a id="search-menu" href="#sidr-right">
			<i class="search-menu-img">&nbsp;</i>
		</a>
	</span>
</div>
<script>
	$(document).ready(function () {
		$('.search-menu-img').click(function () {
			$('.sidr-class-type_4').prop('disabled', false);
			$('#sidr-class-type_4').removeClass('disable');

		});
		$('#mobile-menu').sidr({
            onOpen: function () {
                $('.mobile-menu-img').addClass('opened');
            },
            onClose: function () {
                $('.mobile-menu-img').removeClass('opened');
            }
        });
		$('#search-menu').sidr({
			name: 'sidr-right',
			side: 'right',
			source: '#sidr-right',
            renaming: false,
            onOpen: function () {
                $('.search-menu-img').addClass('opened');
            },
            onClose: function () {
                $('.search-menu-img').removeClass('opened');
            }
		});
	});
</script> 

<?if (!empty($arResult)):?>
<div id="sidr">
<ul id="gp-default-menu-mobile">
<?
$previousLevel = 0;
$firstRoot = false;
$cn=0;
foreach($arResult as $arItem):
$cn++;
?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
		<script>
		$(document).ready(function() {
			$('#gp-menu_<?=$cn?>').click(function(){
				$('#gp-sub_menu_<?=$cn?>').toggle();
			});
		});
		</script>		
			<li id="gp-menu_<?=$cn?>">
				<a href="#" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?><div class="gp_drop"></div></a>
						<ul id="gp-sub_menu_<?=$cn?>" class="gp-sub">
		<?else:?>
			<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li>
					<a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a> 
				</li>
			<?else:?>
				<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="menusub"><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?
		$previousLevel = $arItem["DEPTH_LEVEL"];
		if ($arItem["DEPTH_LEVEL"] == 1)
			$firstRoot = true;
	?>
	
<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
	</ul>
</div>
<?endif?>

<div id="sidr-right">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "filter_mobile",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => ""
	)
);?>
</div>