<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $USER;

if (!empty($arResult)): ?>
<ul id="gp-default-menu">
    <?
    $previousLevel = 0;
    $firstRoot = false;
    foreach ($arResult as $arItem):?>
    <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
        <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
    <? endif ?>

    <? if ($arItem["IS_PARENT"]): ?>

    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
    <li class="menusub"><? if ($firstRoot): ?>
            <div class="root-separator"></div><? endif ?><a href="<?= $arItem["LINK"] ?>"
                                                            class="<? if ($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<?endif ?>"><?= $arItem["TEXT"] ?></a>

        <div class="gp_sub-menu">
            <div class="gp_sub_width">
                <ul class="gp_sub-menu-element">
                    <? else: ?>
                    <li<? if ($arItem["SELECTED"]): ?> class="item-selected"<? endif ?>><a href="<?= $arItem["LINK"] ?>"
                                                                                           class="parent"><?= $arItem["TEXT"] ?></a>
                        <ul>
                            <? endif ?>

                            <? else: ?>

                                <? if ($arItem["PERMISSION"] > "D"): ?>

                                    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                        <li><? if ($firstRoot): ?>
                                                <div class="root-separator"></div><? endif ?><a
                                                href="<?= $arItem["LINK"] ?>"
                                                class="<? if ($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<?endif ?>"><?= $arItem["TEXT"] ?></a>
                                        </li>
                                    <? else: ?>
                                        <li<? if ($arItem["SELECTED"]): ?> class="item-selected"<? endif ?>><a
                                                href="<?= $arItem["LINK"] ?>" <?if(preg_match("/catalog/", $arItem["LINK"])):?>class="cat-change"<?endif?>><?= $arItem["TEXT"] ?></a></li>
                                    <?endif ?>

                                <? else: ?>

                                    <? if ($arItem["DEPTH_LEVEL"] == 1): ?>
                                        <li class="menusub"><? if ($firstRoot): ?>
                                                <div class="root-separator"></div><? endif ?><a href=""
                                                                                                class="<? if ($arItem["SELECTED"]): ?>root-item-selected<? else: ?>root-item<?endif ?>"
                                                                                                title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                        </li>
                                    <? else: ?>
                                        <li><a href="" class="denied"
                                               title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                        </li>
                                    <?endif ?>

                                <?endif ?>

                            <?
                            endif ?>

                            <?
                            $previousLevel = $arItem["DEPTH_LEVEL"];
                            if ($arItem["DEPTH_LEVEL"] == 1)
                                $firstRoot = true;
                            ?>

                            <? endforeach ?>

                            <? if ($previousLevel > 1): //close last item tags?>
                                <?= str_repeat("</ul></div></div></li>", ($previousLevel - 1)); ?>
                            <? endif ?>
                        </ul>
                        <? endif ?>

                        <div class="top-fixed-btn">
                            <a class="btn btn-phone <?if($USER->IsAuthorized()):?>small<?endif;?> callme"
                               href="#"><?if(!$USER->IsAuthorized()):?>�������� ������<?endif;?></a>
                        </div>


