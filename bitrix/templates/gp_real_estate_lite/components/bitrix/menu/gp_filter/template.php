<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div id="gp_filter-menu">
        <ul>
            <? foreach ($arResult as $arItem): ?>
                <li<? if ($arItem["SELECTED"] || ($APPLICATION->GetCurPage(false) == "/" && $arItem["LINK"] == "/catalog/novostroyki/")): ?> class="root-item-selected"<? endif ?>>
                    <a href="<?= $arItem["LINK"] ?>"
                       class="parent"><?= $arItem["TEXT"] ?></a>
                </li>
            <? endforeach ?>
        </ul>
    </div>
<? endif ?>