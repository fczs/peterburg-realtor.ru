$(function () {
	$("#gp_mount_slider").slider({ 
		orientation: "horizontal",
		range: "min",
		min: 100000,
		max: 20000000,
		value: 100000,
		step: 100000,
		slide: function (event, ui) {
			$("#gp_mount").text(accounting.formatNumber(ui.value, 0, " "));
			calculatePayment();
		}
	});
	$("#gp_mount").text(accounting.formatNumber($("#gp_mount_slider").slider("value"), 0, " ")); 
	$("#gp_interest_slider").slider({
		orientation: "horizontal",
		range: "min",
		min: 1,
		max: 50,
		value: 12,
		step: 0.25,
		slide: function (event, ui) {
			$("#gp_interest").text(ui.value);
			calculatePayment();
		}
	});
	$("#gp_interest").text($("#gp_interest_slider").slider("value"));
	$("#gp_time_slider").slider({
		orientation: "horizontal",
		range: "min",
		min: 1,
		max: 30,
		value: 10,
		slide: function (event, ui) {
			$("#gp_time").text(ui.value);
			calculatePayment();
		}
	});
	$("#gp_time").text($("#gp_time_slider").slider("value"));

	function calculatePayment() {
		var gp_mount = $("#gp_mount_slider").slider("value");
		var gp_interest = $("#gp_interest_slider").slider("value") / 1200;
		var gp_time = $("#gp_time_slider").slider("value") * 12;
		var rate = gp_mount * (gp_interest * Math.pow(1 + gp_interest, gp_time)) / (Math.pow(1 + gp_interest, gp_time) - 1);
		$("#result").text(accounting.formatNumber(rate.toFixed(2), 0, " "));
		
	}
	calculatePayment();
});