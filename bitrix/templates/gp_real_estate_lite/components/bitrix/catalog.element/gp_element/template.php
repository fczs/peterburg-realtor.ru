<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$street = strip_tags($arResult["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]);
if(strlen($street) > 0)
    $street = ", " . $street;
$house = $arResult["DISPLAY_PROPERTIES"]["number"]["VALUE"];
if(strlen($house) > 0)
    $house = ", " . $house;
?>
<!--<div class="back-link gp_show"><a href="">�����</a></div>-->
<h1 class="catalog-element-title"><?= $arResult["NAME"] ?><span class="printable"><?= $street ?><?= $house ?></span></h1><a class="print-button gp_hide" href="#print" title="������ ��� ������" target="_blank" rel="nofollow"></a>
<div class="catalog-element">
    <div class="catalog-element-left">
        <div class="left-right">
            <div class="catalog-element-left-left">
                <div class="gp_element_price">
                    <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                        <? if ($arProperty['CODE'] == "price"): ?>
                            <span class="printable">����: </span>
                            <span class="gp_price"><?= number_format($arProperty["DISPLAY_VALUE"], 0, '.', ' ') ?></span>
                            <span class="rub<? if ($arResult['PROPERTIES']['price_currency']['VALUE'] == "���."): ?> rubble"><? else: ?>"><?= $arResult['PROPERTIES']['price_currency']['VALUE']; ?><? endif; ?></span>
                        <? endif; ?>
                    <? endforeach ?>
                </div>
                <div class="gp_object_items">
                    <div class="gp_params">
                        <? foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                            <? if (!$arProperty['FILE_VALUE'] && $arProperty['CODE'] !== "realtor" && $arProperty['CODE'] !== "ymap"): ?>
                                <? if ($arProperty['CODE'] !== "price" && $arProperty['CODE'] !== "infrastructure"): ?>
                                    <div class="gp_params_line">
                                    <div class="gp_params_line_inf"><?= $arProperty["NAME"] ?>:</div>
                                    <div class="gp_params_line_v">
                                <? endif; ?>
                                <?
                                if (is_array($arProperty["DISPLAY_VALUE"])):
                                    $DISPLAY_VALUE = implode(", ", $arProperty["DISPLAY_VALUE"]);
                                    $nHave = $arProperty["NAME"]; elseif ($arProperty['CODE'] == "price"):?>
                                <? else: echo strip_tags($arProperty["DISPLAY_VALUE"]); ?>
                                <?endif; ?>
                                <? if ($arProperty['CODE'] !== "price" && $arProperty['CODE'] !== "infrastructure"): ?>
                                    </div>
                                    </div>
                                    <br>
                                <? endif; ?>
                            <? endif; ?>
                        <? endforeach ?>
                        <? if ($DISPLAY_VALUE): ?>
                            <div>
                                <? echo $nHave . ": " . $DISPLAY_VALUE; ?>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="catalog-element-left-right">
                <? if (is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"])): ?>
                    <!--// ���� //-->
                    <?
                    $arFilters = Array();
                    $arSize = array(
                        "WIDTH" => "70",
                        "HEIGHT" => "50",
                        );
                    $arSizeFont = array(
                        "WIDTH" => "233",
                        "HEIGHT" => "180",
                        );
                    $arSizeAgentht = array(
                        "WIDTH" => "89",
                        "HEIGHT" => "89",
                        );
                    $arImgFont = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], $arSizeFont, BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                    $arImgFontBI = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array("width" => 233, "height" => 180), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                    ?>
                    <? if ($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]): ?>
                        <div class="connected-carousels">
                            <!--/ �������� ���������� /-->
                            <div class="stage">
                                <div class="carousel carousel-stage-pl">
                                    <ul>
                                        <?if (!$arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["SRC"]):
                                            for ($i = 0; $i < sizeof($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]); $i++) {
                                                $arImgSchem = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["ID"], $arSizeFont, BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                                $arImgSchemFront = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["ID"], array("width" => 233, "height" => 180), BX_RESIZE_IMAGE_EXACT, true, $arFilters); ?>
                                                <li>
                                                    <a href="<?= $arImgSchem["src"] ?>"
                                                       class="jq-pp" rel="group-pl"
                                                       title="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["DESCRIPTION"] ?>">
                                                        <img src="<?= $arImgSchemFront["src"] ?>"
                                                             width="<?= $arImgSchemFront["width"] ?>"
                                                             height="<?= $arImgSchemFront["height"] ?>"
                                                             alt="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["DESCRIPTION"] ?>"/>
                                                    </a>
                                                </li>
                                                <?
                                            } else:
                                            $arImgSchem = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["ID"], $arSizeFont, BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                            $arImgSchemFront = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["ID"], array("width" => 233, "height" => 180), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                            ?>
                                            <li>
                                                <a href="<?= $arImgSchem["src"] ?>"
                                                   class="jq-pp"
                                                   title="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["DESCRIPTION"] ?>">
                                                    <img src="<?= $arImgSchemFront["src"]; ?>"
                                                         width="<?= $arImgSchemFront["width"] ?>"
                                                         height="<?= $arImgSchemFront["height"] ?>"
                                                         alt="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["DESCRIPTION"] ?>"/>
                                                </a>
                                            </li>
                                        <?endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <!--/ End �������� ���������� /-->
                            <!--/ End ���������� ��������� /-->
                            <div class="navigation">
                                <a href="#" class="prev prev-navigation-pl">&lsaquo;</a>
                                <a href="#" class="next next-navigation-pl">&rsaquo;</a>
                                <div class="carousel carousel-navigation-pl">
                                    <ul>
                                        <?
                                        if (!$arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["SRC"]):
                                            for ($i = 0; $i < sizeof($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]); $i++) {
                                    $arImgSM = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["ID"], array("width" => 70, "height" => 50), BX_RESIZE_IMAGE_EXACT, true);
                                    ?>
                                    <li><img src="<?= $arImgSM["src"] ?>" width="<?= $arImgSM["width"] ?>"
                                             height="<?= $arImgSM["height"] ?>"
                                             alt="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"][$i]["DESCRIPTION"] ?>"/>
                                    </li>
                                <?
                                } else:
                                $arImgSM = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["ID"], array("width" => 70, "height" => 50), BX_RESIZE_IMAGE_EXACT, true);
                                ?>
                                <img src="<?= $arImgSM["src"] ?>" width="<?= $arImgSM["width"] ?>"
                                     height="<?= $arImgSM["height"] ?>"
                                     alt="<?= $arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]["DESCRIPTION"] ?>"/>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <!--/ End  ���������� ��������� /-->
            </div>
        <? else: ?>
            <div class="connected-carousels">
                <div class="stage">
                    <div class="carousel carousel-stage-pl">
                        <a href="<?= $arImgFont["src"] ?>" class="jq-pp" rel="group-rel"
                           title="<?= $arResult["NAME"] ?>"><img src="<?= $arImgFontBI["src"] ?>"
                                                                 width="<?= $arImgFontBI["width"] ?>"
                                                                 height="<?= $arImgFontBI["height"] ?>"
                                                                 alt="<?= $arResult["NAME"] ?>"></a>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <? if(sizeof($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]) > 0): ?>
            <div class="connected-carousels">
                <!--/ �������� ���� /-->
                <div class="stage">
                    <div class="carousel carousel-stage">
                        <ul>
                            <? if ($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]): ?>
                                <li>
                                    <a href="<?= $arImgFont["src"] ?>" class="jq-pp" rel="group-rel" title="<?= $arResult["NAME"] ?>">
                                        <img src="<?= $arImgFontBI["src"] ?>"
                                             width="<?= $arImgFontBI["width"] ?>"
                                             height="<?= $arImgFontBI["height"] ?>"
                                             alt="<?= $arResult["NAME"] ?>">
                                    </a>
                                </li>
                            <? endif; ?>
                            <?if (!$arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["SRC"]):
                                for ($i = 0; $i < sizeof($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]); $i++) {
                                    $arImg = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"][$i]["ID"], $arSizeFont, BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                    $arImgBI = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"][$i]["ID"], array("width" => 233, "height" => 180), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                    ?>
                                    <li><a href="<?= $arImg["src"] ?>" class="jq-pp" rel="group-rel"
                                           title="<?= $arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"][$i]["DESCRIPTION"] ?>"><img
                                                src="<?= $arImgBI["src"] ?>" width="<?= $arImgBI["width"] ?>"
                                                height="<?= $arImgBI["height"] ?>"
                                                alt="<?= $arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"][$i]["DESCRIPTION"] ?>"/></a>
                                    </li>
                                <?
                                } else:
                                $arImg = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["ID"], $arSizeFont, BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                $arImgBI = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["ID"], array("width" => 233, "height" => 180), BX_RESIZE_IMAGE_EXACT, true, $arFilters);
                                ?>
                                <li><a href="<?= $arImg["src"]; ?>" class="jq-pp" rel="group-rel"
                                       title="<?= $arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["DESCRIPTION"] ?>"><img
                                            src="<?= $arImgBI["src"]; ?>" width="<?= $arImgBI["width"] ?>"
                                            height="<?= $arImgBI["height"] ?>"
                                            alt="<?= $arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["DESCRIPTION"] ?>"/></a>
                                </li>
                            <?endif; ?>

                        </ul>
                    </div>
                </div>
                <!--/ End �������� ���� /-->
                <!--/ ��������� /-->
                <div class="navigation">
                    <a href="#" class="prev prev-navigation">&lsaquo;</a>
                    <a href="#" class="next next-navigation">&rsaquo;</a>

                    <div class="carousel carousel-navigation">
                        <ul>
                            <? if($arResult["DISPLAY_PROPERTIES"]["schem"]["FILE_VALUE"]): ?>
                                <li><img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" width="70" height="50" alt=""></li>
                            <? endif; ?>
                            <?if (!$arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["SRC"]):
                                for ($i = 0; $i < sizeof($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]); $i++) {
                                    $arImg = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"][$i]["ID"], array("width" => 70, "height" => 50), BX_RESIZE_IMAGE_EXACT, true);
                                    ?>
                                    <li><img src="<?= $arImg["src"] ?>" width="<?= $arImg["width"] ?>"
                                             height="<?= $arImg["height"] ?>" alt=""/></li>
                                <?
                                } else:
                                $arImg = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["photo"]["FILE_VALUE"]["ID"], array("width" => 70, "height" => 50), BX_RESIZE_IMAGE_EXACT, true);
                                ?>
                                <li><img src="<?= $arImg["src"] ?>" width="<?= $arImg["width"] ?>"
                                         height="<?= $arImg["height"] ?>" alt=""/></li>
                            <?endif; ?>

                        </ul>
                    </div>
                </div>
                <!--/ End ��������� /-->
            </div>
        <? endif; ?>
    <? endif; ?>
    <!--// End ���� //-->
</div>
</div>
        <? if (!empty($arResult["DISPLAY_PROPERTIES"]["ymap"]["VALUE"])): ?>
            <div class="element-ymap gp_show">
                <?
                $tmp = explode(",", $arResult["DISPLAY_PROPERTIES"]["ymap"]["VALUE"]);
                $opts = array(
                    "yandex_lat" => $tmp[0],
                    "yandex_lon" => $tmp[1],
                    "yandex_scale" => 13,
                    "PLACEMARKS" => array(array(
                        "LAT" => $tmp[0],
                        "LON" => $tmp[1],
                    ))
                );
                ?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:map.yandex.view",
                    "",
                    Array(
                        "KEY" => "",
                        "INIT_MAP_TYPE" => "MAP",
                        "MAP_DATA" => serialize($opts),
                        "MAP_WIDTH" => "100%",
                        "MAP_HEIGHT" => "330",
                        "CONTROLS" => array("TOOLBAR", "ZOOM"),
                        "OPTIONS" => array("ENABLE_DBLCLICK_ZOOM"),
                        "MAP_ID" => ""
                    ),
                    false
                );?>
            </div>
        <? endif; ?>
<?
$rsUser = CUser::GetByID($arResult["DISPLAY_PROPERTIES"]["realtor"]["DISPLAY_VALUE"]);
$arUser = $rsUser->Fetch(); ?>
<div class="gp_object_items">
    <div class="element-description">
        <?= $arResult["DETAIL_TEXT"] ?>
        <p>
        <div class="element-feedback">
            ���� ��� ���������� ������ ���� ������, ��������� ������: <a href="tel:<?= $arUser["PERSONAL_PHONE"] ?>"><b><?= $arUser["PERSONAL_PHONE"] ?></b></a><br>
            ��� � ��� ����: <a href="tel:+78123266904"><b>+7&nbsp;(812)&nbsp;326-69-04</b></a><br><br>
            <div class=gp_hide>
                <div class="add-review-link pink-btn"><a class="btn callme" href="#">�������� ������</a></div>
            </div>
        </div>
        </p>
    </div>
</div>

<div class="gp_show">
    <div class="element-form">
        <?$APPLICATION->IncludeComponent(
            "gp:callme",
            "real_estate",
            array(
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "USE_CAPTCHA" => "N",
                "OK_TEXT" => "�������, ���� ������ �������",
                "REQUIRED_FIELDS" => array(
                    0 => "NAME",
                    1 => "PHONE",
                    2 => "MESSAGE",
                ),
                "EMAIL_TO" => "info@peterburg-realtor.ru",
                "IBLOCK_TYPE" => "gp_realestate_callme",
                "IBLOCK_ID" => "2",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "EVENT_MESSAGE_ID" => array(
                    0 => "8",
                )
            ),
            false
        );?>
    </div>
</div>

</div>


<div class="catalog-element-right">
    <? if (!empty($arResult["DISPLAY_PROPERTIES"]["ymap"]["VALUE"])): ?>
        <div class="element-ymap">
            <?
            $tmp = explode(",", $arResult["DISPLAY_PROPERTIES"]["ymap"]["VALUE"]);
            $opts = array(
                "yandex_lat" => $tmp[0],
                "yandex_lon" => $tmp[1],
                "yandex_scale" => 13,
                "PLACEMARKS" => array(array(
                    "LAT" => $tmp[0],
                    "LON" => $tmp[1],
                ))
            );
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:map.yandex.view",
                "",
                Array(
                    "KEY" => "",
                    "INIT_MAP_TYPE" => "MAP",
                    "MAP_DATA" => serialize($opts),
                    "MAP_WIDTH" => "100%",
                    "MAP_HEIGHT" => "330",
                    "CONTROLS" => array("TOOLBAR", "ZOOM"),
                    "OPTIONS" => array("ENABLE_DBLCLICK_ZOOM"),
                    "MAP_ID" => ""
                ),
                false
            );?>
        </div>
    <? endif; ?>

    <?
    if ($arResult["DISPLAY_PROPERTIES"]["realtor"]["DISPLAY_VALUE"]):

        if ($arUser["ID"]):
            ?>
            <!--// Realtor //-->
            <div class="item">
                <?    if (intval($arUser["PERSONAL_PHOTO"]) > 0) {
                    $imageFile = CFile::GetFileArray($arUser["PERSONAL_PHOTO"]);
                    if ($imageFile !== false) {
                        $arFileTmp = CFile::ResizeImageGet(
                            $imageFile,
                            array("width" => 89, "height" => 89),
                            BX_RESIZE_IMAGE_PROPORTIONAL,
                            false
                        );
                        // � ���������� �������� ������� ��� �����������
                        $userPhoto = CFile::ShowImage($arFileTmp["src"], $width, $height, "border='0'", "");
                    }
                } else {
                    $userPhoto = '<img src="' . SITE_DIR . 'upload/no-pic.jpg" border="0" width="89" height="89">';
                }
                ?>

                <div class="table-realtor">
                    <div class="row-realtor">
                        <div class="pic"><?= $userPhoto ?></div>
                        <div class="youexpert"> <?= $arResult["DISPLAY_PROPERTIES"]["realtor"]["NAME"] ?>:&nbsp;
                        </div>
                        <div class="cell-realtor"><?= $arUser["NAME"] . " " . $arUser["LAST_NAME"]; ?>

                            <? if ($arUser["PERSONAL_PHONE"]): ?>
                                <div class="gp_obj-agent-phone">
                                    <a href="tel:<?= $arUser["PERSONAL_PHONE"] ?>"><?= str_replace(" ", "&nbsp;", $arUser["PERSONAL_PHONE"]) ?></a>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <!--// End Realtor //-->
        <? endif; ?>
    <? endif; ?>

    <div class="element-form">
        <?$APPLICATION->IncludeComponent(
            "gp:callme",
            "real_estate",
            array(
                "AJAX_MODE" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "USE_CAPTCHA" => "N",
                "OK_TEXT" => "�������, ���� ������ �������",
                "REQUIRED_FIELDS" => array(
                    0 => "NAME",
                    1 => "PHONE",
                    2 => "MESSAGE",
                ),
                "EMAIL_TO" => "info@peterburg-realtor.ru",
                "IBLOCK_TYPE" => "gp_realestate_callme",
                "IBLOCK_ID" => "2",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "EVENT_MESSAGE_ID" => array(
                    0 => "8",
                )
            ),
            false
        );?>
    </div>

    <? $APPLICATION->AddHeadString('<script type="text/javascript" src="/bitrix/components/gp/search.filter/js/jquery-ui-1.10.3.custom.min.js"></script>') ?>
    <? $APPLICATION->AddHeadString('<script type="text/javascript" src="' . $this->GetFolder() . '/accounting.js"></script>') ?>
    <div class="gp_object_items">
        <div class="mortgage_calculator"><?= GetMessage("CATALOG_MORTGAGE_CALCULATOR"); ?></div>
        <div class="calc-inner">
            <div class="message"><?= GetMessage("CATALOG_MORTGAGE_CALCULATOR_SUM"); ?></div>
            <div id="gp_mount_slider"></div>
            <div id="gp_mount"></div>
            <span class="amendment"><?= $arResult['PROPERTIES']['price_currency']['VALUE'] ?></span>

            <div class="clear"></div>
            <div class="message"><?= GetMessage("CATALOG_MORTGAGE_CALCULATOR_STAV"); ?></div>
            <div id="gp_interest_slider"></div>
            <div id="gp_interest"></div>
            <span class="amendment">%</span>

            <div class="clear"></div>
            <div class="message"><?= GetMessage("CATALOG_MORTGAGE_CALCULATOR_SROK"); ?></div>
            <div id="gp_time_slider"></div>
            <div id="gp_time"></div>
            <div class="clear"></div>
            <div class="message sum"><?= GetMessage("CATALOG_MORTGAGE_CALCULATOR_PLATA"); ?></div>
            <div id="result"></div>
            <span class="amendment-sum"><?= $arResult['PROPERTIES']['price_currency']['VALUE'] ?></span>

            <div class="clear"></div>
        </div>
    </div>
</div>
<div class="clear"></div>
</div>