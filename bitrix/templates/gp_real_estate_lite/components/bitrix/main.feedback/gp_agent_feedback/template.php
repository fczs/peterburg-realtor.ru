<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="gp_feedback">
<script>
jQuery(document).ready(function(){
	setTimeout(function(){jQuery('.gp_ok-text').fadeOut('fast')},3000);
<?if(!empty($arResult["ERROR_MESSAGE"])){	
foreach($arResult["ERROR_MESSAGE"] as $v){
	if(!$arResult['MESSAGE']){
		?>
		jQuery('.gp_mess').click(function(){
			jQuery('.gp_mess').removeClass('gp_errors');
			jQuery('.gp_mess').attr('placeholder', '<?=GetMessage('MFT_MESSAGE')?>');
			});
		jQuery('.gp_mess').addClass('gp_errors');	
		<?
	if(md5($v) == md5(GetMessage("MF_REQ_MESSAGE"))){?>
		jQuery('.gp_mess').attr('placeholder', '<?=$v?>');
	<?}
	}
	if(!$arResult['AUTHOR_NAME']){
		?>
		jQuery('.gp_na').click(function(){
			jQuery('.gp_na').removeClass('gp_errors');
			jQuery('.gp_na').attr('placeholder', '<?=GetMessage('MFT_NAME')?>');
			});
		jQuery('.gp_na').addClass('gp_errors');		
		<?
	if(md5($v) == md5(GetMessage("MF_REQ_NAME"))){?>
		jQuery('.gp_na').attr('placeholder', '<?=$v?>');
	<?}
	}
	if(!$arResult['AUTHOR_EMAIL'] || !check_email($_POST["user_email"])){
		?>
		jQuery('.gp_em').click(function(){
			jQuery('.gp_em').removeClass('gp_errors');
			jQuery('.gp_em').attr('placeholder', '<?=GetMessage('MFT_EMAIL')?>');
			});
		jQuery('.gp_em').addClass('gp_errors');	
		<?
	if(md5($v) == md5(GetMessage("MF_REQ_EMAIL"))){?>
		jQuery('.gp_em').attr('placeholder', '<?=$v?>');
	<?}elseif(md5($v) == md5(GetMessage("MF_EMAIL_NOT_VALID"))){?>
		jQuery('.gp_em').attr('value', '');
		jQuery('.gp_em').attr('placeholder', '<?=$v?>');
	<?}
	}
	if(!$_POST["captcha_word"] || md5($v) == md5(GetMessage("MF_CAPTCHA_WRONG"))){
		?>
		jQuery('.gp_capt').click(function(){
			jQuery('.gp_capt').removeClass('gp_errors');
			jQuery('.gp_capt').attr('placeholder', '<?=GetMessage('MFT_CAPTCHA_CODE')?>');
			});
		jQuery('.gp_capt').addClass('gp_errors');	
		<?
	if(md5($v) == md5(GetMessage("MF_CAPTHCA_EMPTY"))){?>
		jQuery('.gp_capt').attr('placeholder', '<?=$v?>');
	<?}elseif(md5($v) == md5(GetMessage("MF_CAPTCHA_WRONG"))){?>
		jQuery('.gp_capt').attr('value', '');
		jQuery('.gp_capt').attr('placeholder', '<?=$v?>');
	<?}
	}
}
}
?>
});
</script>
<?
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
?><div class="gp_ok-text">
	<p><?=$arResult["OK_MESSAGE"]?></p>
</div><?
}
?>
<form action="<?=$_SERVER["REQUEST_URI"]?>" method="POST">
<?=bitrix_sessid_post()?>
<div class="gp_form-left">
<div class="gp_naem">
	<input type="text" class="gp_na" placeholder="<?=GetMessage("MFT_NAME")?>" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>"> <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="gp_req">*</span><?endif?>
		<br />
	<input type="text" class="gp_em" placeholder="<?=GetMessage("MFT_EMAIL")?>" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>"> <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="gp_req">*</span><?endif?>
	<?if($arParams["USE_CAPTCHA"] == "N"):?>
	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
	<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
	<?endif;?>
</div>

	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div class="gp_captcha">
	<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
	<input type="text" class="gp_capt" placeholder="<?=GetMessage("MFT_CAPTCHA_CODE")?>" name="captcha_word" size="30" maxlength="50" value=""> <span class="gp_req">*</span><br />
	<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
	</div>
	<?endif;?>
</div>
<div class="gp_form-right">	
	<div class="gp_message">
		<textarea name="MESSAGE" class="gp_mess" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea> <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?> <span class="gp_req">*</span><?endif?>
	</div>
	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
	<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
	<?endif;?>
</div>	
</form>
</div>