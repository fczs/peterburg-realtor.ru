<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult))
    return "";

if ($arResult[count($arResult) - 1]["LINK"] != "" && $arResult[count($arResult) - 1]["LINK"] != $GLOBALS["APPLICATION"]->GetCurPage(false))
    $arResult[] = Array("TITLE" => $GLOBALS["APPLICATION"]->GetTitle());

$strReturn = '<div class="gp_breadcrumb-wrapper"><div class="gp_breadcrumb">';

for ($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    $obParser = new CTextParser;
    $title = $obParser->html_cut($title, 30);

    if ($arResult[$index]["LINK"] <> "" && $arResult[$index]["LINK"] != $GLOBALS["APPLICATION"]->GetCurPage(false))
        $strReturn .= ' <a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a> / ';
    else
        $strReturn .= '';//'<span class="end"> ' . $title . '</span>';
}
$strReturn .= '</div></div>';

return $strReturn;
