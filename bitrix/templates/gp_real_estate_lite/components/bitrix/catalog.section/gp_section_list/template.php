<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

function get_sort_link($field, $label, $arParams)
{
    if ($field !== "property_price_currency") {
        if (preg_match('/gp_filter_pf/i', $_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) && !strpos($_SERVER['REQUEST_URI'], 'PAGEN_1')) {
            $sort_link_asc = $_SERVER['REQUEST_URI'] . '&sort=' . $field . '&order=asc';
            $sort_link_desc = $_SERVER['REQUEST_URI'] . '&sort=' . $field . '&order=desc';

        } else if (!strpos($_SERVER['REQUEST_URI'], 'PAGEN_1') && !empty($_REQUEST['PAGEN_1'])) {
            $sort_link_asc = $_SERVER['HTTP_REFERER'] . '&sort=' . $field . '&order=asc';
            $sort_link_desc = $_SERVER['HTTP_REFERER'] . '&sort=' . $field . '&order=desc';

        } else if (isset($_REQUEST["agent"])) {
            $sort_link_asc = '?agent=' . $_REQUEST["agent"] . '&sort=' . $field . '&order=asc';
            $sort_link_desc = '?agent=' . $_REQUEST["agent"] . '&sort=' . $field . '&order=desc';
        } else {
            $sort_link_asc = '?sort=' . $field . '&order=asc';
            $sort_link_desc = '?sort=' . $field . '&order=desc';
        }

        if ($field == $arParams["ELEMENT_SORT_FIELD"]) {
            $order = strtolower($arParams["ELEMENT_SORT_ORDER"]);
            $cur[$order] = "_a";
        } else
            $order = "desc";
        ?>
        <div class="sort-field <?=$field?>">
            <small><?= $label ?>&nbsp;</small>
            <a href="#" onclick="document.location='<?= $sort_link_asc ?>'; return false;"
               title="<?= GetMessage("CATALOG_SORT_ASC") ?>">
                <span class="arrow_up<?= $cur["asc"] ?>" title="<?= GetMessage("CATALOG_SORT_ASC") ?>"></span>
            </a>
            <a href="#" onclick="document.location='<?= $sort_link_desc ?>'; return false;"
               title="<?= GetMessage("CATALOG_SORT_DESC") ?>">
                <span class="arrow_down<?= $cur["desc"] ?>" title="<?= GetMessage("CATALOG_SORT_DESC") ?>"></span>
            </a>
        </div>
    <?
    }
}
?>

<? $APPLICATION->AddHeadString('<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>'); ?>

<script type="text/javascript">
    $(function () {
        var $item = $('#gp_search-list .gp_name #gp_objects');

        $(window).on('load resize', function () {
            if ($(window).width() <= 970 && $('.list-view').hasClass('active')) setMatrixView();
        });

        $('.view-switch a').on('click', function () {
            if ($(this).hasClass('list-view')) {
                setListView();
                $.cookie('peterburg-realtor.catalog_view', 'list');
                console.log('list')
            } else if ($(this).hasClass('matrix-view')) {
                setMatrixView();
                $.cookie('peterburg-realtor.catalog_view', 'matrix');
                console.log('matrix')
            }
        });

        if ($('#gp_search-list').length && $.cookie('peterburg-realtor.catalog_view') == 'list')
            setListView();
        else
            setMatrixView();

        function setListView() {
            $item.each(function () {
                $(this).removeClass('gp_object-item c_fix caption caption-3').addClass('gp_object-item-list');
            });
            $('.matrix-view').removeClass('active');
            $('.list-view').addClass('active');
        }

        function setMatrixView() {
            $item.each(function () {
                $(this).removeClass('gp_object-item-list').addClass('gp_object-item c_fix caption caption-3');
            });
            $('.list-view').removeClass('active');
            $('.matrix-view').addClass('active');
        }
    });
</script>

<h1 class="catalog-element-title"><?= $arResult["NAME"] ?><? if (in_array(getCatPage($APPLICATION->GetCurPage(false)), unserialize(REALTY_CATEGORY))): ?> <?= GetMessage("CATALOG_REALTY")?><? endif; ?></h1>

<? if (count($arResult["ITEMS"]) == 0): ?>
    <div class="content-left">
        <div class="gp_not-found">
            <?= GetMessage("CATALOG_NOT_FOUND"); ?>
            <div>
                <br>
            </div>
        </div>
    </div>

    <div class="content-right">
        <?$APPLICATION->IncludeFile(
            SITE_DIR . "include/right_block.php",
            Array(),
            Array("MODE" => "html")
        );?>
    </div>
    <div class="clear"></div>
<? else: ?>
    <?/* <script type="text/javascript">
        ymaps.ready(init);
        function init() {
            var map = new ymaps.Map('map', {
                        center: [59.95, 30.31666759],
                        zoom: 9
                    },
                    {
                        minZoom: 1,
                        maxZoom: 17
                    }),
                myGeoObjects = [];

            <?
            $i = 0;
            $arYmapValues = array();
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_YMAP", "PROPERTY_ADDRESS", "PROPERTY_NUMBER", "PROPERTY_DISTRICT");
            $arFilter = Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "SECTION_ID" => $arResult["ID"], "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while($arRes = $res->GetNext()) {
                $k = 0;
                foreach ($arYmapValues as $arYmap) {
                    if ($arYmap["YMAP"] == $arRes["PROPERTY_YMAP_VALUE"]) {
                        $k = 1;
                    }
                }
                if ($k == 0) {
                    $sStreet = CIBlockElement::GetProperty($arResult["IBLOCK_ID"], $arRes["ID"], array("sort" => "asc"), array("ID" => PROP_STREET_ID));
                    if ($ob = $sStreet->GetNext()) {
                        $streetElement = CIBlockElement::GetByID($ob["VALUE"]);
                        if($street = $streetElement->GetNext())
                           $arYmapValues[$i] = array("YMAP" => $arRes["PROPERTY_YMAP_VALUE"], "STREET" => $street["NAME"], "NUMBER" => $arRes["PROPERTY_NUMBER_VALUE"], "DISTRICT" => $arRes["PROPERTY_DISTRICT_VALUE"]);
                    }
                $i++;
                }
            }
            ?>

            <?$objcount = 0;?>

            <?foreach($arYmapValues as $arYmap):?>

            <?$objcount++;?>

            myGeoObjects[<?=$objcount;?>] = new ymaps.GeoObject({
                geometry: {type: "Point", coordinates: [<?=$arYmap["YMAP"]?>]},
                properties: {

                    iconContent: '<?//=$objcount;?>',
                    //clusterCaption: '<?=$arItem["NAME"]?>',
                    balloonContentBody:
                        '<div class="map-control-name">' +
                        '<?=$arYmap["DISTRICT"]?>' +
                        '<?if(!empty($arYmap["DISTRICT"])) echo "<br>";?>' +
                        '<?=$arYmap["STREET"]?>' +
                        '<?if(!empty($arYmap["NUMBER"])) echo ", " . $arYmap["NUMBER"];?>' +
                        '</div>' +
                        '<?
                         $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_YMAP", "PROPERTY_PRICE", "PROPERTY_ALL_AREA", "DETAIL_PAGE_URL");
                         $arFilter = Array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "SECTION_ID" => $arResult["ID"], "ACTIVE"=>"Y");
                         $res = CIBlockElement::GetList(Array("NAME" => "ASC", "PROPERTY_ALL_AREA" => "ASC"), $arFilter, false, false, $arSelect);
                         while($arItem = $res->GetNext()) {
                             if($arItem['PROPERTY_YMAP_VALUE'] == $arYmap["YMAP"]) {
                                 echo '<div class="gp_title map-control">' .
                                      '<a class="gp_name" target="_blank" href="' . $arItem["DETAIL_PAGE_URL"] . '/">' .
                                      $arItem["NAME"] .
                                      '</a>' .
                                      '<span class="map-price">' .
                                      round($arItem["PROPERTY_PRICE_VALUE"]/1000000, 2) .
                                      ' ���. �.</span>' .
                                      '<span class="metr">' .
                                      $arItem["PROPERTY_ALL_AREA_VALUE"] .
                                      GetMessage('CATALOG_ALL_AREA') .
                                      '</span>' .
                                      '</div>' .
                                      '<div class="gp_object-item-map gp_object-item map-control' . $arItem["ID"] . '" data-id="' . $arItem["ID"] . '">' .
                                      '</div>';
                            }
                        }?>',
                    itemId: '<?=$arItem["ID"]?>'
                }
            }, {
                iconImageHref: '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>',
                iconImageSize: [30, 30]
            });

            <?endforeach;?>

            for (i in myGeoObjects) {
                myGeoObjects[i].events.add('click', function (obj) {
                    map.resetPreset();
                    var id = this.properties.get('itemId');
                    $("#gp_search-list .gp_object-item").removeClass('active');
                    $("#gp_search-list .gp_object-item" + id).addClass('active');
                }, myGeoObjects[i]);
                map.geoObjects.add(myGeoObjects[i]);
            }
            map.controls.add('zoomControl').add('typeSelector').add('mapTools');
            map.setBounds(map.geoObjects.getBounds());
            map.resetPreset = function () {
                map.geoObjects.each(function (obj) {
                    obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                });
            }
            map.balloon.events.add('close', function () {
                map.resetPreset();
                $("#gp_search-list .gp_object-item").removeClass('active');
            });
            $("#gp_search-list .gp_object-item").on('mouseenter', function () {
                var id = $(this).data('id');
                map.geoObjects.each(function (obj) {
                    if (obj.properties.get('itemId') == id) {
                        obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                    }
                    else {
                        obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                    }
                });
            })
                .on('mouseleave', function () {
                    map.resetPreset();
                })
                .on("click", function () {
                    var $this = this;
                    var id = $(this).data('id');
                    map.balloon.close();
                    map.geoObjects.each(function (obj) {
                        if (obj.properties.get('itemId') == id) {
                            obj.balloon.open();
                            $("#gp_search-list .gp_object-item").removeClass('active');
                            $($this).addClass('active');
                        }
                    });
                })
        }
    </script> */?> <? /* Old Map */ ?>
    <?
    $objects = [];
    foreach ($arResult["ITEMS"] as $arElement) {
        $objects[] = array(
            "ID" => $arElement["ID"],
            "NAME" => $arElement["NAME"],
            "YMAP" => $arElement['PROPERTIES']['ymap']['VALUE'],
            "DISTRICT" => $arElement["DISPLAY_PROPERTIES"]["district"]["DISPLAY_VALUE"],
            "ADDRESS" => strip_tags($arElement["DISPLAY_PROPERTIES"]["address"]["DISPLAY_VALUE"]),
            "NUMBER" => $arElement["DISPLAY_PROPERTIES"]["number"]["DISPLAY_VALUE"],
            "URL" => $arElement["DETAIL_PAGE_URL"],
            "PRICE" => $arElement['PROPERTIES']["price"]["VALUE"],
            "AREA" => $arElement['PROPERTIES']["all_area"]["VALUE"]);
    }

    usort($objects, function ($a, $b) {
        return strcasecmp($a['YMAP'], $b['YMAP']);
    });

    $magicString = ",itemId:'0'}},{iconImageHref:'".SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'."',iconImageSize: [30, 30]});";
    ?>

    <script type="text/javascript">
         ymaps.ready(init);
         function init() {
             var map = new ymaps.Map('map', {
                         center: [59.95, 30.31666759],
                         zoom: 9
                     },
                     {
                         minZoom: 1,
                         maxZoom: 17
                     }),
                 myGeoObjects = [];

             <? $objCount = 0; $tmpYmap = ""; $tmpId = "";?>
             <? foreach ($objects as $object): ?>
                 <? $objCount++ ?>
                 <? if($object["YMAP"] != $tmpYmap): ?>
                     <? if($objCount != 1) echo $magicString; ?>
                     myGeoObjects[<?= $objCount ?>] = new ymaps.GeoObject({
                         geometry: {type: "Point", coordinates: [<?= $object["YMAP"] ?>]},
                         properties: {
                             balloonContentBody:
                                 '<div class="map-control-name">' +
                                     '<?= $object["DISTRICT"] ?>' +
                                     '<? if(!empty($object["DISTRICT"])) echo "<br>" ?>' +
                                     '<?= $object["ADDRESS"] ?>' +
                                     '<? if(!empty($object["NUMBER"])) echo ", " . $object["NUMBER"] ?>' +
                                     '</div>' +
                                     '<? echo '<div class="gp_title map-control element--' . $object["ID"] . '">' .
                                              '<a class="gp_name" target="_blank" href="' . $object["URL"] . '">' . $object["NAME"] . '</a>' .
                                              '<span class="map-price">' . round($object["PRICE"]/1000000, 2) . ' ���. �.</span>' .
                                              '<span class="metr">' . $object["AREA"] . GetMessage('CATALOG_ALL_AREA') . '</span>' .
                                              '</div>' ?>'
                 <? else: ?>
                                     +'<? echo '<div class="gp_title map-control element--' . $object["ID"] . '">' .
                                               '<a class="gp_name" target="_blank" href="' . $object["URL"] . '">' . $object["NAME"] . '</a>' .
                                               '<span class="map-price">' . round($object["PRICE"]/1000000, 2) . ' ���. �.</span>' .
                                               '<span class="metr">' . $object["AREA"] . GetMessage('CATALOG_ALL_AREA') . '</span>' .
                                               '</div>' ?>'
                 <? endif ?>
                 <? $tmpYmap = $object["YMAP"]; $tmpId = $object["ID"]; ?>
             <? endforeach ?>
             <?= $magicString ?>

             for (i in myGeoObjects) {
                 myGeoObjects[i].events.add('click', function (obj) {
                     map.resetPreset();
                     var id = this.properties.get('itemId');
                     $("#gp_search-list .gp_object-item").removeClass('active');
                     $("#gp_search-list .gp_object-item" + id).addClass('active');
                 }, myGeoObjects[i]);
                 map.geoObjects.add(myGeoObjects[i]);
             }
             map.controls.add('zoomControl').add('typeSelector').add('mapTools');
             map.setBounds(map.geoObjects.getBounds());
             map.resetPreset = function () {
                 map.geoObjects.each(function (obj) {
                     obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                 });
             }
             map.balloon.events.add('close', function () {
                 map.resetPreset();
                 $("#gp_search-list .gp_object-item").removeClass('active');
             });
             $("#gp_search-list .gp_object-item").on('mouseenter', function () {
                 var id = $(this).data('id');
                 map.geoObjects.each(function (obj) {
                     if (obj.properties.get('itemId') == id) {
                         obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                     }
                     else {
                         obj.options.set('iconImageHref', '<?= SITE_TEMPLATE_PATH.'/images/catalog.section/images/map-logo.png'?>');
                     }
                 });
             })
                 .on('mouseleave', function () {
                     map.resetPreset();
                 })
                 .on("click", function () {
                     var $this = this;
                     var id = $(this).data('id');
                     map.balloon.close();
                     map.geoObjects.each(function (obj) {
                         if (obj.properties.get('itemId') == id) {
                             obj.balloon.open();
                             $("#gp_search-list .gp_object-item").removeClass('active');
                             $($this).addClass('active');
                         }
                     });
                 })
         }
    </script>

    <div class="gp_catalog-section">
        <div id="gp_search-list">
            <div class="gp_arrow-block">
                <?if (count($arResult["ITEMS"]) > 0):
                    foreach ($arResult["ITEMS"][0]["DISPLAY_PROPERTIES"] as $arProperty):?>
                        <? if ($arProperty["CODE"] == "price" || $arProperty["CODE"] == "all_area") : ?>
                            <? get_sort_link("property_" . $arProperty["CODE"], $arProperty["NAME"], $arParams); ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <div class="view-switch">
                        <a onclick="return false;" href="javascript:void(0);" class="matrix-view active"></a>
                        <a onclick="return false;" href="javascript:void(0);" class="list-view"></a>
                    </div>
                <? endif; ?>
            </div>
            <div class="clear"></div>
            <? $objcount = 0; ?>
            <? foreach ($arResult["ITEMS"] as $arElement): ?>
                <?
                $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
                ?>
                <?$objcount++;
                $arImg = CFile::ResizeImageGet($arElement["DETAIL_PICTURE"]["ID"], array("width" => "165", "height" => "110"), BX_RESIZE_IMAGE_EXACT, true);
                ?>
                <a class="gp_name" href="<?= $arElement["DETAIL_PAGE_URL"] ?>">
                    <div id="gp_objects" class="gp_object-item gp_object-item<?= $arElement["ID"] ?> c_fix caption caption-3" data-id="<?= $arElement["ID"] ?>" data-description="<?=GetMessage('CATALOG_MORE')?>">

                        <div class="gp_title">
                            <?= $arElement["NAME"] ?>
                        </div>
                        <div class="gp_object-item-pic">
                            <img width="<?= $arImg["width"] ?>" height="<?= $arImg["height"] ?>" src="<?= $arImg["src"] ?>" alt="<?= $arElement["NAME"] ?>">
                        </div>
                        <div class="gp_price"><?= number_format($arElement['PROPERTIES']['price']['VALUE'], 0, '.', ' ') ?>
                            <span class="rub<? if ($arElement['PROPERTIES']['price_currency']['VALUE'] == "���."): ?> rubble"><? else: ?>"><?= $arElement['PROPERTIES']['price_currency']['VALUE']; ?><? endif; ?></span>
                        </div>
                        <? if (!empty($arElement['PROPERTIES']['room_area']['VALUE'])): ?>
                            <div class="metr">
                                <?= $arElement['PROPERTIES']['room_area']['VALUE']; ?><?= GetMessage('CATALOG_ALL_AREA') ?>
                            </div>
                        <? elseif (!empty($arElement['PROPERTIES']['all_area']['VALUE'])): ?>
                            <div class="metr">
                                <?= $arElement['PROPERTIES']['all_area']['VALUE']; ?><?= GetMessage('CATALOG_ALL_AREA') ?>
                            </div>
                        <? endif; ?>
                        <? if (!empty($arElement['PROPERTIES']['site_area']['VALUE'])): ?>
                            <div class="site">
                                <?= $arElement['PROPERTIES']['site_area']['VALUE']; ?><?= GetMessage('CATALOG_SITE_AREA') ?>
                            </div>
                        <? endif; ?>
                        <? if ($APPLICATION->GetCurPage(false) == "/catalog/commercial/"):?>
                            <div class="rent <?if($arElement['PROPERTIES']['type_of_deal']['VALUE_SORT']=="10"):?>lease<? else: ?>sale<? endif; ?>"><?= $arElement['PROPERTIES']['type_of_deal']['VALUE']; ?></div>
                        <? endif ?>
                        <div class="gp_object-item-detail">
                            <div class="gp_object-item-desc">
                                <? foreach ($arElement["DISPLAY_PROPERTIES"] as $pid => $arProperty): ?>
                                    <? if($arProperty['CODE'] == "district"): ?>
                                        <? if($arProperty["DISPLAY_VALUE"]): ?>
                                            <div class="list-cell">
                                                <div class="detail-label"><?= $arProperty["NAME"]; ?></div>
                                                <div class="gp_obj-dist detail-value"><?= $arProperty["DISPLAY_VALUE"]; ?></div>
                                            </div>
                                        <? endif; ?>
                                    <? endif; ?>
                                    <? if($arProperty['CODE'] == "address"): ?>
                                        <? if($arProperty["DISPLAY_VALUE"]): ?>
                                            <div class="list-cell addr-cell">
                                                <div class="detail-label"><?= $arProperty["NAME"]; ?></div>
                                                <div class="gp_obj-address detail-value"><?= strip_tags($arProperty["DISPLAY_VALUE"]); ?></div>
                                            </div>
                                        <? endif; ?>
                                    <? endif; ?>
                                    <? if($arProperty['CODE'] == "number"): ?>
                                        <? if($arProperty["DISPLAY_VALUE"]): ?>
                                            <div class="list-cell number-cell">
                                                <div class="detail-label"><?= $arProperty["NAME"]; ?></div>
                                                <div class="gp_obj-number detail-value"><span class="list-hide">, </span><?= $arProperty["DISPLAY_VALUE"] ?></div>
                                            </div>
                                        <? endif; ?>
                                    <? endif; ?>
                                    <? if($arProperty['CODE'] == "metro"): ?>
                                        <? if($arProperty["VALUE"]): ?>
                                            <?
                                            $strMetro =  $arProperty["VALUE"]["0"];
                                            if(count($arProperty["VALUE"]) > 1) {
                                                foreach($arProperty["VALUE"] as $k => $v) {
                                                    if($k == 0) continue;
                                                    $strMetro .= ", " . $v;
                                                }
                                            }
                                            ?>
                                            <div class="list-cell">
                                                <div class="detail-label"><?= $arProperty["NAME"]; ?></div>
                                                <div class="gp_obj-metro detail-value"><?= $strMetro ?></div>
                                            </div>
                                        <? endif; ?>
                                    <? endif; ?>
                                    <? if($arProperty['CODE'] == "term"): ?>
                                        <? if($arProperty["DISPLAY_VALUE"]): ?>
                                            <div class="list-cell">
                                                <div class="detail-label"><?= $arProperty["NAME"]; ?></div>
                                                <div class="gp_obj-term detail-value"><?= $arProperty["DISPLAY_VALUE"] ?></div>
                                            </div>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </a>
            <? endforeach; ?>

            <div class="clear"></div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <div>
                    <?= $arResult["NAV_STRING"] ?>
                </div>
            <? endif ?>
        </div>
        <div class="clear"></div>
        <div class="gp_hide">
            <div id="map"></div>
        </div>
    </div>
<?endif?>