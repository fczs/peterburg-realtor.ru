<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>


    <!--<li>
        <div class="teaser">
            <a href="/cooperating/applications/">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                <div class="fade"></div>
                <div class="span-center"><span>������ ���������</span></div>
            </a>
        </div>
    </li>
    <li>
        <div class="teaser">
            <a href="/cooperating/needs/">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/teaser/services-x.jpg">

                <div class="fade"></div>
                <div class="span-center"><span>���������</span></div>
            </a>-->


    <ul>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <li>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                    <div class="news-teaser">
                        <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div class="news-item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                                    <img
                                        class="preview_picture"
                                        border="0"
                                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                        width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                                        height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                                        />
                                <? else: ?>
                                    <img
                                        class="preview_picture"
                                        border="0"
                                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                        width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>"
                                        height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>"
                                        alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                        title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                                        />
                                <?endif; ?>
                            <? endif ?>
                            <div class="news-date-time">
                                <? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
                                    <? echo $arItem["DISPLAY_ACTIVE_FROM"] ?>
                                <? endif ?>
                            </div>
                            <? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]): ?>
                                <? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
                                    <h3><? echo $arItem["NAME"] ?></h3>
                                <? else: ?>
                                    <h3><? echo $arItem["NAME"] ?><h3>
                                <? endif; ?>
                            <? endif; ?>
                            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                <div class="news-preview-text"><?=$arItem["PREVIEW_TEXT"]; ?></div>
                            <? endif; ?>
                            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])): ?>
                                <div style="clear:both"></div>
                            <? endif ?>
                        </div>
                    </div>
                </a>
            </li>
        <? endforeach; ?>
    </ul>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
