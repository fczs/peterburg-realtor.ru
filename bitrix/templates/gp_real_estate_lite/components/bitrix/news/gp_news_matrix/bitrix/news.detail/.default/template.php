<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-detail">
    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
        <img
            class="detail_picture"
            border="0"
            src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
            width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
            height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
            alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
            title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
            />
    <?endif?>
    <?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
        <h1><?=$arResult["NAME"]?></h1>
    <?endif;?>
    <?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
        <span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
    <?endif;?>
    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
        <p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
    <?endif;?>
    <?if($arResult["NAV_RESULT"]):?>
        <?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
        <?echo $arResult["NAV_TEXT"];?>
        <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
    <?elseif($arResult["PROPERTIES"]["IS_TABLE"]["VALUE"] == "Y"):?>
        <link rel='stylesheet' href='<?= SITE_TEMPLATE_PATH ?>/js/watable-master/watable.css'/>
        <link rel='stylesheet' href="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/bootstrap.min.css"/>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/jquery.watable.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/objects.add.list.js"></script>
        <div id="object-table"></div>
        <script type="text/javascript">
            function generateData() {
                var data = {
                    cols: {
                        �����: {
                            index: 1,
                            type: "string"
                        },
                        ������������: {
                            index: 2,
                            type: "string"
                        },
                        �������: {
                            index: 3,
                            type: "string"
                        }
                    },
                    rows: [
                        <? $elements = CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID" => 10, "SECTION_ID" => $arResult["PROPERTIES"]["ID_TABLE"]["VALUE"]), false, false, Array("ID", "NAME", "PROPERTY_ROUTE_NAME", "PROPERTY_ROUTE")); ?>
                        <? while($e = $elements->Fetch()): ?>
                        {
                            �����: "<?= $e["NAME"] ?>",
                            ������������: '<?= $e["PROPERTY_ROUTE_NAME_VALUE"] ?>',
                            �������: '<?= $e["PROPERTY_ROUTE_VALUE"] ?>'
                        },
                        <? endwhile; ?>
                    ]
                };
                return data;
            }
            function getDate(date) {
                var d = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
                return d.getTime();
            }
        </script>
    <?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
        <div class="detail-news-text"><?echo $arResult["DETAIL_TEXT"];?></div>
    <?else:?>
        <?echo $arResult["PREVIEW_TEXT"];?>
    <?endif?>
    <div class="clear"></div>
    <?foreach($arResult["FIELDS"] as $code=>$value):
        if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
        {
            ?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
            if (!empty($value) && is_array($value))
            {
                ?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
            }
        }
        else
        {
            ?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
        }
        ?><br />
    <?endforeach;?>
   
    <? if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
    {
        ?>
        <div class="news-detail-share">
            <noindex>
                <?
                $APPLICATION->IncludeComponent("bitrix:main.share", "", array(
                        "HANDLERS" => $arParams["SHARE_HANDLERS"],
                        "PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
                        "PAGE_TITLE" => $arResult["~NAME"],
                        "SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
                        "SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
                        "HIDE" => $arParams["SHARE_HIDE"],
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );
                ?>
            </noindex>
        </div>
    <?
    }
    ?>
</div>