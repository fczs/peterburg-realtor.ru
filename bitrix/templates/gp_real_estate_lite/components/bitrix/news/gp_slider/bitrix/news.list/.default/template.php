<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="banner">
    <div id="slides">
        <div class="slides_container">
            <?
            foreach ($arResult["ITEMS"] as $arItem):
                $arImg = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array("width" => 323, "height" => 323), BX_RESIZE_IMAGE_EXACT, true);

                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                if (isset($arItem["PROPERTIES"]["slider"]["VALUE"]) && !empty($arItem["PROPERTIES"]["slider"]["VALUE"])):
                    ?>
                    <div class="slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>">
                            <? if (!preg_match("/(MSIE|rv:11.0)/", $_SERVER['HTTP_USER_AGENT'])): ?>
                                <div class="gpslide_bg" style="background:url(<?= $arImg["src"] ?>) no-repeat ; background-position:center center; -o-background-size: 100% 100%, auto; -moz-background-size: 100% 100%, auto; -webkit-background-size: 100% 100%, auto; background-size: 100% 100%, auto;"></div>
                            <? endif; ?>
                            <div class="slides_container_two">
                                <h3><?= $arItem["NAME"] ?></h3>
                                <?
                                $obParser = new CTextParser;
                                $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"], 500);
                                ?>
                                <div class="slider-preview-text">
                                    <?= $arItem["PREVIEW_TEXT"]; ?>
                                </div>
                                <div class="view_price">
                                    <div class="gp_price">
                                        <?= number_format($arItem["PROPERTIES"]["price"]["VALUE"], 0, '.', ' ') ?>
                                        <i class="rub<?if($arItem['PROPERTIES']['price_currency']['VALUE'] == "���."): ?> rubble<? endif; ?>"><?if($arItem['PROPERTIES']['price_currency']['VALUE'] != "���.") echo $arItem['PROPERTIES']['price_currency']['VALUE'];?></i>
                                    </div>
                                </div>

                                <div class="gp_slvklad">
                                    <div class="gp_slvklad_m"><?= $arItem["PROPERTIES"]["all_area"]["VALUE"] ?> <?= GetMessage("GP_ALL_AREA") ?></div>
                                    <div class="gp_slvklad_r"><?= $arItem["PROPERTIES"]["district"]["VALUE"] ?></div>
                                    <div class="gp_slvklad_und">
                                        <span class="gp_obj-metro"><?= $arItem["PROPERTIES"]["metro"]["VALUE"][0] ?></span>
                                    </div>
                                </div>

                                <img class="img-front" width="<?= $arImg["width"] ?>" height="<?= $arImg["height"] ?>" src="<?= $arImg["src"] ?>" border="0" alt="<?= $arItem["NAME"] ?>">
                            </div>
                        </a>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
        <a href="#" class="prev"><span></span></a>
        <a href="#" class="next"><span></span></a>
    </div>
</div>
