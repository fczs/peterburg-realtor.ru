<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);?>

<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/add-element.js"></script>

<?/*if($USER->isAdmin()) {
    echo "<pre>";
    print_r($arResult);
    echo "</pre>";
}*/?>

<div class="fadeout-div">
    <? if (!empty($arResult["ERRORS"])): ?>
        <? ShowError(implode("<br />", $arResult["ERRORS"])) ?>
    <?endif;
    if (strlen($arResult["MESSAGE"]) > 0):?>
        <? ShowNote($arResult["MESSAGE"]) ?>
    <? endif ?>
</div>

<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
<?= bitrix_sessid_post() ?>
<? if ($arParams["MAX_FILE_SIZE"] > 0): ?><input type="hidden" name="MAX_FILE_SIZE"
                                                 value="<?= $arParams["MAX_FILE_SIZE"] ?>" /><? endif ?>
<table class="data-table">
<? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
    <tbody>
    <? foreach ($arResult["PROPERTY_LIST"] as $propertyID): ?>
        <tr data-name="<?=$propertyID?>">
        <td><? if (intval($propertyID) > 0): ?><?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"] ?><? else: ?><?= !empty($arParams["CUSTOM_TITLE_" . $propertyID]) ? $arParams["CUSTOM_TITLE_" . $propertyID] : GetMessage("IBLOCK_FIELD_" . $propertyID) ?><?endif ?><? if (in_array($propertyID, $arResult["PROPERTY_REQUIRED"])): ?>
                <span class="starrequired">*</span><? endif ?>
            <?if($arResult["PROPERTY_LIST_FULL"][$propertyID]["HINT"]):?><div class="prop-hint"><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["HINT"]?></div><?endif;?>
            <?if($propertyID == "PREVIEW_TEXT"):?><div class="prop-hint"><?=GetMessage('PREVIEW_TEXT_HINT')?></div><?endif;?>
            <?if($propertyID == "DETAIL_TEXT"):?><div class="prop-hint"><?=GetMessage('DETAIL_TEXT_HINT')?></div><?endif;?>
            <?if($propertyID == "DETAIL_PICTURE"):?><div class="prop-hint"><?=GetMessage('DETAIL_PICTURE_HINT')?></div><?endif;?>
        </td>
        <td>
        <?
        if (intval($propertyID) > 0) {
            if (
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                &&
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
            )
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
            elseif (
                (
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                    ||
                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                )
                &&
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
            )
                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
        } elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y") {
            $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
            $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
        } else {
            $inputNum = 1;
        }

        if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
            $INPUT_TYPE = "USER_TYPE";
        else
            $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

        switch ($INPUT_TYPE):
            case "USER_TYPE":
                for ($i = 0; $i < $inputNum; $i++) {
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["~VALUE"] : $arResult["ELEMENT"][$propertyID];
                        $description = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["DESCRIPTION"] : "";
                    } elseif ($i == 0) {
                        $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                        $description = "";
                    } else {
                        $value = "";
                        $description = "";
                    }
                    echo call_user_func_array($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"],
                        array(
                            $arResult["PROPERTY_LIST_FULL"][$propertyID],
                            array(
                                "VALUE" => $value,
                                "DESCRIPTION" => $description,
                            ),
                            array(
                                "VALUE" => "PROPERTY[" . $propertyID . "][" . $i . "][VALUE]",
                                "DESCRIPTION" => "PROPERTY[" . $propertyID . "][" . $i . "][DESCRIPTION]",
                                "FORM_NAME" => "iblock_add",
                            ),
                        ));
                    ?><br/><?
                }
                break;
            case "TAGS":
                $APPLICATION->IncludeComponent(
                    "bitrix:search.tags.input",
                    "",
                    array(
                        "VALUE" => $arResult["ELEMENT"][$propertyID],
                        "NAME" => "PROPERTY[" . $propertyID . "][0]",
                        "TEXT" => 'size="' . $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] . '"',
                    ), null, array("HIDE_ICONS" => "Y")
                );
                break;
            case "HTML":
                $LHE = new CLightHTMLEditor;
                $LHE->Show(array(
                    'id' => preg_replace("/[^a-z0-9]/i", '', "PROPERTY[" . $propertyID . "][0]"),
                    'width' => '100%',
                    'height' => '200px',
                    'inputName' => "PROPERTY[" . $propertyID . "][0]",
                    'content' => $arResult["ELEMENT"][$propertyID],
                    'bUseFileDialogs' => false,
                    'bFloatingToolbar' => false,
                    'bArisingToolbar' => false,
                    'toolbarConfig' => array(
                        'Bold', 'Italic', 'Underline', 'RemoveFormat',
                        'CreateLink', 'DeleteLink', 'Image', 'Video',
                        'BackColor', 'ForeColor',
                        'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyFull',
                        'InsertOrderedList', 'InsertUnorderedList', 'Outdent', 'Indent',
                        'StyleList', 'HeaderList',
                        'FontList', 'FontSizeList',
                    ),
                ));
                break;
            case "T":
                for ($i = 0; $i < $inputNum; $i++) {

                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                    } elseif ($i == 0) {
                        $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                    } else {
                        $value = "";
                    }
                    ?>
                    <textarea cols="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                              rows="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] ?>"
                              name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
                <?
                }
                break;

            case "S":
            case "N":
                for ($i = 0; $i < $inputNum; $i++) {
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                    } elseif ($i == 0) {
                        $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                    } else {
                        $value = "";
                    }
                    ?>
                    <? if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "map_yandex"): ?>
                        <?
                        CModule::IncludeModule('iblock');
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/admin_tools.php');
                        $property_fields = $arResult["PROPERTY_LIST_FULL"][$propertyID];
                        $arUserType = CIBlockProperty::GetUserType($property_fields["USER_TYPE"]);
                        echo call_user_func_array($arUserType["GetPropertyFieldHtml"],
                            array(
                                $property_fields,
                                array("VALUE" => $value),
                                array(
                                    "VALUE" => 'PROPERTY[' . $property_fields["ID"] . '][' . $i . ']',
                                    "DESCRIPTION" => 'PROPERTY[' . $property_fields["ID"] . '][' . $i . '][DESCRIPTION]',
                                    "FORM_NAME" => 'iblock_add',
                                    "MODE" => "FORM_FILL",
                                    "COPY" => false,
                                ),
                            )
                        );
                        ?>

                    <?
                    elseif ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "UserID"):
                        if($propertyID == 51 && !empty($arResult["ELEMENT_PROPERTIES"][51][0]["VALUE"]))
                            $userID = $arResult["ELEMENT_PROPERTIES"][51][0]["VALUE"];
                        elseif($propertyID == 87)
                            $userID = 3;
                        else
                            $userID = $USER->GetID();
                        $rsUser = CUser::GetByID($userID);
                        $arUser = $rsUser->Fetch();
                        $arUser_name = $arUser["NAME"] . " " . $arUser["LAST_NAME"];
                        ?>

                        <input type="hidden" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25"
                               value="<?= $userID ?>"/><strong><?= $arUser_name ?></strong><br/>
                    <?elseif($propertyID == 0):?>
                        <select id="names-list" name="PROPERTY[<?=PROP_NAMES_LIST_ID?>]">
                            <option value=""><?=GetMessage("CT_BIEAF_PROPERTY_VALUE_NA")?></option>
                            <?
                            foreach ($arResult["PROPERTY_LIST_FULL"][PROP_NAMES_LIST_ID]["ENUM"] as $key => $arEnum) {
                                $checked = false;
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    foreach ($arResult["ELEMENT_PROPERTIES"][PROP_NAMES_LIST_ID] as $elKey => $arElEnum) {
                                        if ($key == $arElEnum["VALUE"]) {
                                            $checked = true;
                                            break;
                                        }
                                    }
                                } else {
                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                }
                                ?>
                                <option
                                    value="<?= $arEnum["VALUE"] ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                            <?
                            }
                            ?>
                        </select>
                        <input id="obj-name" type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25" style="display: none"
                               value="<?= $value ?>"/><br/>
                    <?else:?>
                        <input <?if($propertyID == PROP_DATE_TERM_ID):?>id="date-term"<?endif?> type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25"
                               value="<?= $value ?>"/><br/>

                    <?endif?>
                    <?
                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "DateTime"):?><?
                        $APPLICATION->IncludeComponent(
                            'bitrix:main.calendar',
                            '',
                            array(
                                'FORM_NAME' => 'iblock_add',
                                'INPUT_NAME' => "PROPERTY[" . $propertyID . "][" . $i . "]",
                                'INPUT_VALUE' => $value,
                            ),
                            null,
                            array('HIDE_ICONS' => 'Y')
                        );
                        ?><br/>
                        <small><?= GetMessage("IBLOCK_FORM_DATE_FORMAT") ?><?= FORMAT_DATETIME ?></small><?
                    endif
                    ?><br/><?
                }
                break;

            case "F":

                for ($i = 0; $i < $inputNum; $i++) {
                    $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                    ?>
                    <input type="hidden"
                           name="PROPERTY[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                           value="<?= $value ?>"/>
                    <input type="file" size="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                           name="PROPERTY_FILE_<?= $propertyID ?>_<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>"/>
                    <br/>
                    <?

                    if (!empty($value) && is_array($arResult["ELEMENT_FILES"][$value])) {
                        ?>
                        <input type="checkbox"
                               name="DELETE_FILE[<?= $propertyID ?>][<?= $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE_ID"] : $i ?>]"
                               id="file_delete_<?= $propertyID ?>_<?= $i ?>" value="Y"/><label
                            for="file_delete_<?= $propertyID ?>_<?= $i ?>"><?= GetMessage("IBLOCK_FORM_FILE_DELETE") ?></label>
                        <br/>
                        <?

                        if ($arResult["ELEMENT_FILES"][$value]["IS_IMAGE"]) {
                            ?>
                            <img src="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>" height="" width="150px"
                                 border="0"/><br/>
                        <?
                        } else {
                            ?>
                            <?= GetMessage("IBLOCK_FORM_FILE_NAME") ?>: <?= $arResult["ELEMENT_FILES"][$value]["ORIGINAL_NAME"] ?>
                            <br/>
                            <?= GetMessage("IBLOCK_FORM_FILE_SIZE") ?>: <?= $arResult["ELEMENT_FILES"][$value]["FILE_SIZE"] ?>
                            <br/>
                            [
                            <a href="<?= $arResult["ELEMENT_FILES"][$value]["SRC"] ?>"><?= GetMessage("IBLOCK_FORM_FILE_DOWNLOAD") ?></a>]
                            <br/>
                        <?
                        }
                    }
                }

                break;
            case "L":

                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["LIST_TYPE"] == "C")
                    $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "checkbox" : "checkbox";
                else
                    $type = $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y" ? "multiselect" : "dropdown";

                switch ($type):
                    case "checkbox":
                    case "radio":
                        foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                            $checked = false;
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                if (is_array($arResult["ELEMENT_PROPERTIES"][$propertyID])) {
                                    foreach ($arResult["ELEMENT_PROPERTIES"][$propertyID] as $arElEnum) {
                                        if ($arElEnum["VALUE"] == $key) {
                                            $checked = true;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if ($arEnum["DEF"] == "Y") $checked = true;
                            }

                            ?>
                            <input type="<?= $type ?>"
                                   name="PROPERTY[<?= $propertyID ?>]<?= $type == "checkbox" ? "[" . $key . "]" : "" ?>"
                                   value="<?= $key ?>"
                                   id="property_<?= $key ?>"<?= $checked ? " checked=\"checked\"" : "" ?> /><label
                                for="property_<?= $key ?>"><?= $arEnum["VALUE"] ?></label><br/>
                        <?
                        }
                        break;

                    case "dropdown":
                    case "multiselect":
                        ?>
                        <select <?
                                  switch($propertyID) {
                                      case PROP_NAMES_LIST_ID :
                                          echo 'id="names-list-prop"';
                                          break;
                                      case "IBLOCK_SECTION" :
                                          echo 'id="section-name"';
                                          break;
                                      case PROP_TYPE_ID :
                                          echo 'id="obj-type"';
                                          break;
                                      case PROP_TERM_ID :
                                          echo 'id="term"';
                                  }?>
                            name="PROPERTY[<?= $propertyID ?>]<?= $type == "multiselect" ? "[]\" size=\"" . $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] . "\" multiple=\"multiple" : "" ?>">
                            <option value=""><? echo GetMessage("CT_BIEAF_PROPERTY_VALUE_NA") ?></option>
                            <?
                            if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                            else $sKey = "ELEMENT";

                            foreach ($arResult["PROPERTY_LIST_FULL"][$propertyID]["ENUM"] as $key => $arEnum) {
                                $checked = false;
                                if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                    foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                        if ($key == $arElEnum["VALUE"]) {
                                            $checked = true;
                                            break;
                                        }
                                    }
                                } else {
                                    if ($arEnum["DEF"] == "Y") $checked = true;
                                }
                                ?>
                                <option value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?> <? if($propertyID == PROP_TERM_ID): ?>data-term="<?= $arEnum["SORT"] ?><? endif ?>"><?= $arEnum["VALUE"] ?></option>
                            <?
                            }
                            ?>
                        </select>
                        <?
                        break;
                endswitch;
                break;
            case "E":
                for ($i = 0; $i < $inputNum; $i++) {
                    if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                        $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                    } elseif ($i == 0) {
                        $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];

                    } else {
                        $value = "";
                    }
                    if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["USER_TYPE"] == "EAutocomplete") {

                        CModule::IncludeModule('iblock');
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/iblock/admin_tools.php');
                        $property_fields = $arResult["PROPERTY_LIST_FULL"][$propertyID];
                        $arUserType = CIBlockProperty::GetUserType($property_fields["USER_TYPE"]);
                        echo call_user_func_array($arUserType["GetPropertyFieldHtml"],
                            array(
                                $property_fields,
                                array("VALUE" => $value),
                                array(
                                    "VALUE" => 'PROPERTY[' . $property_fields["ID"] . '][' . $i . ']',
                                    "DESCRIPTION" => 'PROPERTY[' . $property_fields["ID"] . '][' . $i . '][DESCRIPTION]',
                                    "FORM_NAME" => 'iblock_add',
                                    "MODE" => "FORM_FILL",
                                    "COPY" => false,
                                ),
                            )
                        );
                    }
                }
                break;
        endswitch; ?>
        </td>
        </tr>
    <? endforeach; ?>
    </tbody>
<? endif ?>
<tfoot>
<tr>
    <td colspan="2">
        <input type="submit" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
        <? if (strlen($arParams["LIST_URL"]) > 0): ?>
            <input type="submit" name="iblock_apply" value="<?= GetMessage("IBLOCK_FORM_APPLY") ?>"/>
            <input
                type="button"
                name="iblock_cancel"
                value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
                onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"]) ?>';"
                >
        <? endif ?>
    </td>
</tr>
</tfoot>
</table>
</form>
