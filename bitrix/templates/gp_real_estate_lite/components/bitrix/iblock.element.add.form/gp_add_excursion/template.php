<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):?>
    <? ShowError(implode("", array("0" => GetMessage(REVIEW_ERROR_MASSAGE)))) ?>
<?endif;?>

<script type="text/javascript">
    $(function () {
        $('.user-cell').mask("9 (999) 999-9999");
    });
    $(document).ready(function () {
        $('.mf-ok-text').delay(5000).fadeOut();
    });
</script>
<? if(strlen($arResult["MESSAGE"]) > 0): ?>
    <div style="display:block;" class="mf-ok-text">���� ������ �������</div>
    <? ?>
<? endif; ?>
<form name="iblock_add" action="" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <table class="add-review-table excursion">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
            <tbody>
            <tr data-name="NAME">
                <td><?= GetMessage("IBLOCK_FORM_YOUR_NAME") ?></td>
                <td><input type="text" name='PROPERTY[NAME][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            <tr data-name="78">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["78"]["NAME"] ?></td>
                <td><input type="text" name='PROPERTY[78][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            <tr data-name="79">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["79"]["NAME"] ?></td>
                <td><input class="user-cell" type="text" name='PROPERTY[79][<?= $i ?>]' size="25" value="<?= $value ?>">
                </td>
            </tr>
            <tr data-name="80">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["80"]["NAME"] ?></td>
                <td><input type="text" name='PROPERTY[80][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            </tbody>
        <? endif ?>
        <tfoot>
        <tr>
            <td colspan="2">
                <input class="add-review-submit" type="submit" name="iblock_submit"
                       value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>">
            </td>
        </tr>
        </tfoot>
    </table>
</form>