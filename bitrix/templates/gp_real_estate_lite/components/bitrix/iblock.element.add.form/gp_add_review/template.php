<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):?>
    <? ShowError(implode("", array("0" => GetMessage(REVIEW_ERROR_MASSAGE)))) ?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
    <? ShowNote($arResult["MESSAGE"]) ?>
<? endif ?>
<form name="iblock_add" action="<?= POST_FORM_ACTION_URI ?>" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <table class="add-review-table">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
            <tbody>
            <? foreach ($arResult["PROPERTY_LIST"] as $propertyID): ?>
                <tr data-name="<?= $propertyID ?>">
                    <td>
                        <? if ($propertyID == "NAME"): ?>
                            ���� ���
                        <? endif; ?>
                        <? if ($propertyID == "DETAIL_TEXT"): ?>
                            �����
                        <? endif; ?>
                        <? if ($propertyID == "73"): ?>
                            ��� e-mail
                        <? endif; ?>
                    </td>
                    <td>
                        <?
                        if (intval($propertyID) > 0) {
                            if (
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                                &&
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                            )
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                            elseif (
                                (
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                    ||
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                                )
                                &&
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                            )
                                $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                        }

                        $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                        switch ($INPUT_TYPE):
                            case "T":
                                ?>
                                <textarea cols="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"] ?>"
                                          rows="<?= $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] ?>"
                                          name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]"><?= $value ?></textarea>
                                <? break;
                            case "S":
                            case "N":
                                ?>
                                <input type="text" name="PROPERTY[<?= $propertyID ?>][<?= $i ?>]" size="25"
                                       value="<?= $value ?>"/>
                                <? break;
                        endswitch; ?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        <? endif ?>

        <tfoot>
        <tr>
            <td colspan="2">
                <input class="add-review-submit" type="submit" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>"/>
            </td>
        </tr>
        </tfoot>
    </table>
</form>