<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?=$arParams["REDIRECT_FOLDER"]?>" method="get">
    <table cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" />
            </td>
        </tr>
        <tr>
            <td height="10" valign="top"></td>
        </tr>
        <tr>
            <td valign="top"><?=GetMessage("REALTY_TYPE")?></td>
        </tr>
        <tr>
            <td valign="top">
                <select id="realty-type">
                    <?
                    $dbSections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array('IBLOCK_ID' => CATALOG_IBLOCK_ID, 'GLOBAL_ACTIVE' => 'Y'), true);
                    while ($section = $dbSections->GetNext()): ?>
                        <option value="<?=$section['SECTION_PAGE_URL']?>"
                                <?if ($section['SECTION_PAGE_URL'] == $arParams["REDIRECT_FOLDER"]):?>selected<?endif;?>>
                            <?=$section['NAME']?>
                        </option>
                    <? endwhile; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td height="10" valign="top"></td>
        </tr>
        </thead>
        <tbody>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if(!array_key_exists("HIDDEN", $arItem)):?>
                <tr>
                    <td valign="top"><?=htmlspecialchars_decode($arItem["NAME"])?></td>
                </tr>
                <tr>
                    <td valign="top"><?=$arItem["INPUT"]?></td>
                </tr>
                <tr>
                    <td height="10" valign="top"></td>
                </tr>
            <?endif?>
        <?endforeach;?>
        </tbody>
    </table>
</form>
