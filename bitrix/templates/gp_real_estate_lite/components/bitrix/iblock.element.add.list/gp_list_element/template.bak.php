<?
$colspan = 6;
if ($arResult["CAN_EDIT"] == "Y") $colspan++;
if ($arResult["CAN_DELETE"] == "Y") $colspan++;
?>

<script type="text/javascript">
    $(document).ready(function hideDiv() {
        $('font.notetext').delay(3000).fadeOut();
    });
</script>

<div class="fadeout-div">
    <? if (strlen($arResult["MESSAGE"]) > 0): ?>
        <?= ShowNote($arResult["MESSAGE"]) ?>
    <? endif ?>
</div>


<table class="data-table">
    <? if ($arResult["NO_USER"] == "N"): ?>
        <thead>
        <tr>
            <td<?= $colspan > 1 ? " colspan=\"" . $colspan . "\"" : "" ?>><?= GetMessage("IBLOCK_ADD_LIST_TITLE") ?></td>
        </tr>
        </thead>
        <tbody>
        <? if (count($arResult["ELEMENTS"]) > 0): ?>
            <? foreach ($arResult["ELEMENTS"] as $arElement): ?>
                <tr>
                    <td>
                        <?= $arElement["NAME"] ?>
                    </td>
                    <td>
                        <small>
                            <?= is_array($arResult["WF_STATUS"]) ? $arResult["WF_STATUS"][$arElement["WF_STATUS_ID"]] : $arResult["ACTIVE_STATUS"][$arElement["ACTIVE"]] ?>
                        </small>
                    </td>

                    <td><?
                        $developerProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_DEVELOPER_ID));
                        while ($developer = $developerProp->GetNext())
                            echo $developer['VALUE'];
                        ?>
                    </td>
                    <td><?
                        $streetProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_STREET_ID));
                        while ($streetID = $streetProp->GetNext())
                            $streetElement = CIBlockElement::GetByID($streetID["VALUE"]);
                        if ($street = $streetElement->GetNext())
                            echo $street["NAME"] . ", ";
                        $houseProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_HOUSE_ID));
                        while ($house = $houseProp->GetNext())
                            echo $house['VALUE'];
                        ?>
                    </td>
                    <td><?
                        $priceProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_PRICE_ID));
                        while ($price = $priceProp->GetNext())
                            echo number_format($price['VALUE'], 0, '.', ' ') . " ";
                        $priceCurrProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_PRICE_CURR_ID));
                        while ($priceCurr = $priceCurrProp->GetNext())
                            echo $priceCurr['VALUE_ENUM'];
                        ?>
                    </td>
                    <td><?
                        $allAreaProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_ALL_AREA_ID));
                        while ($allArea = $allAreaProp->GetNext())
                            echo $allArea['VALUE'] . GetMessage('IBLOCK_LIST_ALL_AREA');
                        ?>
                    </td>

                    <? if ($arResult["CAN_EDIT"] == "Y"): ?>
                        <td>
                            <? if ($arElement["CAN_EDIT"] == "Y"): ?><a
                                href="<?= $arParams["EDIT_URL"] ?>?edit=Y&amp;CODE=<?= $arElement["ID"] ?>"><?= GetMessage("IBLOCK_ADD_LIST_EDIT") ?><? else: ?>&nbsp;<?
                                endif ?></a></td>
                    <? endif ?>
                    <? if ($arResult["CAN_DELETE"] == "Y"): ?>
                        <td><? if ($arElement["CAN_DELETE"] == "Y"): ?><a
                                href="?delete=Y&amp;CODE=<?= $arElement["ID"] ?>&amp;<?= bitrix_sessid_get() ?>"
                                onClick="return confirm('<? echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))) ?>')"><?= GetMessage("IBLOCK_ADD_LIST_DELETE") ?></a><? else: ?>&nbsp;<?endif ?>
                        </td>
                    <? endif ?>
                </tr>
            <? endforeach ?>
        <? else: ?>
            <tr>
                <td<?= $colspan > 1 ? " colspan=\"" . $colspan . "\"" : "" ?>><?= GetMessage("IBLOCK_ADD_LIST_EMPTY") ?></td>
            </tr>
        <?endif ?>
        </tbody>
    <? endif ?>
    <tfoot>
    <tr>
        <td<?= $colspan > 1 ? " colspan=\"" . $colspan . "\"" : "" ?>><? if ($arParams["MAX_USER_ENTRIES"] > 0 && $arResult["ELEMENTS_COUNT"] < $arParams["MAX_USER_ENTRIES"]): ?>
                <a href="<?= $arParams["EDIT_URL"] ?>?edit=Y"><?= GetMessage("IBLOCK_ADD_LINK_TITLE") ?></a><? else: ?><?= GetMessage("IBLOCK_LIST_CANT_ADD_MORE") ?><?endif ?>
        </td>
    </tr>
    </tfoot>
</table>
<? if (strlen($arResult["NAV_STRING"]) > 0): ?><?= $arResult["NAV_STRING"] ?><? endif ?>