<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<div class="gp_hide">
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/jquery.watable.js" charset="windows-1251"></script>
    <link rel='stylesheet' href='<?= SITE_TEMPLATE_PATH ?>/js/watable-master/watable.css'/>
    <link rel='stylesheet' href="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/bootstrap.min.css"/>

    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/watable-master/objects.add.list.js"></script>

    <div class="list-filter">
        <label for="object">������:</label>
        <select id="object" class="object-select" data-child="2">
            <option value="">..</option>
            <?
            $nameProp = CIBlockProperty::GetPropertyEnum(PROP_NAMES_LIST_ID, array("NAME" => "ASC"), array());
            while ($name = $nameProp->GetNext()):?>
                <option value="<?= $name['VALUE'] ?>"><?= $name['VALUE'] ?></option>
            <? endwhile; ?>
        </select>
        <label for="developer">����������:</label>
        <select id="developer" class="object-select" data-child="3">
            <option value="">..</option>
            <?
            $developerProp = CIBlockProperty::GetPropertyEnum(PROP_DEVELOPER_NEW_ID, array("NAME" => "ASC"), array());
            while ($developer = $developerProp->GetNext()):?>
                <option value="<?= $developer['VALUE'] ?>"><?= $developer['VALUE'] ?></option>
            <? endwhile; ?>
        </select>
        <? if ($APPLICATION->GetCurPage(false) == "/personal/objects/"): ?>
            <label for="agent">�����:</label>
            <select id="agent" class="object-select" data-child="4">
                <option value="">..</option>
                <?
                $dbUsers = CUser::GetList($sortBy = "LAST_NAME", $sortOrder = "ASC", array("GROUPS_ID" => "7", "ACTIVE" => "Y"));
                while ($arUser = $dbUsers->Fetch()):?>
                    <option value="<?= $arUser["LAST_NAME"] ?>"><?= $arUser["LAST_NAME"] ?></option>
                <? endwhile; ?>
            </select>
        <? endif; ?>
    </div>

    <div id="object-table">
        <input type="button" onclick="location.href='<?= $arParams["EDIT_URL"] ?>?edit=Y';" value="<?= GetMessage("IBLOCK_ADD_LINK_TITLE") ?>">
    </div>

    <script type="text/javascript">
        function generateData() {
            var data = {
                cols: {
                    ���: {
                        index: 1,
                        type: "string"
                    },
                    ������: {
                        index: 2,
                        type: "string"
                    },
                    ����������: {
                        index: 3,
                        type: "string"
                    },
                    <?if ($APPLICATION->GetCurPage(false) == "/personal/objects/"):?>
                    �����: {
                        index: 4,
                        type: "string"
                    },
                    <?endif;?>
                    �����: {
                        index: 5,
                        type: "string"
                    },
                    ����: {
                        index: 6,
                        type: "number"
                    },
                    �������: {
                        index: 7,
                        type: "number"
                    },
                    ����: {
                        index: 8,
                        type: "date",
                        sortOrder: "desc"
                    },
                    ���: {
                        index: 9,
                        type: "string",
                        filter: false,
                        sorting: false
                    },
                    ���: {
                        index: 10,
                        type: "string",
                        filter: false,
                        sorting: false
                    },
                    ��: {
                        index: 11,
                        type: "string",
                        filter: false,
                        sorting: false
                    }
                },
                rows: [
                    <? if (count($arResult["ELEMENTS"]) > 0): ?>
                    <? foreach ($arResult["ELEMENTS"] as $arElement): ?>
                    <? if($arElement["ACTIVE"] != "Y") continue; ?>
                    {
                        ���: "<?= $arElement["ID"] ?>",
                        ������: "<?= $arElement["NAME"] ?>",
                        ����������: "<?
                                               $developerProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_DEVELOPER_NEW_ID));
                                               while ($developer = $developerProp->GetNext())
                                                   echo $developer["VALUE_ENUM"];
                                             ?>",
                        <?if ($APPLICATION->GetCurPage(false) == "/personal/objects/"):?>
                        �����: "<?
                                               $agentProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_AGENT_ID));
                                               while ($agentID = $agentProp->GetNext())
                                                   $rsUser = CUser::GetByID($agentID['VALUE']);
                                                   $arUser = $rsUser->Fetch();
                                                   echo $arUser["NAME"] . " " . $arUser["LAST_NAME"];
                                             ?>",
                        <?endif;?>
                        �����: "<?
                                          $streetProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_STREET_ID));
                                          while ($streetID = $streetProp->GetNext())
                                              $streetElement = CIBlockElement::GetByID($streetID["VALUE"]);
                                          if ($street = $streetElement->GetNext())
                                              echo $street["NAME"] . ", ";
                                          $houseProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_HOUSE_ID));
                                          while ($house = $houseProp->GetNext())
                                              echo $house['VALUE'];
                                        ?>",
                        ����: "<?
                                         $priceProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_PRICE_ID));
                                         while ($price = $priceProp->GetNext())
                                             echo intval($price['VALUE']);
                                        ?>",
                        �������: "<?
                                            $allAreaProp = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["ID"], array("sort" => "asc"), array("ID" => PROP_ALL_AREA_ID));
                                            while ($allArea = $allAreaProp->GetNext())
                                                echo floatval(str_replace(',', '.', $allArea['VALUE']));
                                          ?>",
                        ����: getDate("<?= $arElement["DATE_CREATE"] ?>"),
                        ���: '<a class="iblock-add-list-edit" href="<?= $arParams["EDIT_URL"] ?>?edit=Y&amp;CODE=<?= $arElement["ID"] ?>"></a>',
                        ���: '<a class="iblock-add-list-copy" href="<?= $arParams["EDIT_URL"] ?>?edit=Y&amp;CODE=<?= $arElement["ID"] ?>&amp;copy=Y"></a>',
                        ��: '<a class="iblock-add-list-del" href="?delete=Y&amp;CODE=<?= $arElement["ID"] ?>&amp;<?= bitrix_sessid_get() ?>" onClick="return confirm(\'<? echo CUtil::JSEscape(str_replace("#ELEMENT_NAME#", $arElement["NAME"], GetMessage("IBLOCK_ADD_LIST_DELETE_CONFIRM"))) ?>\')"></a>'
                    },
                    <? endforeach; ?>
                    <? endif; ?>
                ]
            };
            return data;
        }
        function getDate(date) {
            var d = new Date(date.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
            return d.getTime();
        }
    </script>
</div>

