<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//Make all properties present in order
//to prevent html table corruption
foreach($arResult["ELEMENTS"] as $key => $arElement)
{
	$arRes = array();
	foreach($arParams["PROPERTY_CODE"] as $pid)
	{
		$arRes[$pid] = CIBlockFormatProperties::GetDisplayValue($arElement, $arElement["PROPERTIES"][$pid], "catalog_out");
	}
	$arResult["ELEMENTS"][$key]["DISPLAY_PROPERTIES"] = $arRes;
    //echo "<pre>" . print_r($arRes) . "</pre>";
}
//usort($arResult['ELEMENTS'], array("CCabinet_SortObject", "cmp_"."DATE"."_"."DESC"));