<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->ShowHead();
?>
<script type="text/javascript">
    jQuery(function () {
        jQuery('.user_phone').mask("+7 (999) 999-9999");
    });
    jQuery(document).ready(function () {
        $('.mf-ok-text').delay(5000).fadeOut();
        <?if(!empty($arResult["ERROR_MESSAGE"])):?>
            <?foreach($arResult["ERROR_MESSAGE"] as $v):?>
                <?if(!$arResult['MESSAGE']):?>
                    jQuery('.user_message').click(function () {
                        jQuery('.user_message').removeClass('gp_errors');
                        jQuery('.user_message').attr('placeholder', '<?=GetMessage('GP_MESSAGE')?>');
                    });
                    jQuery('.user_message').addClass('gp_errors');
                    <?if(md5($v) == md5(GetMessage("GP_REQ_MESSAGE"))):?>
                        jQuery('.user_message').attr('placeholder', '<?=$v?>');
                    <?endif;?>
                <?endif;?>
                <?if(!$arResult['AUTHOR_NAME']):?>
                    jQuery('.user_name').click(function () {
                        jQuery('.user_name').removeClass('gp_errors');
                        jQuery('.user_name').attr('placeholder', '<?=GetMessage('GP_NAME')?>');
                    });
                    jQuery('.user_name').addClass('gp_errors');
                    <?if(md5($v) == md5(GetMessage("GP_REQ_NAME"))):?>
                        jQuery('.user_name').attr('placeholder', '<?=$v?>');
                    <?endif;?>
                <?endif;?>
                <?if(!$arResult['AUTHOR_PHONE']):?>
                    jQuery('.user_phone').click(function () {
                        jQuery('.user_phone').removeClass('gp_errors');
                        jQuery('.user_phone').attr('placeholder', '<?=GetMessage('GP_PHONE')?>');
                    });
                    jQuery('.user_phone').addClass('gp_errors');
                    <?if(md5($v) == md5(GetMessage("GP_REQ_PHONE"))):?>
                        jQuery('.user_phone').attr('placeholder', '<?=$v?>');
                    <?endif;?>
                <?endif;?>
            <?endforeach;?>
        <?endif;?>
    });
</script>

<div class="mfeedback">
    <form class="order_call_form" name="order_call_form" action="<?= $APPLICATION->GetCurPage() ?>" method="POST">
        <?= bitrix_sessid_post() ?>
        <div class="order_call_form__title">
            <?if(strpos($APPLICATION->GetCurPage(false), "services")): ?>
                <?= GetMessage("GP_TITLE_SERVICE") ?>
            <?elseif(strpos($APPLICATION->GetCurPage(false), "tour")):?>
                <?= GetMessage("GP_TITLE_ASK") ?>
            <?else:?>
                <?= GetMessage("GP_TITLE") ?>
            <?endif?>
        </div>
        <div class="mf-name">
            <input class="order_call user_name" placeholder="<?= GetMessage("GP_NAME") ?>" type="text" name="user_name"
                   value="<?= $arResult["AUTHOR_NAME"] ?>"><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])): ?><? endif ?>
        </div>
        <div class="mf-phone">
            <input class="order_call user_phone" placeholder="<?= GetMessage("GP_PHONE") ?>" type="text"
                   name="user_phone"
                   value="<?= $arResult["AUTHOR_PHONE"] ?>"><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])): ?><? endif ?>
        </div>
        <div class="mf-message">
            <textarea class="user_message" placeholder="<?= GetMessage("GP_MESSAGE") ?>" type="text" name="MESSAGE"
                   value="<?= $arResult["MESSAGE"] ?>"></textarea><? if (empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])): ?><? endif ?>
        </div>
        <div class="form-notify"><?= GetMessage("GP_NOTIFY") ?></div>
        <div>
            <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
            <input type="submit" class="order_call_button" name="submit" value="<?= GetMessage("GP_SUBMIT") ?>">
        </div>
    </form>
    <? if(empty($arResult["ERROR_MESSAGE"]) && $arResult['RESULT'] == "SUCCESS"): ?>
        <div style="display:block;" class="mf-ok-text"><?= $arParams["OK_TEXT"] ?></div>
    <? endif; ?>
</div>

