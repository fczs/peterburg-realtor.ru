$(function() {
    var $input = $('.main-filter-search'),
        result = '.title-search-result',
        cat = $input.data('cat'),
        resultHover = false;

    $(document).ready(function() {
        $input.val('');
    });

    $input.on('keyup focus', (function () {
        $(this).doTimeout("keyup focus", 300, function () {
            $(result + ' ul').remove();
            $(result).hide();
            $.getJSON("/bitrix/templates/gp_real_estate_lite/workers/search_suggest_ajax.php", {q: $(this).val().trim(), cat: cat}, function (response) {
                if (response != "-1") {
                    console.log(response)
                    $(result + ' .search-result-body').append('<ul class="search-result"></ul>');
                    $.each(response, function (index, value) {
                        $(result + ' ul').append('<li class="search-result__item"><a href="#" class="icon-' + value.KEY + '" data-type="' + value.KEY + '" data-value="' + value.ENUM + '">' + index + '</a></li>');
                    });
                    $(result).show();
                }
                console.log(response);
            })
        });
    }));

    $(result).hover(function() {
        resultHover = true;
    }, function() {
        resultHover = false;
    });

    $(document).on('click', '.search-result__item a', function (e) {
        var $search = $(this);
        $input.val($search.text());
        $('#search-data').val($search.data('value')).attr('name', 'gp_filter_pf[' + $search.data('type') + '][]');
        $(result).hide();
        e.preventDefault();
    });

    $input.on('blur', function() {
        if(!resultHover)
            $(result).hide();
    });
});