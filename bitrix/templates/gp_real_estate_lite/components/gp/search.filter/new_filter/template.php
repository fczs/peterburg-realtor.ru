<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$placeholder = "";
$page = getCatPage($APPLICATION->GetCurPage(false));
switch ($page) {
    case 'new':
        $placeholder =  GetMessage("NEW_PLACEHOLDER");
        break;
    case 'resale':
        $placeholder = GetMessage("RESALE_PLACEHOLDER");
        break;
    case 'abroad':
        $placeholder = GetMessage("ABROAD_PLACEHOLDER");
        break;
    case 'commercial':
        $placeholder = GetMessage("COMMERCIAL_PLACEHOLDER");
        break;
    case 'country':
        $placeholder = GetMessage("COUNTRY_PLACEHOLDER");
        break;
    case 'elite':
        $placeholder = GetMessage("ELITE_PLACEHOLDER");
        break;
    case 'rent':
        $placeholder = GetMessage("RENT_PLACEHOLDER");
}
?>
<form name="<?=$arResult['FILTER_NAME'] . "_form"?>" action="<?=$arResult['FORM_ACTION']?>" method="get" class="gp-form gp-filter search-filter">
    <div class="gp-items">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "gp_filter",
            array(
                "ROOT_MENU_TYPE" => "filter",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => "",
                "MAX_LEVEL" => "0",
                "CHILD_MENU_TYPE" => "",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ),
            false
        );?>
        <div class="filter-inner">
            <div class="search-form">
                <input type="hidden" id="search-data">
                <input type="hidden" name="set_filter" value="Y"/>
                <input type="text" class="main-filter-search" data-cat="<?=$page?>" autocomplete="off" placeholder="<?=$placeholder?>">
                <button class="filter-submit"
                        type="submit"
                        name="set_filter"
                        value="<?= GetMessage("IBLOCK_SET_FILTER") ?>">
                    <?= GetMessage("IBLOCK_SET_FILTER"); ?>
                </button>
                <div class="title-search-result" tabindex="-1">
                    <div class="search-result-body">
                        <!--Search results-->
                    </div>
                </div>
            </div>
            <div class="filter filter-new">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                        <? if ($arItem["CODE"] == "type"): ?>
                            <div class="gp-item">
                                <label><?=GetMessage("TYPE");?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "commercial"): ?>
                            <div class="gp-item">
                                <label><?=GetMessage("TYPE");?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "type_of_deal"): ?>
                            <div class="gp-item">
                                <label><?=GetMessage("DEAL");?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "rooms"): ?>
                            <div class="gp-item">
                                <label><?=GetMessage("ROOMS");?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "price"): ?>
                            <div class="gp-item">
                                <label><?= $arItem['NAME']; ?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "all_area"): ?>
                            <div class="gp-item">
                                <label><?= $arItem['NAME']; ?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "site_area"): ?>
                            <div class="gp-item">
                                <label><?= $arItem['NAME']; ?></label>
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "term"): ?>
                            <div class="gp-item">
                                <label><?=GetMessage("TERM");?></label>
                                <select id="term-select" class="chosen-select chosen-select-deselect">
                                    <option value="default" disabled selected hidden><?= GetMessage("TERM_PLACEHOLDER") ?></option>
                                    <?
                                    $arSelect = Array("ID", "PROPERTY_term");
                                    $result = CIBlockElement::GetList(array("propertysort_60" => "ASC"), array("IBLOCK_ID" => 5, "SECTION_ID" => 9), array("PROPERTY_term"), false, $arSelect);
                                    while ($element = $result->GetNextElement()) {
                                        $arFields = $element->GetFields();
                                        if (!empty($arFields["PROPERTY_TERM_VALUE"]))
                                            $arDates[] = $arFields["PROPERTY_TERM_VALUE"];
                                    }
                                    foreach ($arDates as $arDate): ?>
                                        <option value="<?= $arDate ?>"><?= $arDate ?></option>
                                    <? endforeach ?>
                                </select>
                            </div>
                        <? endif; ?>
                        <? if ($arItem["CODE"] == "term_date"): ?>
                            <div class="hidden-input">
                                <?= $arItem['INPUT']; ?>
                            </div>
                        <? endif; ?>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
            <?/*<div class="filter-buttons">
                    <button class="filter-cancel"
                            type="submit"
                            name="del_filter"
                            value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>">
                        <span><?= GetMessage("IBLOCK_DEL_FILTER"); ?></span>
                    </button>
                </div>*/?>
            <div class="clear"></div>
        </div>
    </div>
</form>