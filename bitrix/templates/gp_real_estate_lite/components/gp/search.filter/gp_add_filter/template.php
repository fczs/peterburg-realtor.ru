<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<form name="<? echo $arResult['FILTER_NAME'] . "_form" ?>" action="<? echo $arResult['FORM_ACTION'] ?>" method="get"
      class="gp-form gp-filter">

    <?foreach ($arResult['ITEMS'] as $arItem):
        if (array_key_exists("HIDDEN", $arItem)):
        endif;
    endforeach;?>
    <div class="gp-items">
        <? echo "<pre>";
        //print_r($arResult);
        echo "</pre>" ?>
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                <? if ($arItem["CODE"] == "names_list"): ?>
                    <div class="gp-item rooms">
                        <?= $arItem['INPUT']; ?>
                    </div>
                <? endif; ?>
            <? endif; ?>
        <? endforeach; ?>

        <div class="filter-buttons">
            <input type="hidden" name="set_filter" value="Y"/>
            <button type="submit" name="del_filter"
                    value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>">
                <span><?= GetMessage("IBLOCK_DEL_FILTER"); ?></span></button>
            <button type="submit" name="set_filter"
                    value="<?= GetMessage("IBLOCK_SET_FILTER") ?>"><?= GetMessage("IBLOCK_SET_FILTER"); ?></button>
        </div>
    </div>

</form>