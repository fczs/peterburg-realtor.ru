<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$arCity = json_encode(array_map("w2u", $arResult["arrProp"][37]["VALUE_LIST"]));
$arDistrict = json_encode(array_map("w2u", $arResult["arrProp"][39]["VALUE_LIST"]));
$arStreet = json_encode(array_map("w2u", $arResult["arrProp"][40]["VALUE_LIST"]));
$spb = json_encode(w2u($arResult["arrProp"][38]["VALUE_LIST"][490]));
?>

<script type="text/javascript">
    var arCity = <? echo $arCity ?>;
    var arDistrict = <? echo $arDistrict ?>;
    var arStreet = <? echo $arStreet ?>;
    var spb = <? echo $spb ?>;
</script>

<script type="text/javascript" src="/bitrix/templates/gp_real_estate_lite/components/gp/search.filter/gp_filter/search_filter.js"></script>

<form name="<? echo $arResult['FILTER_NAME'] . "_form" ?>" action="<? echo $arResult['FORM_ACTION'] ?>" method="get" class="gp-form gp-filter">
    <div class="filter filter-bg">
        <div class="filter-wrapper">
            <div class="gp-items">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "gp_filter",
                    array(
                        "ROOT_MENU_TYPE" => "filter",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "MENU_CACHE_GET_VARS" => "",
                        "MAX_LEVEL" => "0",
                        "CHILD_MENU_TYPE" => "",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N"
                    ),
                    false
                );?>
                <div class="filter-default">
                    <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                            <? if ($arItem["CODE"] == "type"): ?>
                                <div class="gp-item type">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "commercial"): ?>
                                <div class="gp-item commercial">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "type_of_deal"): ?>
                                <div class="gp-item type_of_deal">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "rooms"): ?>
                                <div class="gp-item rooms">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "rooms_of_transaction"): ?>
                                <div class="gp-item rooms_of_transaction">
                                    <label>������ ��� �������</label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "rooms_in_flat"): ?>
                                <div class="gp-item rooms_in_flat">
                                    <label>����� ������ � ��������</label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "price"): ?>
                                <div class="gp-item price">
                                    <label><?= $arItem['NAME']; ?> (���.)</label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "term"): ?>
                                <div class="gp-item term">
                                    <label>���� ����� ��:</label>
                                    <select id="term-select" class="chosen-select chosen-select-deselect">
                                        <option value="default" disabled selected hidden><?= GetMessage("TERM_PLACEHOLDER") ?></option>
                                        <?
                                        $arSelect = Array("ID", "PROPERTY_term");
                                        $result = CIBlockElement::GetList(array("propertysort_60" => "ASC"), array("IBLOCK_ID" => 5, "SECTION_ID" => 9), array("PROPERTY_term"), false, $arSelect);
                                        while ($element = $result->GetNextElement()) {
                                            $arFields = $element->GetFields();
                                            if (!empty($arFields["PROPERTY_TERM_VALUE"]))
                                                $arDates[] = $arFields["PROPERTY_TERM_VALUE"];
                                        }
                                        foreach ($arDates as $arDate): ?>
                                            <option value="<?= $arDate ?>"><?= $arDate ?></option>
                                        <? endforeach ?>
                                    </select>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "term_date"): ?>
                                <div class="hidden-input">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "area"): ?>
                                <br>
                                <div id="area-chosen" class="gp-item left-input">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "city"): ?>
                                <div id="city-chosen" class="gp-item right-input">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "district"): ?>
                                <br>
                                <div id="district-chosen" class="gp-item left-input">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "metro"): ?>
                                <div class="gp-item right-input metro">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "all_area" && $arResult["FORM_ACTION"] == "/catalog/commercial/"): ?>
                                <br>
                                <div class="gp-item all_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <div class="filter-more-button">
                        <span class="show-more">����������� ������</span><span class="show-less">�������� ������</span>
                    </div>
                    <div class="filter-buttons">
                        <div class="gp-buttons" style="text-align: <?= $arParams['BUTTON_ALIGN']; ?>">
                            <input type="hidden" name="set_filter" value="Y"/>
                            <button class="filter-cancel"
                                    type="submit"
                                    name="del_filter"
                                    value="<?= GetMessage("IBLOCK_DEL_FILTER") ?>">
                                <span><?= GetMessage("IBLOCK_DEL_FILTER"); ?></span>
                            </button>
                            <button class="filter-submit"
                                    type="submit"
                                    name="set_filter"
                                    value="<?= GetMessage("IBLOCK_SET_FILTER") ?>">
                                <?= GetMessage("IBLOCK_SET_FILTER"); ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="filter-more">
                    <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                            <? if ($arItem["CODE"] == "address"): ?>
                                <div class="gp-item left-input">
                                    <select id="street-select"
                                            class="chosen-select chosen-select-deselect"
                                            data-placeholder="<?= $arItem["NAME"] ?>"
                                            name="<?= $arItem["INPUT_NAME"] ?>"></select>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "number"): ?>
                                <div class="gp-item number">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "building"): ?>
                                <div class="gp-item left-input">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "floor"): ?>
                                <div class="gp-item floor">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "all_area" && $arResult["FORM_ACTION"] != "/catalog/commercial/"): ?>
                                <br>
                                <div class="gp-item all_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "live_area"): ?>
                                <div class="gp-item live_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "kitchen_area"): ?>
                                <div class="gp-item kitchen_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "room_area"): ?>
                                <div class="gp-item room_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "site_area"): ?>
                                <div class="gp-item live_area">
                                    <label><?= $arItem['NAME']; ?></label><br><?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                            <? if ($arItem["CODE"] == "infrastructure"): ?>
                                <br>
                                <div class="gp-item infrastructure">
                                    <?= $arItem['INPUT']; ?>
                                </div>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</form>