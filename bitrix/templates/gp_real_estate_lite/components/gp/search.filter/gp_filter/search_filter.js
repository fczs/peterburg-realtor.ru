$(document).ready(function () {
    var areaSelect = $('[name="gp_filter_pf[area]"]'),
        citySelect = $('[name="gp_filter_pf[city]"]'),
        districtSelect = $('[name="gp_filter_pf[district]"]'),
        streetSelect = $('[name="gp_filter_pf[address]"]'),
        areaChosen = $('#area-chosen .chosen-single'),
        cityChosen = $('#city-chosen .chosen-single'),
        districtChosen = $('#district-chosen .chosen-single'),
        areaChosenVal = $('#area-chosen .chosen-single span'),
        cityChosenVal = $('#city-chosen .chosen-single span'),
        districtChosenVal = $('#district-chosen .chosen-single span');

    //���� ������ ������ ���, �� ������� ����� �����, � ��������� ������� ���� ��� ������ ����� ����������
    var $metro = $('.metro .chosen-select');
    if ($metro.length > 0) {
        $('.metro .chosen-container').append('<div class="chosen-arrow"><b></b></div>');
        if (areaChosenVal.html() != spb)
            $metro.prop('disabled', true).trigger("chosen:updated");
        areaSelect.chosen({
            allow_single_deselect: true,
            disable_search_threshold: 10
        }).change(function() {
                if (areaChosenVal.html() == spb)
                    $metro.prop('disabled', false).trigger("chosen:updated");
                else
                    $metro.prop('disabled', true).trigger("chosen:updated");
            });
    }

    if (areaChosen.hasClass('chosen-default'))
        citySelect.prop('disabled', true).html('').trigger("chosen:updated"); // ������ ������ ���. ������� ����������
    else
        getCities();
    if (cityChosen.hasClass('chosen-default'))
        districtSelect.prop('disabled', true).html('').trigger("chosen:updated"); // ������ ������ �������/���. ������� ����������
    else
        getDistricts();
    if (districtChosen.hasClass('chosen-default'))
        streetSelect.prop('disabled', true).html('').trigger("chosen:updated"); // ������ ������ ���� ����������
    else
        getStreets();

    areaSelect.chosen({
        allow_single_deselect: true,
        disable_search_threshold: 10
    }).change(function () {
            citySelect.prop('disabled', true).html('').trigger("chosen:updated");
            districtSelect.prop('disabled', true).html('').trigger("chosen:updated");
            streetSelect.prop('disabled', true).html('').trigger("chosen:updated");

            if (!areaChosen.hasClass('chosen-default')) {
                getCities();
            }
        });

    citySelect.chosen({
        allow_single_deselect: true,
        disable_search_threshold: 10
    }).change(function () {
            districtSelect.prop('disabled', true).html('').trigger("chosen:updated");
            streetSelect.prop('disabled', true).html('').trigger("chosen:updated");

            if (!cityChosen.hasClass('chosen-default')) {
                getDistricts();
            }
        });

    districtSelect.chosen({
        allow_single_deselect: true,
        disable_search_threshold: 10
    }).change(function () {
            streetSelect.prop('disabled', true).html('').trigger("chosen:updated");

            if (!districtChosen.hasClass('chosen-default')) {
                getStreets();
            }
        });

    function getCities() {
        var selected = $('[name="gp_filter_pf[city]"] option:selected').val();
        var region = areaChosenVal.html();

        // �������� AJAX ������, ������� ������ ������ ���. ������� ��� ���������� �������
        $.getJSON('/bitrix/templates/gp_real_estate_lite/components/gp/search.filter/gp_filter/ajax.php', {action: 'getCity', region: region}, function (cityList) {

            citySelect.html('').trigger("chosen:updated"); // ������� ������ ���. �������
            citySelect.append('<option value=""></option>').trigger("chosen:updated");

            // ��������� ������ ���. �������, ��������� � ������ ���������� �������
            $.each(arCity, function (key, value) {
                $.each(cityList, function () {
                    if (value == this) {
                        citySelect.append('<option value="' + key + '">' + value + '</option>').trigger("chosen:updated");
                        return true;
                    }
                });
            });

            $('[name="gp_filter_pf[city]"] [value="' + selected + '"]').attr("selected", "selected").trigger("chosen:updated");
            citySelect.prop('disabled', false).trigger("chosen:updated"); // ������ ������ ���. ������� ��������
        });
    }

    function getDistricts() {
        var selected = $('[name="gp_filter_pf[district]"] option:selected').val();
        var region = areaChosenVal.html(), city = cityChosenVal.html();

        // �������� AJAX ������, ������� ������ ������ �������/���. ������� ��� ���������� ���. ������
        $.getJSON('/bitrix/templates/gp_real_estate_lite/components/gp/search.filter/gp_filter/ajax.php', {action: 'getDistrict', region: region, city: city}, function (districtList) {

            districtSelect.html('').trigger("chosen:updated"); // ������� ������ ���. �������
            districtSelect.append('<option value=""></option>').trigger("chosen:updated");

            // ��������� ������ �������/���. �������, ��������� � ������ ���������� �������
            $.each(arDistrict, function (key, value) {
                $.each(districtList, function () {
                    if (value == this) {
                        districtSelect.append('<option value="' + key + '">' + value + '</option>').trigger("chosen:updated");
                        return true;
                    }
                });
            });

            $('[name="gp_filter_pf[district]"] [value="' + selected + '"]').attr("selected", "selected").trigger("chosen:updated");
            districtSelect.prop('disabled', false).trigger("chosen:updated"); // ������ ������ �������/���. ������� ��������
        });
    }

    function getStreets() {
        var selected = $('[name="gp_filter_pf[address]"] option:selected').val();
        var region = areaChosenVal.html(), city = cityChosenVal.html(), district = districtChosenVal.html();

        // �������� AJAX ������, ������� ������ ������ ���� ��� ���������� ������/���. ������
        $.getJSON('/bitrix/templates/gp_real_estate_lite/components/gp/search.filter/gp_filter/ajax.php', {action: 'getStreet', region: region, city: city, district: district}, function (streetList) {

            streetSelect.html('').trigger("chosen:updated"); // ������� ������ ���. �������
            streetSelect.append('<option value=""></option>').trigger("chosen:updated");

            // ��������� ������ ����, ��������� � ������ ���������� �������
            $.each(streetList, function (a, b) {
                $.each(arStreet, function (key, value) {
                    if (b == value) {
                        streetSelect.append('<option value="' + key + '">' + value + '</option>').trigger("chosen:updated");
                        return false;
                    }
                });
            });

            $('[name="gp_filter_pf[address]"] [value="' + selected + '"]').attr("selected", "selected").trigger("chosen:updated");
            streetSelect.prop('disabled', false).trigger("chosen:updated"); // ������ ������ ���� ��������
        });
    }
});

