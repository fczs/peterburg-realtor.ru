<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die(0); //������ ajax ������

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$region = u2w($_GET['region']);
$city = u2w($_GET['city']);
$district = u2w($_GET['district']);

$arAddress = [];

if ($action == 'getCity') {
    if (CModule::IncludeModule("iblock")) {
        //�������� �������, ������� ������� �������� (���������)
        $arFilterRegion = Array('IBLOCK_ID' => 7, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
        $resultRegion = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterRegion, true);
        while ($array_region = $resultRegion->Fetch()) {
            if ($array_region['NAME'] == $region) {
                //������� ���. ������ ��� ������� �������, ������ ������� �������� (���������)
                $arFilterCity = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_region['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 2, "INCLUDE_SUBSECTIONS" => "Y");
                $resultCity = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterCity, true);
                while ($array_city = $resultCity->Fetch()) {
                    $city_id = $array_city['ID'];
                    $arAddress[] = $array_city['NAME'];
                }
                break;
            }
        }
    }

    $result = array_map("w2u", $arAddress);

    echo json_encode($result);

    exit;
}

if ($action == 'getDistrict') {
    if (CModule::IncludeModule("iblock")) {
        //�������� �������, ������� ������� �������� (���������)
        $arFilterRegion = Array('IBLOCK_ID' => 7, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
        $resultRegion = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterRegion, true);
        while ($array_region = $resultRegion->Fetch()) {
            if ($array_region['NAME'] == $region) {
                //������� ���. ������ ��� ������� �������, ������ ������� �������� (���������)
                $arFilterCity = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_region['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 2, "INCLUDE_SUBSECTIONS" => "Y");
                $resultCity = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterCity, true);
                while ($array_city = $resultCity->Fetch()) {
                    if ($array_city['NAME'] == $city) {
                        //������� ������ ��� ������� ���. ������, ������ ������� �������� (���������)
                        $arFilterDistrict = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_city['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 3, "INCLUDE_SUBSECTIONS" => "Y");
                        $resultDistrict = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterDistrict, true);
                        while ($array_district = $resultDistrict->Fetch()) {
                            $arAddress[] = $array_district['NAME'];
                        }
                        break 2;
                    }
                }
            }
        }
    }

    $result = array_map("w2u", $arAddress);

    echo json_encode($result);

    exit;
}

if ($action == 'getStreet') {
    if (CModule::IncludeModule("iblock")) {
        //�������� �������, ������� ������� �������� (���������)
        $arFilterRegion = Array('IBLOCK_ID' => 7, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
        $resultRegion = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterRegion, true);
        while ($array_region = $resultRegion->Fetch()) {
            if ($array_region['NAME'] == $region) {
                //������� ���. ������ ��� ������� �������, ������ ������� �������� (���������)
                $arFilterCity = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_region['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 2, "INCLUDE_SUBSECTIONS" => "Y");
                $resultCity = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterCity, true);
                while ($array_city = $resultCity->Fetch()) {
                    if ($array_city['NAME'] == $city) {
                        //������� ������ ��� ������� ���. ������, ������ ������� �������� (���������)
                        $arFilterDistrict = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_city['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 3, "INCLUDE_SUBSECTIONS" => "Y");
                        $resultDistrict = CIBlockSection::GetList(Array('name' => 'asc'), $arFilterDistrict, true);
                        while ($array_district = $resultDistrict->Fetch()) {
                            if ($array_district['NAME'] == $district) {
                                //������� ����� ��� ������� ������, ��������� ������� �������� (�������)
                                $arFilterStreet = Array('IBLOCK_ID' => 7, "SECTION_ID" => $array_district['ID'], 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 4);
                                $resultStreet = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilterStreet, false, false, Array("ID", "NAME", "IBLOCK_SECTION_ID"));
                                while ($array_street = $resultStreet->Fetch()) {
                                    $arAddress[] = $array_street['NAME'];
                                }
                                break 3;
                            }
                        }
                    }
                }
            }
        }
    }

    $result = array_map("w2u", $arAddress);

    echo json_encode($result);

    exit;
}