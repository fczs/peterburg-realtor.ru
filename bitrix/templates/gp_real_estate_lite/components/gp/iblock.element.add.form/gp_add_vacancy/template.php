<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);?>

<script type="text/javascript">
    $(function () {
        $('.user-cell').mask("9 (999) 999-9999");
    });
</script>

<? if(strlen($arResult["MESSAGE"]) > 0): ?>
    <div style="display:block;" class="mf-ok-text">���� ������ �������</div>
    <script type="text/javascript">
        $('.mf-ok-text').delay(5000).fadeOut();
    </script>
<? endif; ?>

<? if (!empty($arResult["ERRORS"])):?>
    <? ShowError(implode("", array("0" => GetMessage(REVIEW_ERROR_MASSAGE)))) ?>
<?endif;?>

<form name="iblock_add" action="" method="post" enctype="multipart/form-data">
    <?= bitrix_sessid_post() ?>
    <table class="add-review-table excursion">
        <? if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])): ?>
            <tbody>
            <tr data-name="NAME">
                <td><?= GetMessage("IBLOCK_FORM_YOUR_NAME") ?></td>
                <td><input type="text" name='PROPERTY[NAME][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            <tr data-name="82">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["82"]["NAME"] ?></td>
                <td><input type="text" name='PROPERTY[82][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            <tr data-name="83">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["83"]["NAME"] ?></td>
                <td><input class="user-cell" type="text" name='PROPERTY[83][<?= $i ?>]' size="25" value="<?= $value ?>"></td>
            </tr>
            <tr data-name="84">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["84"]["NAME"] ?></td>
                <td>
                    <select name="PROPERTY[84][<?= $i ?>]">
                        <?
                        if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                        else $sKey = "ELEMENT";
                        foreach ($arResult["PROPERTY_LIST_FULL"][84]["ENUM"] as $key => $arEnum) {
                            $checked = false;
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                    if ($key == $arElEnum["VALUE"]) {
                                        $checked = true;
                                        break;
                                    }
                                }
                            } else {
                                if ($arEnum["DEF"] == "Y") $checked = true;
                            }
                            ?>
                            <option
                                value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                        <?
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr data-name="85">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["85"]["NAME"] ?></td>
                <td><select name="PROPERTY[85][<?= $i ?>]">
                        <?
                        if (intval($propertyID) > 0) $sKey = "ELEMENT_PROPERTIES";
                        else $sKey = "ELEMENT";
                        foreach ($arResult["PROPERTY_LIST_FULL"][85]["ENUM"] as $key => $arEnum) {
                            $checked = false;
                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) {
                                foreach ($arResult[$sKey][$propertyID] as $elKey => $arElEnum) {
                                    if ($key == $arElEnum["VALUE"]) {
                                        $checked = true;
                                        break;
                                    }
                                }
                            } else {
                                if ($arEnum["DEF"] == "Y") $checked = true;
                            }
                            ?>
                            <option
                                value="<?= $key ?>" <?= $checked ? " selected=\"selected\"" : "" ?>><?= $arEnum["VALUE"] ?></option>
                        <?
                        }
                        ?>
                    </select></td>
            </tr>
            <tr data-name="86">
                <td><?= $arResult["PROPERTY_LIST_FULL"]["86"]["NAME"] ?></td>
                <td><textarea name='PROPERTY[86][<?= $i ?>]' rows="5" cols="25" value="<?= $value ?>"></textarea></td>
            </tr>

            </tbody>
        <? endif ?>

        <tfoot>
        <tr>
            <td colspan="2">
                <input class="add-review-submit" type="submit" name="iblock_submit" value="<?= GetMessage("IBLOCK_FORM_SUBMIT") ?>">
            </td>
        </tr>
        </tfoot>
    </table>
</form>