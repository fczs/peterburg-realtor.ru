<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="<?= LANG_CHARSET; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><? $APPLICATION->ShowTitle(); ?></title>
        <?
        $APPLICATION->ShowMeta("robots", false, true);
        $APPLICATION->ShowMeta("description", false, true);
        ?>
        <? if(ERROR_404 != "Y"): ?><link rel="canonical" href="http://<?= $_SERVER['HTTP_HOST']; ?><?= parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>"/><? endif; ?>

        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="theme-color" content="#ffffff">
        
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/fancybox/jquery.fancybox-1.3.4.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/jcarousel/jcarousel.connected-carousels.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/animations.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/component.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/timeline.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/animate.css');?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/js/sidr/jquery.sidr.dark.css');?>
        <?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/styles.min.css');?>

        <?$APPLICATION->AddHeadString('<link rel="stylesheet" type="text/css" media="print" href="' . SITE_TEMPLATE_PATH . '/css/print.css" />', true);?>
        <?$APPLICATION->AddHeadString('<link rel="alternate stylesheet" type="text/css" media="screen,projection" href="' . SITE_TEMPLATE_PATH . '/css/print.css" title="print" disabled="disabled" />', true);?>

        <?$APPLICATION->ShowCSS(true, true);?>

        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.scrollUp.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.maskedinput-1.3.1.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jcarousel/jcarousel.connected-carousels.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jcarousel/jquery.jcarousel.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/sidr/jquery.sidr.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/Base64.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/modernizr.custom.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/stickUp.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-dotimeout.min.js"></script>
        <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>

        <?$APPLICATION->ShowHeadStrings()?>

        <!--<![endif]-->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/html5.js"></script>
        <![endif]-->
    </head>
<body>
<div class="wrapper">
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
    <div class="header-wrapper">
        <div class="gp_hide">
            <div class="bus-banner">
                <div class="bus-inner">
                    <a href="/tour/" class="more-link"><div class="bus"><span>���������</span></div></a>
                </div>
            </div>
        </div>
        <div class="gp_menu-mobile">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "gp_default_mobile",
                array(
                    "ROOT_MENU_TYPE" => "top",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_THEME" => "site"
                ),
                false
            );?>
        </div>

        <!--/  Header /-->
        <header>
            <div class="header-left">
                <a href="<?= SITE_DIR; ?>">
                    <? $APPLICATION->IncludeFile(SITE_DIR . "include/logo.php", Array(), Array("MODE" => "html")); ?>
                </a>
            </div>
            <div class="header-center gp_hide">
                <a href="/cooperating/vacancy/">
                    <span>&nbsp;���������� �� ������ �������!&nbsp;</span><br>
                    <span>&nbsp;������� <span>%</span>&nbsp;</span>
                </a>
            </div>
            <div class="header-right">
                <? $APPLICATION->IncludeFile(SITE_DIR . "include/address.php", Array(), Array("MODE" => "html")); ?>
            </div>
            <div class="clear"></div>
        </header>

        <!--/ End Header /-->

        <!--/  Navigation /-->
        <nav class="navbar-wrapper">
            <div class="nav">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "gp_default",
                    array(
                        "ROOT_MENU_TYPE" => "top",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MAX_LEVEL" => "3",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_THEME" => "site"
                    ),
                    false
                );?>
            </div>
        </nav>
        <div class="clear gp_hide"></div>
        <!--/ End Navigation /-->
    </div>
<?
$page = getCatPage($APPLICATION->GetCurPage(false));
if ($page != 'none'): ?>
    <!-- Filter -->
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
            "AREA_FILE_SHOW" => "sect",
            "AREA_FILE_SUFFIX" => $page,
            "AREA_FILE_RECURSIVE" => "Y",
            "EDIT_TEMPLATE" => ""
        )
    );?>
    <!-- End Filter -->
<? endif ?>

<!--/ Content /-->
<? if ($APPLICATION->GetCurPage(false) == SITE_DIR): ?>
    <div class="gp_hide">
        <div class="contain">
            <a class="comission" href="http://petreator.nmarket.pro/">
                �������� <span>0</span>% �� �����������
            </a>
        </div>
    </div>
<? endif; ?>
    <div class="content">
        <? if (!$APPLICATION->GetCurPage() != "/"): ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "gp_breadcrumb",
                Array()
            );?>
        <? endif; ?>
<? if (!strpos($APPLICATION->GetCurPage(false), "catalog") && $APPLICATION->GetCurPage(false) != SITE_DIR && !strpos($APPLICATION->GetCurPage(false), "personal") && $APPLICATION->GetCurPage(false) != "/about/" && $APPLICATION->GetCurPage(false) != "/services/" && $APPLICATION->GetCurPage(false) != "/cooperating/" && $APPLICATION->GetCurPage(false) != "/info/"): ?>
    <div class="content-left">
<? endif ?>