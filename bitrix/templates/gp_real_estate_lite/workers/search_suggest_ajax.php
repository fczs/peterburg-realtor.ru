<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') die(0);

CModule::IncludeModule("iblock");

strlen($_GET["q"]) > 0 or die("-1");

$query = trim(htmlspecialcharsbx($_GET["q"]));

$query = mb_strtolower($query);
$Query = u2w(my_mb_ucfirst($query));
$QUERY = u2w(mb_strtoupper($query));
$query = u2w($query);
$category = u2w($_GET["cat"]);

//������������ ���������
$result = [];
//������ ��� ������ ������� �� ��
$arFilter = [];
//������������ ���� ������ �������
$arSelectFields = [];
//��������� ������������
$cat = "";

//������ �������� � ��������� �������
if ($category == "new") {
    $cat = CAT_NEW;
    $arFilter = Array(
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => $cat,
        "ACTIVE" => "Y",
        Array("LOGIC" => "OR",
            Array("PROPERTY_HOUSE_NAME_VALUE" => "%$query%"),
            Array("PROPERTY_DEVELOPER_NEW_VALUE" => "%$query%"),
            Array("PROPERTY_DISTRICT_VALUE" => "%$query%"),
            Array("PROPERTY_METRO_VALUE" => "%$query%"),
            Array("ID" => $query . "%"),
        ),
    );
    $arSelectFields = Array("ID", "PROPERTY_HOUSE_NAME", "PROPERTY_DEVELOPER_NEW", "PROPERTY_DISTRICT", "PROPERTY_METRO");
} elseif ($category == "resale" || $category == "commercial" || $category == "elite" || $category == "rent") {
    if ($category == "resale")
        $cat = CAT_RESALE;
    elseif ($category == "commercial")
        $cat = CAT_COMMERCIAL;
    elseif($category == "elite")
        $cat = CAT_ELITE;
    else
        $cat = CAT_RENT;
    $arFilter = Array(
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => $cat,
        "ACTIVE" => "Y",
        Array("LOGIC" => "OR",
            Array("PROPERTY_DISTRICT_VALUE" => "%$query%"),
            Array("PROPERTY_METRO_VALUE" => "%$query%"),
            Array("ID" => $query . "%")
        ),
    );
    $arSelectFields = Array("ID", "PROPERTY_DISTRICT", "PROPERTY_METRO");
} elseif ($category == "country") {
    $cat = CAT_COUNTRY;
    $arFilter = Array(
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => $cat,
        "ACTIVE" => "Y",
        Array("LOGIC" => "OR",
            Array("PROPERTY_DISTRICT_VALUE" => "%$query%"),
            Array("ID" => $query . "%")
        ),
    );
    $arSelectFields = Array("ID", "PROPERTY_DISTRICT");
}

//������� ID ���� ��� ���� �������� � ���������
$arEntireCat = Array("IBLOCK_ID" => CATALOG_IBLOCK_ID, "SECTION_ID" => $cat, "ACTIVE" => "Y");
$addressItems = CIBlockElement::GetList(Array("SORT" => "ASC"), $arEntireCat, false, false, Array("PROPERTY_ADDRESS"));
$streetID = [];
while ($address = $addressItems->Fetch()) {
    $streetID[] = $address["PROPERTY_ADDRESS_VALUE"];
}

//�� ����������� ������� ������� �������� ��� ������� ID ����
$arStreetIDs = Array("IBLOCK_ID" => ADDRESS_IBLOCK_ID, "ID" => $streetID, "ACTIVE" => "Y");
$dbStreets = CIBlockElement::GetList(Array("NAME" => "ASC"), $arStreetIDs, false, false, Array("ID", "NAME"));
$streetName = [];
while ($street = $dbStreets->Fetch()) {
    //���� � �������� ����� ���� ������ �� �������, ������� �� � ������ ����������
    if (strpos($street["NAME"], $query) !== false || strpos($street["NAME"], $Query) !== false || strpos($street["NAME"], $QUERY) !== false) {
        $name = w2u($street["NAME"]);
        $result[$name]["KEY"] = "address";
        $result[$name]["ENUM"] = $street["ID"];
    }
    if (count($result) >= 5) {
        break;
    }
}

//����� �� ������ ���������
$searchDB = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelectFields);
while ($search = $searchDB->Fetch()) {
    foreach ($search as $k => $v) {
        if ((strpos($v, $query) !== false || strpos($v, $Query) !== false || strpos($v, $QUERY) !== false) && !array_key_exists($v, $result)) {
            $tmpKey = str_replace("_VALUE", "", $k);
            $enumID = empty($search[$tmpKey . "_ENUM_ID"]) ? $search[$tmpKey] : $search[$tmpKey . "_ENUM_ID"];
            $key = w2u(str_replace("PROPERTY_", "", $tmpKey));
            $v = w2u($v);
            $result[$v]["KEY"] = strtolower($key);
            $result[$v]["ENUM"] = strtolower($enumID);
        }
        if (count($result) >= 15) {
            echo json_encode($result);
            exit;
        }
    }
}
if (count($result) == 0) die("-1");

echo json_encode($result);