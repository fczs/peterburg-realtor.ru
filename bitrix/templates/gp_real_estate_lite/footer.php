<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
?>

<? if (!strpos($APPLICATION->GetCurPage(false), "catalog") && $APPLICATION->GetCurPage(false) !== SITE_DIR && !strpos($APPLICATION->GetCurPage(false), "personal")): ?>
    </div>
    <? if ($APPLICATION->GetCurPage(false) == "/about/" || $APPLICATION->GetCurPage(false) == "/services/" || $APPLICATION->GetCurPage(false) == "/cooperating/" || $APPLICATION->GetCurPage(false) == "/info/"): ?>
        <div class="right-block-margin"></div>
    <? endif ?>
    <div class="content-right">
        <?$APPLICATION->IncludeFile(
            SITE_DIR . "include/right_block.php",
            Array(),
            Array("MODE" => "html")
        );?>
    </div>
    <div class="clear"></div>
<? endif ?>
<div class="gp_hide">
    <? if ($APPLICATION->GetCurPage(false) == SITE_DIR): ?>
        <div class="index-feed">
            <ul class="grid effect-7" id="grid">
                <li>
                    <div class="info-block news-block">
                        <? $APPLICATION->IncludeFile(SITE_DIR . "include/news.php"); ?>
                    </div>
                </li>
                <li>
                    <div class="info-block reviews-block">
                        <? $APPLICATION->IncludeFile(SITE_DIR . "include/reviews.php"); ?>
                    </div>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
    <? endif; ?>
</div>
</div><!--/ Content /-->

<? if ($APPLICATION->GetCurPage(false) == SITE_DIR): ?>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/masonry.pkgd.min.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/imagesloaded.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/classie.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/GridLoadingEffects/AnimOnScroll.js"></script>
    <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.5,
            maxDuration: 1,
            viewportFactor: 0.2
        });
    </script>
<? endif; ?>

</div><!--/ Wrapper /-->

<div class="gp_hide">
    <div class="partners-block">
        <div><img src="/images/partners/sber.png"></div>
        <div><a href="/redirect.php?site=http://www.spbpn.ru/companies/companies.html" target="_blank"><img src="/images/partners/spn.png"></a></div>
        <div class="newestate"><a href="/redirect.php?site=http://www.newestate.ru/excursions/" target="_blank"><img src="/images/partners/newestate.gif"></a></div>
    </div>
    <div class="clear"></div>
</div>

<!--/  Footer  /-->
<footer>
    <div class="footer">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "gp_bottom",
            array(
                "ROOT_MENU_TYPE" => "bottom", // ��� ���� ��� ������� ������
                "MENU_CACHE_TYPE" => "N", // ��� �����������
                "MENU_CACHE_TIME" => "", // ����� ����������� (���.)
                "MENU_CACHE_USE_GROUPS" => "Y", // ��������� ����� �������
                "MENU_CACHE_GET_VARS" => "", // �������� ���������� �������
                "MAX_LEVEL" => "0", // ������� ����������� ����
                "CHILD_MENU_TYPE" => "", // ��� ���� ��� ��������� �������
                "USE_EXT" => "N", // ���������� ����� � ������� ���� .���_����.menu_ext.php
                "DELAY" => "N", // ����������� ���������� ������� ����
                "ALLOW_MULTI_SELECT" => "N", // ��������� ��������� �������� ������� ������������
            ),
            false
        );?>

        <div class="copyright">
            <?$APPLICATION->IncludeFile(
                SITE_DIR . "include/copy.php",
                Array(),
                Array("MODE" => "html")
            );?><?=
            GetMessage('CFST_COPY');?>
        </div>
        <div class="clear"></div>
        <div class="footer-contacts">
            <div class="footer-mode">
                <a href="tel:+78123266904">+7 (812) 326-69-04</a>
            </div>
            <div class="footer-addr">
                <a href="<?=SITE_DIR?>contacts/">�����-���������, ��. ����� �������������, ������������ ��., 17, ���� 402</a>
            </div>
        </div>
        <div class="gp-social">
            <a class="gp-social-icon vkontakte" href="/redirect.php?site=http://vk.com/nspb777" target="_blank"></a>
            <a class="gp-social-icon facebook" href="/redirect.php?site=https://ru-ru.facebook.com/spbrealt" target="_blank"></a>
            <a class="gp-social-icon odnoklassniki" href="/redirect.php?site=http://ok.ru/profile/518733442321" target="_blank"></a>
        </div>
        <?if($USER->isAdmin()):?>
            <div class="ya-informer">
            <!-- Yandex.Metrika informer -->
            <a href="/redirect.php?site=https://metrika.yandex.ru/stat/?id=32719135&amp;from=informer"
               target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/32719135/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                                   style="width:88px; height:31px; border:0;" alt="������.�������" title="������.�������: ������ �� ������� (���������, ������ � ���������� ����������)" onclick="try{Ya.Metrika.informer({i:this,id:32719135,lang:'ru'});return false}catch(e){}" /></a>
            <!-- /Yandex.Metrika informer -->
            </div>
        <?endif?>
        <div class="clear"></div>
    </div>
</footer>
<!--/ End  Footer  /-->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32719135 = new Ya.Metrika({
                    id:32719135,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32719135" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google counter -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68144084-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- /Google counter -->
</body>

</html>