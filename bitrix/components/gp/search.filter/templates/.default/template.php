<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<form name="<?echo $arResult['FILTER_NAME']."_form"?>" action="<?echo $arResult['FORM_ACTION']?>" method="get" class="gp-form gp-filter">
<div class="filter">
<div class="filter-wrapper">
	<?foreach($arResult['ITEMS'] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
		/* Fix */
			//echo $arItem['INPUT'];
		endif;
	endforeach;?>	
	<?if(!empty($arParams['FILTER_TITLE'])):?>
	<h3><?=$arParams['FILTER_TITLE'];?></h3>
	<?endif;?>		
	<div class="gp-items">
		<div class="filter-default">
		<?
		$i=0;
		foreach($arResult['ITEMS'] as $arItem):
		$i++;
		?>
			<?if(!array_key_exists("HIDDEN", $arItem)):?>
				<?if ($i < 9):?>
					<div class="gp-item" style="width: <?=intval(100 / $arParams['ELEMENT_IN_ROW']);?>%;">
						<span style="width: <?=$arParams['NAME_WIDTH'];?>px;"><?=$arItem['NAME'];?>:</span> <?=$arItem['INPUT'];?>
					</div>
					<?endif?>		
			<?endif?>		
		<?endforeach;?>
		<div class="filter-more-button btn"><?=GetMessage("IBLOCK_MORE_FILTER");?></div>
		</div>
		
		<div class="filter-more">
		<?
		$i=0;
		foreach($arResult['ITEMS'] as $arItem):
		$i++;
		?>
			<?if(!array_key_exists("HIDDEN", $arItem)):?>
				<?if ($i > 9):?>
					<div class="gp-item" style="width: <?=intval(100 / $arParams['ELEMENT_IN_ROW']);?>%;">
						<span style="width: <?=$arParams['NAME_WIDTH'];?>px;"><?=$arItem['NAME'];?>:</span> <?=$arItem['INPUT'];?>
					</div>
				<?endif?>		
			<?endif?>		
		<?endforeach;?>
		</div>
	</div>
	</div>
</div>
	<div class="gp-buttons" style="text-align: <?=$arParams['BUTTON_ALIGN'];?>">
		<input type="hidden" name="set_filter" value="Y" />
		<button type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>"><?=GetMessage("IBLOCK_SET_FILTER");?></button>&nbsp;&nbsp;
		<button type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>"><?=GetMessage("IBLOCK_DEL_FILTER");?></button>
	</div>
</form>