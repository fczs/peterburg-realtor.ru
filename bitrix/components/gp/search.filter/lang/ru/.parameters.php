<?
if( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true )
	die();

$MESS['IBLOCK_PRICES']               = '����';
$MESS['IBLOCK_TYPE']                 = '��� ���������';
$MESS['IBLOCK_IBLOCK']               = '��������';
$MESS['IBLOCK_PROPERTY']             = '��������';
$MESS['IBLOCK_SECTION_ID']           = 'ID ������� �� $_REQUEST';
$MESS['IBLOCK_SECTION_CODE']         = '��� ������� �� $_REQUEST';
$MESS['IBLOCK_PRICE_CODE']           = '��� ����';
$MESS['IBLOCK_FIELD']                = '����';
$MESS['REDIRECT_FOLDER']             = '������� ��� �������� ������';
$MESS['IBLOCK_FILTER_NAME_OUT']      = '��� ���������� ������� ��� ����������';
$MESS['IBLOCK_LIST_HEIGHT']          = '������ ������� �������������� ������, (�����)';
$MESS['IBLOCK_TEXT_WIDTH']           = '������ ������������ ��������� �����, (px)';
$MESS['IBLOCK_NUMBER_WIDTH']         = '������ ����� ����� ��� �������� ����������, (px)';
$MESS['IBLOCK_SELECT_WIDTH']         = '������ ����� ���������� �������, (px)';
$MESS['IBLOCK_SAVE_IN_SESSION']      = '��������� ��������� ������� � ������ ������������';
$MESS['CP_BCF_CACHE_GROUPS']         = '��������� ����� �������';
$MESS['CP_BCF_OFFERS_FIELD_CODE']    = '���� �����������';
$MESS['CP_BCF_OFFERS_PROPERTY_CODE'] = '�������� �����������';
$MESS['ELEMENT_IN_ROW']              = '����� � ����� ������, (��)';
$MESS['NAME_WIDTH']                  = '������ �������� �����, (px)';
$MESS['BUTTON_ALIGN']                = '������������ ������';
$MESS['FILTER_TITLE']                = '��������� �������';
$MESS['GP_SEARCHFILTER_FILTER']     = '������';
$MESS['GP_SEARCHFILTER_LEFT']      = '�����';
$MESS['GP_SEARCHFILTER_RIGHT']     = '������';
$MESS['GP_SEARCHFILTER_CENTER']  = '�� ������';
$MESS['SELECT_IN_CHECKBOX']          = '���������� ������ �������';
$MESS['SELECT_IN_RADIO']         	 = '����� ������';
$MESS['CHECKBOX_NEW_STRING']         = '������ � ����� ������';
$MESS['RADIO_NEW_STRING']      	     = '����� ������ � ����� ������';
$MESS['INCLUDE_JQUERY']              = '�������� jQuery';
$MESS['INCLUDE_JQUERY_UI']           = '�������� jQuery UI';
$MESS['INCLUDE_JQUERY_UI_SLIDER']    = '�������� ��������';
$MESS['JQUERY_UI_SLIDER_BORDER_RADIUS'] = '������� ��������';
$MESS['JQUERY_UI_THEME']             = '���� jQuery UI';
$MESS['JQUERY_UI_FONT_SIZE']         = '������ ������ jQuery UI';
$MESS['INCLUDE_CHOSEN_PLUGIN']       = '�������� ������-���������� ���������� �������';
$MESS['INCLUDE_FORMSTYLER_PLUGIN']   = '�������� ������-���������� �������/��������������';
$MESS['REPLACE_ALL_LABEL']           = '�������� (���) �� �������� ���� �������';
$MESS['INCLUDE_PLACEHOLDER']         = '�������� placeholder';
$MESS['CHECK_ACTIVE_SECTIONS']       = '��������� ���������� �������� ��� ����������';
$MESS['INCLUDE_AUTOCOMPLETE_PLUGIN'] = '�������� �����-����������� ��� ��������� �����';
$MESS['REMOVE_POINTS']               = '������ ����� �� ������� � ����� ��������';

$MESS['JQUERY_GROUP'] = 'JS � jQuery';