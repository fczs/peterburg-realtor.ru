<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('GP_NAME'),
	"DESCRIPTION" => GetMessage('GP_DESCRIPTION'),
	"ICON" => "/images/user_list.gif",
	"CACHE_PATH" => "Y",
	"PATH"        => array(
			"ID"    => "gp-components",
			"NAME"  => GetMessage("GP_NODE_NAME"),
			"CHILD" => array(
				"ID"   => "folder.other",
				"NAME" => GetMessage("GP_FOLDER_NAME"),
				"SORT" => 10,
			),
		),
);
?>
