<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();


$arGroups = array();
$rsGroups = CGroup::GetList($by="c_sort", $order="asc", Array("ACTIVE" => "Y"));
while ($arGroup = $rsGroups->Fetch())
{
	$arGroups[$arGroup["ID"]] = $arGroup["NAME"];
}

if ($bWorkflowIncluded)
{
	$rsWFStatus = CWorkflowStatus::GetList($by="c_sort", $order="asc", Array("ACTIVE" => "Y"), $is_filtered);
	$arWFStatus = array();
	while ($arWFS = $rsWFStatus->Fetch())
	{
		$arWFStatus[$arWFS["ID"]] = $arWFS["TITLE"];
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
		"GROUPS_ID" => array(
			"SORT" => 110,
			"NAME" => GetMessage("GP_FIELDS"),
		),
		"NAV_SETTINGS" => array(
			"SORT" => 120,
			"NAME" => GetMessage("GP_NAV_FIELDS"),
		),
	),
	"PARAMETERS" => array(
		"GROUPS_ID" => array(
			"PARENT" => "GROUPS_ID",
			"NAME" => GetMessage("GP_GROUPS"),
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" => $arGroups,
		),
		"NAV_ON_PAGE" => array(
			"PARENT" => "NAV_SETTINGS",
			"NAME" => GetMessage("GP_NAV_ON_PAGE"),
			"TYPE" => "TEXT",
			"DEFAULT" => "10",
		),
		"TEMPLATE_NAV" => array(
			"PARENT" => "NAV_SETTINGS",
			"NAME" => GetMessage("GP_TEMPLATE_NAV"),
			"TYPE" => "TEXT",
			"DEFAULT" => "",
		),	
		"NAME_NAV" => array(
			"PARENT" => "NAV_SETTINGS",
			"NAME" => GetMessage("GP_NAME_NAV"),
			"TYPE" => "TEXT",
			"DEFAULT" => GetMessage("GP_NAME_NAV"),
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
		"AJAX_MODE" => array(),		
	),
);

?>
