<div id="gp_m-about">
	<div class="agents_gp">			
		<div class="exp-enum">
<? 
foreach($arResult as $arItem): 
					if (intval($arItem["PERSONAL_PHOTO"]) > 0)
			         {
			           $imageFile = CFile::GetFileArray($arItem["PERSONAL_PHOTO"]);
			             if ($imageFile !== false)
			               {
			                  $arFileTmp = CFile::ResizeImageGet(
			                     $imageFile,
			                     array("width" =>"89", "height" => "89"),
			                     BX_RESIZE_IMAGE_PROPORTIONAL,
			                     false
			                  );
			                  // в переменной хранится готовый тег изображения
			                  $userPhoto = CFile::ShowImage($arFileTmp["src"], $width, $height, "border='0'", "");
			               }
			         } else {
				         $userPhoto = '<img src="'.SITE_DIR.'upload/no-pic.jpg" border="0" width="89" height="89">';
			         }
 
?>				
				<div class="item">
						<div class="pic">
							<?=$userPhoto?>						
						</div>
					<div class="body">
						<div class="name">
							<?=$arItem["NAME"]?> <?=$arItem["LAST_NAME"]?>
							</div>
						<div class="position">
							<?=$arItem["PERSONAL_PROFESSION"]?>
						</div>
						<?/*<div class="action">
							<a href="<?=SITE_DIR?>catalog/?agent=<?=$arItem["ID"]?>" class="btn"><?=GetMessage('GP_ALL_USER_OBJECT')?></a>
						</div>*/?>
					</div>
				</div>
<?endforeach;?>
</div>
