<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
// echo '<pre>'; print_r($arParams); echo '</pre>';
CModule::IncludeModule('iblock');

#if ($this->StartResultCache(3600))
#{	
	$gp_group_id = $arParams['GROUPS_ID'];
	$gp_active = $arParams['ACTIVE'];
	$gp_nav_template = $arParams['TEMPLATE_NAV'];
	$gp_count_nav = $arParams['NAV_ON_PAGE'];
	$gp_name_nav = $arParams['NAME_NAV'];
	
	$aFilter = Array
		(
		    "GROUPS_ID" => $gp_group_id,
		    "ACTIVE" => "Y",
		);
	$rsUsers = CUser::GetList(($by="NAME"), ($order="ASC"), $aFilter); // выбираем пользователей
	$is_filtered = $rsUsers->is_filtered; // отфильтрована ли выборка ?
	$rsUsers->NavStart($gp_count_nav); // разбиваем постранично по 10 записей
	$navStr = $rsUsers->GetPageNavStringEx($navComponentObject, $gp_name_nav, $gp_nav_template);
		while($F = $rsUsers->Fetch()) :
				$arResult[] = array(
							"ID" => $F['ID'],
							"NAME" => $F['NAME'],
							"LAST_NAME" => $F['LAST_NAME'],
							"SECOND_NAME" => $F['SECOND_NAME'],
							"EMAIL" => $F['EMAIL'],
							"PERSONAL_WWW" => $F['PERSONAL_WWW'],
							"PERSONAL_ICQ" => $F['PERSONAL_ICQ'],
							"PERSONAL_PHONE" => $F['PERSONAL_PHONE'],
							"PERSONAL_FAX" => $F['PERSONAL_FAX'],
							"PERSONAL_MOBILE" => $F['PERSONAL_MOBILE'],
							"PERSONAL_PROFESSION" => $F['PERSONAL_PROFESSION'],
							"PERSONAL_PHOTO" => $F['PERSONAL_PHOTO'],
							
						   );
		endwhile;
#}
$this->IncludeComponentTemplate();
?>
	<?=$navStr;?>
	</div>
</div>