<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
		"NAME"        => GetMessage("GP_COMPONENT_NAME"),
		"DESCRIPTION" => GetMessage("GP_COMPONENT_DESCR"),
		"ICON"        => "/images/icon.png",
		"PATH"        => array(
			"ID"    => "gp-components",
			"NAME"  => GetMessage("GP_NODE_NAME"),
			"CHILD" => array(
				"ID"   => "folder.service",
				"NAME" => GetMessage("GP_FOLDER_NAME"),
				"SORT" => 10,
			),
		),
	);
?>