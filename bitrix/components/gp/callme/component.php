<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

//print_r($arParams);
$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());
$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '') $arParams["EVENT_NAME"] = "GP_CALLME_ORDER";
$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '') $arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if(strlen($arParams["OK_TEXT"]) <= 0) $arParams["OK_TEXT"] = GetMessage("GP_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["submit"] <> '' && (!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	if(check_bitrix_sessid())
	{
		if(empty($arParams["REQUIRED_FIELDS"]) || !in_array("NONE", $arParams["REQUIRED_FIELDS"]))
		{
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])) && empty($_POST["user_name"]))
				$arResult["ERROR_MESSAGE"][] = GetMessage("GP_REQ_NAME");
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])) && empty($_POST["user_phone"]))
				$arResult["ERROR_MESSAGE"][] = GetMessage("GP_REQ_PHONE");
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) && empty($_POST["MESSAGE"]))
				$arResult["ERROR_MESSAGE"][] = GetMessage("GP_REQ_MESSAGE");
		}		
		if($arParams["USE_CAPTCHA"] == "Y")
		{
			include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
			$captcha_code = $_POST["captcha_sid"];
			$captcha_word = $_POST["captcha_word"];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString("main", "captcha_password", "");
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0)
			{
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass))
					$arResult["ERROR_MESSAGE"][] = GetMessage("GP_CAPTCHA_WRONG");
			}
			else
				$arResult["ERROR_MESSAGE"][] = GetMessage("GP_CAPTHCA_EMPTY");

		}
		if(empty($arResult["ERROR_MESSAGE"]))
		{			
			$arFields = Array(
				"AUTHOR" => htmlspecialcharsbx($_POST["user_name"]),
				"AUTHOR_PHONE" => htmlspecialcharsbx($_POST["user_phone"]),
				"EMAIL_TO" => $arParams["EMAIL_TO"],			
				"CALL_TIME_TEXT" => htmlspecialcharsbx($_POST["MESSAGE"]),
			);
			
			$arLoadFields = Array(
				"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
				"IBLOCK_ID"      => $arParams["IBLOCK_ID"],			
				"NAME"           => htmlspecialcharsbx($_POST["user_name"]),
				"ACTIVE"         => "Y", // активен
				"PREVIEW_TEXT"   => htmlspecialcharsbx($_POST["user_phone"]),
				"DETAIL_TEXT"    => htmlspecialcharsbx($_POST["MESSAGE"]),
				);
				
			CModule::IncludeModule('iblock');
				
			$iBlockElem = new CIBlockElement;
			
			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
			foreach($arParams["EVENT_MESSAGE_ID"] as $v)
				if(IntVal($v) > 0)
				$Res = $iBlockElem->Add($arLoadFields);
				CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
			}
			else{				
				$Res = $iBlockElem->Add($arLoadFields);
				CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
				}
			
			$_SESSION["GP_NAME"] = htmlspecialcharsEx($_POST["user_name"]);
			$_SESSION["GP_PHONE"] = htmlspecialcharsEx($_POST["user_phone"]);
			$_SESSION["GP_MESSAGE"] = htmlspecialcharsEx($_POST["MESSAGE"]);
			
			$arResult['RESULT']="SUCCESS";
			// LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));
		}
		
		$arResult["MESSAGE"] = htmlspecialcharsEx($_POST["MESSAGE"]);
		$arResult["AUTHOR_NAME"] = htmlspecialcharsEx($_POST["user_name"]);
		$arResult["AUTHOR_PHONE"] = htmlspecialcharsEx($_POST["user_phone"]);
	}
	else
	{
		$arResult["ERROR_MESSAGE"][] = GetMessage("GP_SESS_EXP");		
	}
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
	$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

if(empty($arResult["ERROR_MESSAGE"]))
{
	if(strlen($_SESSION["GP_NAME"]) > 0)
		$arResult["AUTHOR_NAME"] = htmlspecialcharsEx($_SESSION["GP_NAME"]);
	if(strlen($_SESSION["GP_PHONE"]) > 0)
		$arResult["AUTHOR_PHONE"] = htmlspecialcharsEx($_SESSION["GP_PHONE"]);
	if(strlen($_SESSION["GP_MESSAGE"]) > 0)
		$arResult["MESSAGE"] = htmlspecialcharsEx($_SESSION["GP_MESSAGE"]);
}

if($arParams["USE_CAPTCHA"] == "Y")
	$arResult["capCode"] =  htmlspecialchars($APPLICATION->CaptchaGetCode());

$this->IncludeComponentTemplate();
