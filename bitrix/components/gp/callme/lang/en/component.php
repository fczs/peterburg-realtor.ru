<?
$MESS['GP_OK_MESSAGE'] = "Thank you. Your order is now being processed.";
$MESS['GP_REQ_NAME'] = "Please type your name.";
$MESS['GP_REQ_PHONE'] = "Please type your phone number";
$MESS['GP_REQ_TIME'] = "Please type call time";
$MESS['GP_REQ_MESSAGE'] = "The message text is required.";
$MESS['GP_CAPTCHA_WRONG'] = "The CAPTCHA code you have typed is incorrect.";
$MESS['GP_CAPTHCA_EMPTY'] = "The CAPTCHA code is required.";
$MESS['GP_SESS_EXP'] = "Your session has expired. Please send your message again.";
?>