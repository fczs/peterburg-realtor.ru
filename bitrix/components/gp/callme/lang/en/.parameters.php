<?
$MESS['GP_CAPTCHA'] = "Use CAPTCHA for Unauthorized Visitors";
$MESS['GP_OK_MESSAGE'] = "Text To Show Upon Sending";
$MESS['GP_OK_TEXT'] = "Thank you. Your order is now being processed.";
$MESS['GP_REQUIRED_FIELDS'] = "Required fields";
$MESS['GP_ALL_REQ'] = "(all non-required)";
$MESS['GP_NAME'] = "Name";
$MESS['GP_PHONE'] = "Phone";
$MESS['GP_MESSAGE'] = "Call time";
$MESS['GP_EMAIL_TEMPLATES'] = "E-mail Templates";
$MESS['GP_EMAIL_TO'] = "E-mail (для отправки заказов звонков)";
$MESS["GP_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["GP_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока";
?>