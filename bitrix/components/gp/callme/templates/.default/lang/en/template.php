<?
$MESS['GP_TITLE'] = "Call Me";
$MESS['GP_NAME'] = "Name";
$MESS['GP_PHONE'] = "Phone number";
$MESS['GP_TIME'] = "Please type call time";
$MESS['GP_CAPTCHA'] = "CAPTCHA";
$MESS['GP_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS['GP_SUBMIT'] = "Send";
?>