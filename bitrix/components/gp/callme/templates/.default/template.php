<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
$APPLICATION->ShowHead();
?>
<script src="<?=$this->GetFolder() ?>/jquery.maskedinput-1.3.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
   jQuery(function() {
          $('.user_phone').mask("9 (999) 999-9999");
       });
</script>	
<div class="mfeedback">
<?if(!empty($arResult["ERROR_MESSAGE"])):
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
elseif (empty($arResult["ERROR_MESSAGE"]) && $arResult['RESULT']=="SUCCESS"):
?>
<div style="display:block;" class="mf-ok-text"><?=$arParams["OK_TEXT"]?></div>
<?endif;?>
<form class="order_call_form" name="order_call_form" action="<?=$APPLICATION->GetCurPage()?>" method="POST">
<?=bitrix_sessid_post()?>
	<h1><?=GetMessage("GP_TITLE")?></h1>
	<div class="mf-name">
		<div class="mf-text">
			<?=GetMessage("GP_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="order_call user_name" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
	</div>
	<div class="mf-phone">
		<div class="mf-text">
			<?=GetMessage("GP_PHONE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="order_call user_phone" type="text" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>">
	</div>	
	<div class="mf-message">
		<div class="mf-text">
			<?=GetMessage("GP_TIME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input  class="user_message" type="text" name="MESSAGE" value="<?=$arResult["MESSAGE"]?>">
		<?
					$APPLICATION->IncludeComponent(
						"bitrix:main.calendar",
						"",
						array(
							"FORM_NAME" => "order_call_form",
							"SHOW_INPUT" => "N",
							"INPUT_NAME" => "MESSAGE",
							"SHOW_TIME" => "Y",
							"HIDE_TIMEBAR" => "N"
						),
						null,
						array("HIDE_ICONS"=>"Y")
					);
				?>
	</div>

	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div class="mf-captcha">
		<div class="mf-text"><?=GetMessage("GP_CAPTCHA")?></div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
		<div class="mf-text"><?=GetMessage("GP_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="">
	</div>
	<?endif;?>
	<div>
		<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
		<input type="submit" class="order_call_button" name="submit" value="<?=GetMessage("GP_SUBMIT")?>">
	</div>
</form>
</div>